[
	{
		emoji: '🐵',
		name: 'Monkey Face',
		hexcode: 'U+1F435',
		html: '&#128053'
	},
	{
		emoji: '🐒',
		name: 'Monkey',
		hexcode: 'U+1F412',
		html: '&#128018'
	},
	{
		emoji: '🦍',
		name: 'Gorilla',
		hexcode: 'U+1F98D',
		html: '&#129421'
	},
	{
		emoji: '🐶',
		name: 'Dog Face',
		hexcode: 'U+1F436',
		html: '&#128054'
	},
	{
		emoji: '🐕',
		name: 'Dog',
		hexcode: 'U+1F415',
		html: '&#128021'
	},
	{
		emoji: '🐩',
		name: 'Poodle',
		hexcode: 'U+1F429',
		html: '&#128041'
	},
	{
		emoji: '🐺',
		name: 'Wolf Face',
		hexcode: 'U+1F43A',
		html: '&#128058'
	},
	{
		emoji: '🦊',
		name: 'Fox Face',
		hexcode: 'U+1F98A',
		html: '&#129418'
	},
	{
		emoji: '🦝',
		name: 'Raccoon',
		hexcode: 'U+1F99D',
		html: '&#129437'
	},
	{
		emoji: '🐱',
		name: 'Cat Face',
		hexcode: 'U+1F431',
		html: '&#128049'
	},
	{
		emoji: '🐈',
		name: 'Cat',
		hexcode: 'U+1F408',
		html: '&#128008'
	},
	{
		emoji: '🦁',
		name: 'Lion Face',
		hexcode: 'U+1F981',
		html: '&#129409'
	},
	{
		emoji: '🐯',
		name: 'Tiger Face',
		hexcode: 'U+1F42F',
		html: '&#128047'
	},
	{
		emoji: '🐅',
		name: 'Tiger',
		hexcode: 'U+1F405',
		html: '&#128005'
	},
	{
		emoji: '🐆',
		name: 'Leopard',
		hexcode: 'U+1F406',
		html: '&#128006'
	},
	{
		emoji: '🐴',
		name: 'Horse Face',
		hexcode: 'U+1F434',
		html: '&#128052'
	},
	{
		emoji: '🐎',
		name: 'Horse',
		hexcode: 'U+1F40E',
		html: '&#128014'
	},
	{
		emoji: '🦄',
		name: 'Unicorn Face',
		hexcode: 'U+1F984',
		html: '&#129412'
	},
	{
		emoji: '🦓',
		name: 'Zebra',
		hexcode: 'U+1F993',
		html: '&#129427'
	},
	{
		emoji: '🦌',
		name: 'Deer',
		hexcode: 'U+1F98C',
		html: '&#129420'
	},
	{
		emoji: '🐮',
		name: 'Cow Face',
		hexcode: 'U+1F42E',
		html: '&#128046'
	},
	{
		emoji: '🐂',
		name: 'Ox',
		hexcode: 'U+1F402',
		html: '&#128002'
	},
	{
		emoji: '🐃',
		name: 'Water Buffalo',
		hexcode: 'U+1F403',
		html: '&#128003'
	},
	{
		emoji: '🐄',
		name: 'Cow',
		hexcode: 'U+1F404',
		html: '&#128004'
	},
	{
		emoji: '🐷',
		name: 'Pig Face',
		hexcode: 'U+1F437',
		html: '&#128055'
	},
	{
		emoji: '🐖',
		name: 'Pig',
		hexcode: 'U+1F416',
		html: '&#128022'
	},
	{
		emoji: '🐗',
		name: 'Boar',
		hexcode: 'U+1F417',
		html: '&#128023'
	},
	{
		emoji: '🐽',
		name: 'Pig Nose',
		hexcode: 'U+1F43D',
		html: '&#128061'
	},
	{
		emoji: '🐏',
		name: 'Ram',
		hexcode: 'U+1F40F',
		html: '&#128015'
	},
	{
		emoji: '🐑',
		name: 'Ewe',
		hexcode: 'U+1F411',
		html: '&#128017'
	},
	{
		emoji: '🐐',
		name: 'Goat',
		hexcode: 'U+1F410',
		html: '&#128016'
	},
	{
		emoji: '🐪',
		name: 'Camel',
		hexcode: 'U+1F42A',
		html: '&#128042'
	},
	{
		emoji: '🐫',
		name: 'Two-Hump Camel',
		hexcode: 'U+1F42B',
		html: '&#128043'
	},
	{
		emoji: '🦙',
		name: 'Lama',
		hexcode: 'U+1F999',
		html: '&#129433'
	},
	{
		emoji: '🦒',
		name: 'Giraffe',
		hexcode: 'U+1F992',
		html: '&#129426'
	},
	{
		emoji: '🐘',
		name: 'Elephant',
		hexcode: 'U+1F418',
		html: '&#128024'
	},
	{
		emoji: '🦏',
		name: 'Rhinoceros',
		hexcode: 'U+1F98F',
		html: '&#129423'
	},
	{
		emoji: '🦛',
		name: 'Hippopotamus',
		hexcode: 'U+1F99B',
		html: '&#129435'
	},
	{
		emoji: '🐭',
		name: 'Mouse Face',
		hexcode: 'U+1F42D',
		html: '&#128045'
	},
	{
		emoji: '🐁',
		name: 'Mouse',
		hexcode: 'U+1F401',
		html: '&#128001'
	},
	{
		emoji: '🐀',
		name: 'Rat',
		hexcode: 'U+1F400',
		html: '&#128000'
	},
	{
		emoji: '🐹',
		name: 'Hamster Face',
		hexcode: 'U+1F439',
		html: '&#128057'
	},
	{
		emoji: '🐰',
		name: 'Rabbit Face',
		hexcode: 'U+1F430',
		html: '&#128048'
	},
	{
		emoji: '🐇',
		name: 'Rabbit',
		hexcode: 'U+1F407',
		html: '&#128007'
	},
	{
		emoji: '🐿',
		name: 'Chipmunk',
		hexcode: 'U+1F43F',
		html: '&#128063'
	},
	{
		emoji: '🦔',
		name: 'Hedgehog',
		hexcode: 'U+1F994',
		html: '&#129428'
	},
	{
		emoji: '🦇',
		name: 'Bat',
		hexcode: 'U+1F987',
		html: '&#129415'
	},
	{
		emoji: '🐻',
		name: 'Bear Face',
		hexcode: 'U+1F43B',
		html: '&#128059'
	},
	{
		emoji: '🐨',
		name: 'Koala',
		hexcode: 'U+1F428',
		html: '&#128040'
	},
	{
		emoji: '🐼',
		name: 'Panda Face',
		hexcode: 'U+1F43C',
		html: '&#128060'
	},
	{
		emoji: '🦘',
		name: 'Kangaroo',
		hexcode: 'U+1F998',
		html: '&#129432'
	},
	{
		emoji: '🦡',
		name: 'Badger',
		hexcode: 'U+1F9A1',
		html: '&#129441'
	},
	{
		emoji: '🐾',
		name: 'Paw Prints',
		hexcode: 'U+1F43E',
		html: '&#128062'
	},
	{
		emoji: '🦃',
		name: 'Turkey',
		hexcode: 'U+1F983',
		html: '&#129411'
	},
	{
		emoji: '🐔',
		name: 'Chicken',
		hexcode: 'U+1F414',
		html: '&#128020'
	},
	{
		emoji: '🐓',
		name: 'Rooster',
		hexcode: 'U+1F413',
		html: '&#128019'
	},
	{
		emoji: '🐣',
		name: 'Hatching Chick',
		hexcode: 'U+1F423',
		html: '&#128035'
	},
	{
		emoji: '🐤',
		name: 'Baby Chick',
		hexcode: 'U+1F424',
		html: '&#128036'
	},
	{
		emoji: '🐥',
		name: 'Baby Chick',
		hexcode: 'U+1F425',
		html: '&#128037'
	},
	{
		emoji: '🐦',
		name: 'Bird',
		hexcode: 'U+1F426',
		html: '&#128038'
	},
	{
		emoji: '🐧',
		name: 'Penguin',
		hexcode: 'U+1F427',
		html: '&#128039'
	},
	{
		emoji: '🕊',
		name: 'Dove',
		hexcode: 'U+1F54A',
		html: '&#128330'
	},
	{
		emoji: '🦅',
		name: 'Eagle',
		hexcode: 'U+1F985',
		html: '&#129413'
	},
	{
		emoji: '🦆',
		name: 'Duck',
		hexcode: 'U+1F986',
		html: '&#129414'
	},
	{
		emoji: '🦢',
		name: 'Swan',
		hexcode: 'U+1F9A2',
		html: '&#129442'
	},
	{
		emoji: '🦉',
		name: 'Owl',
		hexcode: 'U+1F989',
		html: '&#129417'
	},
	{
		emoji: '🦚',
		name: 'Peacock',
		hexcode: 'U+1F99A',
		html: '&#129434'
	},
	{
		emoji: '🦜',
		name: 'Parrot',
		hexcode: 'U+1F99C',
		html: '&#129436'
	},
	{
		emoji: '🐸',
		name: 'Frog Face',
		hexcode: 'U+1F438',
		html: '&#128056'
	},
	{
		emoji: '🐊',
		name: 'Crocodile',
		hexcode: 'U+1F40A',
		html: '&#128010'
	},
	{
		emoji: '🐢',
		name: 'Turtle',
		hexcode: 'U+1F422',
		html: '&#128034'
	},
	{
		emoji: '🦎',
		name: 'Lizard',
		hexcode: 'U+1F98E',
		html: '&#129422'
	},
	{
		emoji: '🐍',
		name: 'Snake',
		hexcode: 'U+1F40D',
		html: '&#128013'
	},
	{
		emoji: '🐲',
		name: 'Dragon Face',
		hexcode: 'U+1F432',
		html: '&#128050'
	},
	{
		emoji: '🐉',
		name: 'Dragon',
		hexcode: 'U+1F409',
		html: '&#128009'
	},
	{
		emoji: '🦕',
		name: 'Sauropod',
		hexcode: 'U+1F995',
		html: '&#129429'
	},
	{
		emoji: '🦖',
		name: 'T-Rex',
		hexcode: 'U+1F996',
		html: '&#129430'
	},
	{
		emoji: '🐳',
		name: 'Spouting Whale',
		hexcode: 'U+1F433',
		html: '&#128051'
	},
	{
		emoji: '🐋',
		name: 'Whale',
		hexcode: 'U+1F40B',
		html: '&#128011'
	},
	{
		emoji: '🐬',
		name: 'Dolphin',
		hexcode: 'U+1F42C',
		html: '&#128044'
	},
	{
		emoji: '🐟',
		name: 'Fish',
		hexcode: 'U+1F41F',
		html: '&#128031'
	},
	{
		emoji: '🐠',
		name: 'Tropical Fish',
		hexcode: 'U+1F420',
		html: '&#128032'
	},
	{
		emoji: '🐡',
		name: 'Blowfish',
		hexcode: 'U+1F421',
		html: '&#128033'
	},
	{
		emoji: '🦈',
		name: 'Shark',
		hexcode: 'U+1F988',
		html: '&#129416'
	},
	{
		emoji: '🐙',
		name: 'Octopus',
		hexcode: 'U+1F419',
		html: '&#128025'
	},
	{
		emoji: '🐚',
		name: 'Spiral Shell',
		hexcode: 'U+1F41A',
		html: '&#128026'
	},
	{
		emoji: '🐌',
		name: 'Snail',
		hexcode: 'U+1F40C',
		html: '&#128012'
	},
	{
		emoji: '🦋',
		name: 'Butterfly',
		hexcode: 'U+1F98B',
		html: '&#129419'
	},
	{
		emoji: '🐛',
		name: 'Bug',
		hexcode: 'U+1F41B',
		html: '&#128027'
	},
	{
		emoji: '🐜',
		name: 'Ant',
		hexcode: 'U+1F41C',
		html: '&#128028'
	},
	{
		emoji: '🐝',
		name: 'Honeybee',
		hexcode: 'U+1F41D',
		html: '&#128029'
	},
	{
		emoji: '🐞',
		name: 'Lady Beetle',
		hexcode: 'U+1F41E',
		html: '&#128030'
	},
	{
		emoji: '🦗',
		name: 'Cricket',
		hexcode: 'U+1F997',
		html: '&#129431'
	},
	{
		emoji: '🕷',
		name: 'Spider',
		hexcode: 'U+1F577',
		html: '&#128375'
	},
	{
		emoji: '🕸',
		name: 'Spider Web',
		hexcode: 'U+1F578',
		html: '&#128376'
	},
	{
		emoji: '🦂',
		name: 'Scorpion',
		hexcode: 'U+1F982',
		html: '&#129410'
	},
	{
		emoji: '🦟',
		name: 'Mosquito',
		hexcode: 'U+1F99F',
		html: '&#129439'
	},
	{
		emoji: '🦠',
		name: 'Microbe',
		hexcode: 'U+1F9A0',
		decccode: '&#129440'
	},
	{
    emoji: '💪',
    name: 'Flexed Biceps',
    hexcode: 'U+1F4AA',
    html: '&#128170'
	},
	{
    emoji: '🦾',
    name: 'Mechanical Arm',
    hexcode: 'U+1F9BE',
    html: '&#129470'
	},
	{
    emoji: '🦿',
    name: 'Mechanical Leg',
    hexcode: 'U+1F9BF',
    html: '&#129471'
	},
	{
    emoji: '🦵',
    name: 'Leg',
    hexcode: 'U+1F9B5',
    html: '&#129461'
	},
	{
    emoji: '🦶',
    name: 'Foot',
    hexcode: 'U+1F9B6',
    html: '&#129462'
	},
	{
    emoji: '👂',
    name: 'Ear',
    hexcode: 'U+1F442',
    html: '&#128066'
	},
	{
    emoji: '🦻',
    name: 'Ear With Hearing Aid',
    hexcode: 'U+1F9BB',
    html: '&#129467'
	},
	{
    emoji: '👃',
    name: 'Nose',
    hexcode: 'U+1F443',
    html: '&#128067'
	},
	{
    emoji: '🧠',
    name: 'Brain',
    hexcode: 'U+1F9E0',
    html: '&#129504'
	},
	{
    emoji: '🦷',
    name: 'Tooth',
    hexcode: 'U+1F9B7',
    html: '&#129463'
	},
	{
    emoji: '🦴',
    name: 'Bone',
    hexcode: 'U+1F9B4',
    html: '&#129460'
	},
	{
    emoji: '👀',
    name: 'Eyes',
    hexcode: 'U+1F440',
    html: '&#128064'
	},
	{
    emoji: '👁',
    name: 'Eye',
    hexcode: 'U+1F441',
    html: '&#128065'
	},
	{
    emoji: '👅',
    name: 'Tongue',
    hexcode: 'U+1F445',
    html: '&#128069'
	},
	{
    emoji: '👄',
    name: 'Mouth',
    hexcode: 'U+1F444',
    html: '&#128068'
  },
  {
		emoji: '🏁',
		name: 'Chequered Flag',
		hexcode: 'U+1F3C1',
		html: '&#127937'
  },
  {
		emoji: '🚩',
		name: 'Triangular Flag',
		hexcode: 'U+1F6A9',
		html: '&#128681'
  },
  {
		emoji: '🎌',
		name: 'Crossed Flags',
		hexcode: 'U+1F38C',
		html: '&#127884'
  },
  {
		emoji: '🏴',
		name: 'Black Flag',
		hexcode: 'U+1F3F4',
		html: '&#127988'
  },
  {
		emoji: '🏳',
		name: 'White Flag',
		hexcode: 'U+1F3F3',
		html: '&#127987'
  },
  {
		emoji: '🏳️‍🌈',
		name: 'Rainbow Flag',
		hexcode: 'U+1F3F3 U+FE0F U+200D U+1F308',
		html: '&#127987 &#65039 &#8205 &#127752'
  },
  {
		emoji: '🏴‍☠️',
		name: 'Pirate Flag',
		hexcode: 'U+1F3F4 U+200D U+2620 U+FE0F',
		html: '&#127988 &#8205 &#9760 &#65039'
  },
  {
    emoji: '🇦🇨',
    name: 'Ascension Island', 
    hexcode: 'U+1F1E6 U+1F1E8',
    html: '&#127462 &#127464'
  },
  {
    emoji: '🇦🇩',
    name: 'Andorra', 
    hexcode: 'U+1F1E6 U+1F1E9',
    html: '&#127462 &#127465'
  },
  {
    emoji: '🇦🇪',
    name: 'United Arab Emirates', 
    hexcode: 'U+1F1E6 U+1F1EA',
    html: '&#127462 &#127466'
  },
  {
    emoji: '🇦🇫',
    name: 'Afghanistan', 
    hexcode: 'U+1F1E6 U+1F1EB',
    html: '&#127462 &#127467'
  },
  {
    emoji: '🇦🇬',
    name: 'Antigua & Barbuda', 
    hexcode: 'U+1F1E6 U+1F1EC',
    html: '&#127462 &#127468'
  },
  {
    emoji: '🇦🇮',
    name: 'Anguilla', 
    hexcode: 'U+1F1E6 U+1F1EE',
    html: '&#127462 &#127470'
  },
  {
    emoji: '🇦🇱',
    name: 'Albania', 
    hexcode: 'U+1F1E6 U+1F1F1',
    html: '&#127462 &#127473'
  },
  {
    emoji: '🇦🇲',
    name: 'Armenia', 
    hexcode: 'U+1F1E6 U+1F1F2',
    html: '&#127462 &#127474'
  },
  {
    emoji: '🇦🇴',
    name: 'Angola', 
    hexcode: 'U+1F1E6 U+1F1F4',
    html: '&#127462 &#127476'
  },
  {
    emoji: '🇦🇶',
    name: 'Antarctica', 
    hexcode: 'U+1F1E6 U+1F1F6',
    html: '&#127462 &#127478'
  },
  {
    emoji: '🇦🇷',
    name: 'Argentina', 
    hexcode: 'U+1F1E6 U+1F1F7',
    html: '&#127462 &#127479'
  },
  {
    emoji: '🇦🇸',
    name: 'American Samoa', 
    hexcode: 'U+1F1E6 U+1F1F8',
    html: '&#127462 &#127480'
  },
  {
    emoji: '🇦🇹',
    name: 'Austria', 
    hexcode: 'U+1F1E6 U+1F1F9',
    html: '&#127462 &#127481'
  },
  {
    emoji: '🇦🇺',
    name: 'Australia', 
    hexcode: 'U+1F1E6 U+1F1FA',
    html: '&#127462 &#127482'
  },
  {
    emoji: '🇦🇼',
    name: 'Aruba', 
    hexcode: 'U+1F1E6 U+1F1FC',
    html: '&#127462 &#127484'
  },
  {
    emoji: '🇦🇽',
    name: 'Åland Islands', 
    hexcode: 'U+1F1E6 U+1F1FD',
    html: '&#127462 &#127485'
  },
  {
    emoji: '🇦🇿',
    name: 'Azerbaijan', 
    hexcode: 'U+1F1E6 U+1F1FF',
    html: '&#127462 &#127487'
  },
  {
    emoji: '🇧🇦',
    name: 'Bosnia & Herzegovina', 
    hexcode: 'U+1F1E7 U+1F1E6',
    html: '&#127463 &#127462'
  },
  {
    emoji: '🇧🇧',
    name: 'Barbados', 
    hexcode: 'U+1F1E7 U+1F1E7',
    html: '&#127463 &#127463'
  },
  {
    emoji: '🇧🇩',
    name: 'Bangladesh', 
    hexcode: 'U+1F1E7 U+1F1E9',
    html: '&#127463 &#127465'
  },
  {
    emoji: '🇧🇪',
    name: 'Belgium', 
    hexcode: 'U+1F1E7 U+1F1EA',
    html: '&#127463 &#127466'
  },
  {
    emoji: '🇧🇫',
    name: 'Burkina Faso', 
    hexcode: 'U+1F1E7 U+1F1EB',
    html: '&#127463 &#127467'
  },
  {
    emoji: '🇧🇬',
    name: 'Bulgaria', 
    hexcode: 'U+1F1E7 U+1F1EC',
    html: '&#127463 &#127468'
  },
  {
    emoji: '🇧🇭',
    name: 'Bahrain', 
    hexcode: 'U+1F1E7 U+1F1ED',
    html: '&#127463 &#127469'
  },
  {
    emoji: '🇧🇮',
    name: 'Burundi', 
    hexcode: 'U+1F1E7 U+1F1EE',
    html: '&#127463 &#127470'
  },
  {
    emoji: '🇧🇯',
    name: 'Benin', 
    hexcode: 'U+1F1E7 U+1F1EF',
    html: '&#127463 &#127471'
  },
  {
    emoji: '🇧🇱',
    name: 'St. Barthélemy', 
    hexcode: 'U+1F1E7 U+1F1F1',
    html: '&#127463 &#127473'
  },
  {
    emoji: '🇧🇲',
    name: 'Bermuda', 
    hexcode: 'U+1F1E7 U+1F1F2',
    html: '&#127463 &#127474'
  },
  {
    emoji: '🇧🇳',
    name: 'Brunei', 
    hexcode: 'U+1F1E7 U+1F1F3',
    html: '&#127463 &#127475'
  },
  {
    emoji: '🇧🇴',
    name: 'Bolivia', 
    hexcode: 'U+1F1E7 U+1F1F4',
    html: '&#127463 &#127476'
  },
  {
    emoji: '🇧🇶',
    name: 'Caribbean Netherlands', 
    hexcode: 'U+1F1E7 U+1F1F6',
    html: '&#127463 &#127478'
  },
  {
    emoji: '🇧🇷',
    name: 'Brazil', 
    hexcode: 'U+1F1E7 U+1F1F7',
    html: '&#127463 &#127479'
  },
  {
    emoji: '🇧🇸',
    name: 'Bahamas', 
    hexcode: 'U+1F1E7 U+1F1F8',
    html: '&#127463 &#127480'
  },
  {
    emoji: '🇧🇹',
    name: 'Bhutan', 
    hexcode: 'U+1F1E7 U+1F1F9',
    html: '&#127463 &#127481'
  },
  {
    emoji: '🇧🇻',
    name: 'Bouvet Island', 
    hexcode: 'U+1F1E7 U+1F1FB',
    html: '&#127463 &#127483'
  },
  {
    emoji: '🇧🇼',
    name: 'Botswana', 
    hexcode: 'U+1F1E7 U+1F1FC',
    html: '&#127463 &#127484'
  },
  {
    emoji: '🇧🇾',
    name: 'Belarus', 
    hexcode: 'U+1F1E7 U+1F1FE',
    html: '&#127463 &#127486'
  },
  {
    emoji: '🇧🇿',
    name: 'Belize', 
    hexcode: 'U+1F1E7 U+1F1FF',
    html: '&#127463 &#127487'
  },
  {
    emoji: '🇨🇦',
    name: 'Canada', 
    hexcode: 'U+1F1E8 U+1F1E6',
    html: '&#127464 &#127462'
  },
  {
    emoji: '🇨🇨',
    name: 'Cocos (Keeling) Islands', 
    hexcode: 'U+1F1E8 U+1F1E8',
    html: '&#127464 &#127464'
  },
  {
    emoji: '🇨🇩',
    name: 'Congo - Kinshasa', 
    hexcode: 'U+1F1E8 U+1F1E9',
    html: '&#127464 &#127465'
  },
  {
    emoji: '🇨🇫',
    name: 'Central African Republic', 
    hexcode: 'U+1F1E8 U+1F1EB',
    html: '&#127464 &#127467'
  },
  {
    emoji: '🇨🇬',
    name: 'Congo - Brazzaville', 
    hexcode: 'U+1F1E8 U+1F1EC',
    html: '&#127464 &#127468'
  },
  {
    emoji: '🇨🇭',
    name: 'Switzerland', 
    hexcode: 'U+1F1E8 U+1F1ED',
    html: '&#127464 &#127469'
  },
  {
    emoji: '🇨🇮',
    name: 'Côte d’Ivoire', 
    hexcode: 'U+1F1E8 U+1F1EE',
    html: '&#127464 &#127470'
  },
  {
    emoji: '🇨🇰',
    name: 'Cook Islands', 
    hexcode: 'U+1F1E8 U+1F1F0',
    html: '&#127464 &#127472'
  },
  {
    emoji: '🇨🇱',
    name: 'Chile', 
    hexcode: 'U+1F1E8 U+1F1F1',
    html: '&#127464 &#127473'
  },
  {
    emoji: '🇨🇲',
    name: 'Cameroon', 
    hexcode: 'U+1F1E8 U+1F1F2',
    html: '&#127464 &#127474'
  },
  {
    emoji: '🇨🇳',
    name: 'China', 
    hexcode: 'U+1F1E8 U+1F1F3',
    html: '&#127464 &#127475'
  },
  {
    emoji: '🇨🇴',
    name: 'Colombia', 
    hexcode: 'U+1F1E8 U+1F1F4',
    html: '&#127464 &#127476'
  },
  {
    emoji: '🇨🇵',
    name: 'Clipperton Island', 
    hexcode: 'U+1F1E8 U+1F1F5',
    html: '&#127464 &#127477'
  },
  {
    emoji: '🇨🇷',
    name: 'Costa Rica', 
    hexcode: 'U+1F1E8 U+1F1F7',
    html: '&#127464 &#127479'
  },
  {
    emoji: '🇨🇺',
    name: 'Cuba', 
    hexcode: 'U+1F1E8 U+1F1FA',
    html: '&#127464 &#127482'
  },
  {
    emoji: '🇨🇻',
    name: 'Cape Verde', 
    hexcode: 'U+1F1E8 U+1F1FB',
    html: '&#127464 &#127483'
  },
  {
    emoji: '🇨🇼',
    name: 'Curaçao', 
    hexcode: 'U+1F1E8 U+1F1FC',
    html: '&#127464 &#127484'
  },
  {
    emoji: '🇨🇽',
    name: 'Christmas Island', 
    hexcode: 'U+1F1E8 U+1F1FD',
    html: '&#127464 &#127485'
  },
  {
    emoji: '🇨🇾',
    name: 'Cyprus', 
    hexcode: 'U+1F1E8 U+1F1FE',
    html: '&#127464 &#127486'
  },
  {
    emoji: '🇨🇿',
    name: 'Czechia', 
    hexcode: 'U+1F1E8 U+1F1FF',
    html: '&#127464 &#127487'
  },
  {
    emoji: '🇩🇪',
    name: 'Germany', 
    hexcode: 'U+1F1E9 U+1F1EA',
    html: '&#127465 &#127466'
  },
  {
    emoji: '🇩🇬',
    name: 'Diego Garcia', 
    hexcode: 'U+1F1E9 U+1F1EC',
    html: '&#127465 &#127468'
  },
  {
    emoji: '🇩🇯',
    name: 'Djibouti', 
    hexcode: 'U+1F1E9 U+1F1EF',
    html: '&#127465 &#127471'
  },
  {
    emoji: '🇩🇰',
    name: 'Denmark', 
    hexcode: 'U+1F1E9 U+1F1F0',
    html: '&#127465 &#127472'
  },
  {
    emoji: '🇩🇲',
    name: 'Dominica', 
    hexcode: 'U+1F1E9 U+1F1F2',
    html: '&#127465 &#127474'
  },
  {
    emoji: '🇩🇴',
    name: 'Dominican Republic', 
    hexcode: 'U+1F1E9 U+1F1F4',
    html: '&#127465 &#127476'
  },
  {
    emoji: '🇩🇿',
    name: 'Algeria', 
    hexcode: 'U+1F1E9 U+1F1FF',
    html: '&#127465 &#127487'
  },
  {
    emoji: '🇪🇦',
    name: 'Ceuta & Melilla', 
    hexcode: 'U+1F1EA U+1F1E6',
    html: '&#127466 &#127462'
  },
  {
    emoji: '🇪🇨',
    name: 'Ecuador', 
    hexcode: 'U+1F1EA U+1F1E8',
    html: '&#127466 &#127464'
  },
  {
    emoji: '🇪🇪',
    name: 'Estonia', 
    hexcode: 'U+1F1EA U+1F1EA',
    html: '&#127466 &#127466'
  },
  {
    emoji: '🇪🇬',
    name: 'Egypt', 
    hexcode: 'U+1F1EA U+1F1EC',
    html: '&#127466 &#127468'
  },
  {
    emoji: '🇪🇭',
    name: 'Western Sahara', 
    hexcode: 'U+1F1EA U+1F1ED',
    html: '&#127466 &#127469'
  },
  {
    emoji: '🇪🇷',
    name: 'Eritrea', 
    hexcode: 'U+1F1EA U+1F1F7',
    html: '&#127466 &#127479'
  },
  {
    emoji: '🇪🇸',
    name: 'Spain', 
    hexcode: 'U+1F1EA U+1F1F8',
    html: '&#127466 &#127480'
  },
  {
    emoji: '🇪🇹',
    name: 'Ethiopia', 
    hexcode: 'U+1F1EA U+1F1F9',
    html: '&#127466 &#127481'
  },
  {
    emoji: '🇪🇺',
    name: 'European Union', 
    hexcode: 'U+1F1EA U+1F1FA',
    html: '&#127466 &#127482'
  },
  {
    emoji: '🇫🇮',
    name: 'Finland', 
    hexcode: 'U+1F1EB U+1F1EE',
    html: '&#127467 &#127470'
  },
  {
    emoji: '🇫🇯',
    name: 'Fiji', 
    hexcode: 'U+1F1EB U+1F1EF',
    html: '&#127467 &#127471'
  },
  {
    emoji: '🇫🇰',
    name: 'Falkland Islands', 
    hexcode: 'U+1F1EB U+1F1F0',
    html: '&#127467 &#127472'
  },
  {
    emoji: '🇫🇲',
    name: 'Micronesia', 
    hexcode: 'U+1F1EB U+1F1F2',
    html: '&#127467 &#127474'
  },
  {
    emoji: '🇫🇴',
    name: 'Faroe Islands', 
    hexcode: 'U+1F1EB U+1F1F4',
    html: '&#127467 &#127476'
  },
  {
    emoji: '🇫🇷',
    name: 'France', 
    hexcode: 'U+1F1EB U+1F1F7',
    html: '&#127467 &#127479'
  },
  {
    emoji: '🇬🇦',
    name: 'Gabon', 
    hexcode: 'U+1F1EC U+1F1E6',
    html: '&#127468 &#127462'
  },
  {
    emoji: '🇬🇧',
    name: 'United Kingdom', 
    hexcode: 'U+1F1EC U+1F1E7',
    html: '&#127468 &#127463'
  },
  {
    emoji: '🇬🇩',
    name: 'Grenada', 
    hexcode: 'U+1F1EC U+1F1E9',
    html: '&#127468 &#127465'
  },
  {
    emoji: '🇬🇪',
    name: 'Georgia', 
    hexcode: 'U+1F1EC U+1F1EA',
    html: '&#127468 &#127466'
  },
  {
    emoji: '🇬🇫',
    name: 'French Guiana', 
    hexcode: 'U+1F1EC U+1F1EB',
    html: '&#127468 &#127467'
  },
  {
    emoji: '🇬🇬',
    name: 'Guernsey', 
    hexcode: 'U+1F1EC U+1F1EC',
    html: '&#127468 &#127468'
  },
  {
    emoji: '🇬🇭',
    name: 'Ghana', 
    hexcode: 'U+1F1EC U+1F1ED',
    html: '&#127468 &#127469'
  },
  {
    emoji: '🇬🇮',
    name: 'Gibraltar', 
    hexcode: 'U+1F1EC U+1F1EE',
    html: '&#127468 &#127470'
  },
  {
    emoji: '🇬🇱',
    name: 'Greenland', 
    hexcode: 'U+1F1EC U+1F1F1',
    html: '&#127468 &#127473'
  },
  {
    emoji: '🇬🇲',
    name: 'Gambia', 
    hexcode: 'U+1F1EC U+1F1F2',
    html: '&#127468 &#127474'
  },
  {
    emoji: '🇬🇳',
    name: 'Guinea', 
    hexcode: 'U+1F1EC U+1F1F3',
    html: '&#127468 &#127475'
  },
  {
    emoji: '🇬🇵',
    name: 'Guadeloupe', 
    hexcode: 'U+1F1EC U+1F1F5',
    html: '&#127468 &#127477'
  },
  {
    emoji: '🇬🇶',
    name: 'Equatorial Guinea', 
    hexcode: 'U+1F1EC U+1F1F6',
    html: '&#127468 &#127478'
  },
  {
    emoji: '🇬🇷',
    name: 'Greece', 
    hexcode: 'U+1F1EC U+1F1F7',
    html: '&#127468 &#127479'
  },
  {
    emoji: '🇬🇸',
    name: 'South Georgia & South Sandwich Islands', 
    hexcode: 'U+1F1EC U+1F1F8',
    html: '&#127468 &#127480'
  },
  {
    emoji: '🇬🇹',
    name: 'Guatemala', 
    hexcode: 'U+1F1EC U+1F1F9',
    html: '&#127468 &#127481'
  },
  {
    emoji: '🇬🇺',
    name: 'Guam', 
    hexcode: 'U+1F1EC U+1F1FA',
    html: '&#127468 &#127482'
  },
  {
    emoji: '🇬🇼',
    name: 'Guinea-Bissau', 
    hexcode: 'U+1F1EC U+1F1FC',
    html: '&#127468 &#127484'
  },
  {
    emoji: '🇬🇾',
    name: 'Guyana', 
    hexcode: 'U+1F1EC U+1F1FE',
    html: '&#127468 &#127486'
  },
  {
    emoji: '🇭🇰',
    name: 'Hong Kong SAR China', 
    hexcode: 'U+1F1ED U+1F1F0',
    html: '&#127469 &#127472'
  },
  {
    emoji: '🇭🇲',
    name: 'Heard & McDonald Islands', 
    hexcode: 'U+1F1ED U+1F1F2',
    html: '&#127469 &#127474'
  },
  {
    emoji: '🇭🇳',
    name: 'Honduras', 
    hexcode: 'U+1F1ED U+1F1F3',
    html: '&#127469 &#127475'
  },
  {
    emoji: '🇭🇷',
    name: 'Croatia', 
    hexcode: 'U+1F1ED U+1F1F7',
    html: '&#127469 &#127479'
  },
  {
    emoji: '🇭🇹',
    name: 'Haiti', 
    hexcode: 'U+1F1ED U+1F1F9',
    html: '&#127469 &#127481'
  },
  {
    emoji: '🇭🇺',
    name: 'Hungary', 
    hexcode: 'U+1F1ED U+1F1FA',
    html: '&#127469 &#127482'
  },
  {
    emoji: '🇮🇨',
    name: 'Canary Islands', 
    hexcode: 'U+1F1EE U+1F1E8',
    html: '&#127470 &#127464'
  },
  {
    emoji: '🇮🇩',
    name: 'Indonesia', 
    hexcode: 'U+1F1EE U+1F1E9',
    html: '&#127470 &#127465'
  },
  {
    emoji: '🇮🇪',
    name: 'Ireland', 
    hexcode: 'U+1F1EE U+1F1EA',
    html: '&#127470 &#127466'
  },
  {
    emoji: '🇮🇱',
    name: 'Israel', 
    hexcode: 'U+1F1EE U+1F1F1',
    html: '&#127470 &#127473'
  },
  {
    emoji: '🇮🇲',
    name: 'Isle of Man', 
    hexcode: 'U+1F1EE U+1F1F2',
    html: '&#127470 &#127474'
  },
  {
    emoji: '🇮🇳',
    name: 'India', 
    hexcode: 'U+1F1EE U+1F1F3',
    html: '&#127470 &#127475'
  },
  {
    emoji: '🇮🇴',
    name: 'British Indian Ocean Territory', 
    hexcode: 'U+1F1EE U+1F1F4',
    html: '&#127470 &#127476'
  },
  {
    emoji: '🇮🇶',
    name: 'Iraq', 
    hexcode: 'U+1F1EE U+1F1F6',
    html: '&#127470 &#127478'
  },
  {
    emoji: '🇮🇷',
    name: 'Iran', 
    hexcode: 'U+1F1EE U+1F1F7',
    html: '&#127470 &#127479'
  },
  {
    emoji: '🇮🇸',
    name: 'Iceland', 
    hexcode: 'U+1F1EE U+1F1F8',
    html: '&#127470 &#127480'
  },
  {
    emoji: '🇮🇹',
    name: 'Italy', 
    hexcode: 'U+1F1EE U+1F1F9',
    html: '&#127470 &#127481'
  },
  {
    emoji: '🇯🇪',
    name: 'Jersey', 
    hexcode: 'U+1F1EF U+1F1EA',
    html: '&#127471 &#127466'
  },
  {
    emoji: '🇯🇲',
    name: 'Jamaica', 
    hexcode: 'U+1F1EF U+1F1F2',
    html: '&#127471 &#127474'
  },
  {
    emoji: '🇯🇴',
    name: 'Jordan', 
    hexcode: 'U+1F1EF U+1F1F4',
    html: '&#127471 &#127476'
  },
  {
    emoji: '🇯🇵',
    name: 'Japan', 
    hexcode: 'U+1F1EF U+1F1F5',
    html: '&#127471 &#127477'
  },
  {
    emoji: '🇰🇪',
    name: 'Kenya', 
    hexcode: 'U+1F1F0 U+1F1EA',
    html: '&#127472 &#127466'
  },
  {
    emoji: '🇰🇬',
    name: 'Kyrgyzstan', 
    hexcode: 'U+1F1F0 U+1F1EC',
    html: '&#127472 &#127468'
  },
  {
    emoji: '🇰🇭',
    name: 'Cambodia', 
    hexcode: 'U+1F1F0 U+1F1ED',
    html: '&#127472 &#127469'
  },
  {
    emoji: '🇰🇮',
    name: 'Kiribati', 
    hexcode: 'U+1F1F0 U+1F1EE',
    html: '&#127472 &#127470'
  },
  {
    emoji: '🇰🇲',
    name: 'Comoros', 
    hexcode: 'U+1F1F0 U+1F1F2',
    html: '&#127472 &#127474'
  },
  {
    emoji: '🇰🇳',
    name: 'St. Kitts & Nevis', 
    hexcode: 'U+1F1F0 U+1F1F3',
    html: '&#127472 &#127475'
  },
  {
    emoji: '🇰🇵',
    name: 'North Korea', 
    hexcode: 'U+1F1F0 U+1F1F5',
    html: '&#127472 &#127477'
  },
  {
    emoji: '🇰🇷',
    name: 'South Korea', 
    hexcode: 'U+1F1F0 U+1F1F7',
    html: '&#127472 &#127479'
  },
  {
    emoji: '🇰🇼',
    name: 'Kuwait', 
    hexcode: 'U+1F1F0 U+1F1FC',
    html: '&#127472 &#127484'
  },
  {
    emoji: '🇰🇾',
    name: 'Cayman Islands', 
    hexcode: 'U+1F1F0 U+1F1FE',
    html: '&#127472 &#127486'
  },
  {
    emoji: '🇰🇿',
    name: 'Kazakhstan', 
    hexcode: 'U+1F1F0 U+1F1FF',
    html: '&#127472 &#127487'
  },
  {
    emoji: '🇱🇦',
    name: 'Laos', 
    hexcode: 'U+1F1F1 U+1F1E6',
    html: '&#127473 &#127462'
  },
  {
    emoji: '🇱🇧',
    name: 'Lebanon', 
    hexcode: 'U+1F1F1 U+1F1E7',
    html: '&#127473 &#127463'
  },
  {
    emoji: '🇱🇨',
    name: 'St. Lucia', 
    hexcode: 'U+1F1F1 U+1F1E8',
    html: '&#127473 &#127464'
  },
  {
    emoji: '🇱🇮',
    name: 'Liechtenstein', 
    hexcode: 'U+1F1F1 U+1F1EE',
    html: '&#127473 &#127470'
  },
  {
    emoji: '🇱🇰',
    name: 'Sri Lanka', 
    hexcode: 'U+1F1F1 U+1F1F0',
    html: '&#127473 &#127472'
  },
  {
    emoji: '🇱🇷',
    name: 'Liberia', 
    hexcode: 'U+1F1F1 U+1F1F7',
    html: '&#127473 &#127479'
  },
  {
    emoji: '🇱🇸',
    name: 'Lesotho', 
    hexcode: 'U+1F1F1 U+1F1F8',
    html: '&#127473 &#127480'
  },
  {
    emoji: '🇱🇹',
    name: 'Lithuania', 
    hexcode: 'U+1F1F1 U+1F1F9',
    html: '&#127473 &#127481'
  },
  {
    emoji: '🇱🇺',
    name: 'Luxembourg', 
    hexcode: 'U+1F1F1 U+1F1FA',
    html: '&#127473 &#127482'
  },
  {
    emoji: '🇱🇻',
    name: 'Latvia', 
    hexcode: 'U+1F1F1 U+1F1FB',
    html: '&#127473 &#127483'
  },
  {
    emoji: '🇱🇾',
    name: 'Libya', 
    hexcode: 'U+1F1F1 U+1F1FE',
    html: '&#127473 &#127486'
  },
  {
    emoji: '🇲🇦',
    name: 'Morocco', 
    hexcode: 'U+1F1F2 U+1F1E6',
    html: '&#127474 &#127462'
  },
  {
    emoji: '🇲🇨',
    name: 'Monaco', 
    hexcode: 'U+1F1F2 U+1F1E8',
    html: '&#127474 &#127464'
  },
  {
    emoji: '🇲🇩',
    name: 'Moldova', 
    hexcode: 'U+1F1F2 U+1F1E9',
    html: '&#127474 &#127465'
  },
  {
    emoji: '🇲🇪',
    name: 'Montenegro', 
    hexcode: 'U+1F1F2 U+1F1EA',
    html: '&#127474 &#127466'
  },
  {
    emoji: '🇲🇫',
    name: 'St. Martin', 
    hexcode: 'U+1F1F2 U+1F1EB',
    html: '&#127474 &#127467'
  },
  {
    emoji: '🇲🇬',
    name: 'Madagascar', 
    hexcode: 'U+1F1F2 U+1F1EC',
    html: '&#127474 &#127468'
  },
  {
    emoji: '🇲🇭',
    name: 'Marshall Islands', 
    hexcode: 'U+1F1F2 U+1F1ED',
    html: '&#127474 &#127469'
  },
  {
    emoji: '🇲🇰',
    name: 'Macedonia', 
    hexcode: 'U+1F1F2 U+1F1F0',
    html: '&#127474 &#127472'
  },
  {
    emoji: '🇲🇱',
    name: 'Mali', 
    hexcode: 'U+1F1F2 U+1F1F1',
    html: '&#127474 &#127473'
  },
  {
    emoji: '🇲🇲',
    name: 'Myanmar (Burma)', 
    hexcode: 'U+1F1F2 U+1F1F2',
    html: '&#127474 &#127474'
  },
  {
    emoji: '🇲🇳',
    name: 'Mongolia', 
    hexcode: 'U+1F1F2 U+1F1F3',
    html: '&#127474 &#127475'
  },
  {
    emoji: '🇲🇴',
    name: 'Macao SAR China', 
    hexcode: 'U+1F1F2 U+1F1F4',
    html: '&#127474 &#127476'
  },
  {
    emoji: '🇲🇵',
    name: 'Northern Mariana Islands', 
    hexcode: 'U+1F1F2 U+1F1F5',
    html: '&#127474 &#127477'
  },
  {
    emoji: '🇲🇶',
    name: 'Martinique', 
    hexcode: 'U+1F1F2 U+1F1F6',
    html: '&#127474 &#127478'
  },
  {
    emoji: '🇲🇷',
    name: 'Mauritania', 
    hexcode: 'U+1F1F2 U+1F1F7',
    html: '&#127474 &#127479'
  },
  {
    emoji: '🇲🇸',
    name: 'Montserrat', 
    hexcode: 'U+1F1F2 U+1F1F8',
    html: '&#127474 &#127480'
  },
  {
    emoji: '🇲🇹',
    name: 'Malta', 
    hexcode: 'U+1F1F2 U+1F1F9',
    html: '&#127474 &#127481'
  },
  {
    emoji: '🇲🇺',
    name: 'Mauritius', 
    hexcode: 'U+1F1F2 U+1F1FA',
    html: '&#127474 &#127482'
  },
  {
    emoji: '🇲🇻',
    name: 'Maldives', 
    hexcode: 'U+1F1F2 U+1F1FB',
    html: '&#127474 &#127483'
  },
  {
    emoji: '🇲🇼',
    name: 'Malawi', 
    hexcode: 'U+1F1F2 U+1F1FC',
    html: '&#127474 &#127484'
  },
  {
    emoji: '🇲🇽',
    name: 'Mexico', 
    hexcode: 'U+1F1F2 U+1F1FD',
    html: '&#127474 &#127485'
  },
  {
    emoji: '🇲🇾',
    name: 'Malaysia', 
    hexcode: 'U+1F1F2 U+1F1FE',
    html: '&#127474 &#127486'
  },
  {
    emoji: '🇲🇿',
    name: 'Mozambique', 
    hexcode: 'U+1F1F2 U+1F1FF',
    html: '&#127474 &#127487'
  },
  {
    emoji: '🇳🇦',
    name: 'Namibia', 
    hexcode: 'U+1F1F3 U+1F1E6',
    html: '&#127475 &#127462'
  },
  {
    emoji: '🇳🇨',
    name: 'New Caledonia', 
    hexcode: 'U+1F1F3 U+1F1E8',
    html: '&#127475 &#127464'
  },
  {
    emoji: '🇳🇪',
    name: 'Niger', 
    hexcode: 'U+1F1F3 U+1F1EA',
    html: '&#127475 &#127466'
  },
  {
    emoji: '🇳🇫',
    name: 'Norfolk Island', 
    hexcode: 'U+1F1F3 U+1F1EB',
    html: '&#127475 &#127467'
  },
  {
    emoji: '🇳🇬',
    name: 'Nigeria', 
    hexcode: 'U+1F1F3 U+1F1EC',
    html: '&#127475 &#127468'
  },
  {
    emoji: '🇳🇮',
    name: 'Nicaragua', 
    hexcode: 'U+1F1F3 U+1F1EE',
    html: '&#127475 &#127470'
  },
  {
    emoji: '🇳🇱',
    name: 'Netherlands', 
    hexcode: 'U+1F1F3 U+1F1F1',
    html: '&#127475 &#127473'
  },
  {
    emoji: '🇳🇴',
    name: 'Norway', 
    hexcode: 'U+1F1F3 U+1F1F4',
    html: '&#127475 &#127476'
  },
  {
    emoji: '🇳🇵',
    name: 'Nepal', 
    hexcode: 'U+1F1F3 U+1F1F5',
    html: '&#127475 &#127477'
  },
  {
    emoji: '🇳🇷',
    name: 'Nauru', 
    hexcode: 'U+1F1F3 U+1F1F7',
    html: '&#127475 &#127479'
  },
  {
    emoji: '🇳🇺',
    name: 'Niue', 
    hexcode: 'U+1F1F3 U+1F1FA',
    html: '&#127475 &#127482'
  },
  {
    emoji: '🇳🇿',
    name: 'New Zealand', 
    hexcode: 'U+1F1F3 U+1F1FF',
    html: '&#127475 &#127487'
  },
  {
    emoji: '🇴🇲',
    name: 'Oman', 
    hexcode: 'U+1F1F4 U+1F1F2',
    html: '&#127476 &#127474'
  },
  {
    emoji: '🇵🇦',
    name: 'Panama', 
    hexcode: 'U+1F1F5 U+1F1E6',
    html: '&#127477 &#127462'
  },
  {
    emoji: '🇵🇪',
    name: 'Peru', 
    hexcode: 'U+1F1F5 U+1F1EA',
    html: '&#127477 &#127466'
  },
  {
    emoji: '🇵🇫',
    name: 'French Polynesia', 
    hexcode: 'U+1F1F5 U+1F1EB',
    html: '&#127477 &#127467'
  },
  {
    emoji: '🇵🇬',
    name: 'Papua New Guinea', 
    hexcode: 'U+1F1F5 U+1F1EC',
    html: '&#127477 &#127468'
  },
  {
    emoji: '🇵🇭',
    name: 'Philippines', 
    hexcode: 'U+1F1F5 U+1F1ED',
    html: '&#127477 &#127469'
  },
  {
    emoji: '🇵🇰',
    name: 'Pakistan', 
    hexcode: 'U+1F1F5 U+1F1F0',
    html: '&#127477 &#127472'
  },
  {
    emoji: '🇵🇱',
    name: 'Poland', 
    hexcode: 'U+1F1F5 U+1F1F1',
    html: '&#127477 &#127473'
  },
  {
    emoji: '🇵🇲',
    name: 'St. Pierre & Miquelon', 
    hexcode: 'U+1F1F5 U+1F1F2',
    html: '&#127477 &#127474'
  },
  {
    emoji: '🇵🇳',
    name: 'Pitcairn Islands', 
    hexcode: 'U+1F1F5 U+1F1F3',
    html: '&#127477 &#127475'
  },
  {
    emoji: '🇵🇷',
    name: 'Puerto Rico', 
    hexcode: 'U+1F1F5 U+1F1F7',
    html: '&#127477 &#127479'
  },
  {
    emoji: '🇵🇸',
    name: 'Palestinian Territories', 
    hexcode: 'U+1F1F5 U+1F1F8',
    html: '&#127477 &#127480'
  },
  {
    emoji: '🇵🇹',
    name: 'Portugal', 
    hexcode: 'U+1F1F5 U+1F1F9',
    html: '&#127477 &#127481'
  },
  {
    emoji: '🇵🇼',
    name: 'Palau', 
    hexcode: 'U+1F1F5 U+1F1FC',
    html: '&#127477 &#127484'
  },
  {
    emoji: '🇵🇾',
    name: 'Paraguay', 
    hexcode: 'U+1F1F5 U+1F1FE',
    html: '&#127477 &#127486'
  },
  {
    emoji: '🇶🇦',
    name: 'Qatar', 
    hexcode: 'U+1F1F6 U+1F1E6',
    html: '&#127478 &#127462'
  },
  {
    emoji: '🇷🇪',
    name: 'Réunion', 
    hexcode: 'U+1F1F7 U+1F1EA',
    html: '&#127479 &#127466'
  },
  {
    emoji: '🇷🇴',
    name: 'Romania', 
    hexcode: 'U+1F1F7 U+1F1F4',
    html: '&#127479 &#127476'
  },
  {
    emoji: '🇷🇸',
    name: 'Serbia', 
    hexcode: 'U+1F1F7 U+1F1F8',
    html: '&#127479 &#127480'
  },
  {
    emoji: '🇷🇺',
    name: 'Russia', 
    hexcode: 'U+1F1F7 U+1F1FA',
    html: '&#127479 &#127482'
  },
  {
    emoji: '🇷🇼',
    name: 'Rwanda', 
    hexcode: 'U+1F1F7 U+1F1FC',
    html: '&#127479 &#127484'
  },
  {
    emoji: '🇸🇦',
    name: 'Saudi Arabia', 
    hexcode: 'U+1F1F8 U+1F1E6',
    html: '&#127480 &#127462'
  },
  {
    emoji: '🇸🇧',
    name: 'Solomon Islands', 
    hexcode: 'U+1F1F8 U+1F1E7',
    html: '&#127480 &#127463'
  },
  {
    emoji: '🇸🇨',
    name: 'Seychelles', 
    hexcode: 'U+1F1F8 U+1F1E8',
    html: '&#127480 &#127464'
  },
  {
    emoji: '🇸🇩',
    name: 'Sudan', 
    hexcode: 'U+1F1F8 U+1F1E9',
    html: '&#127480 &#127465'
  },
  {
    emoji: '🇸🇪',
    name: 'Sweden', 
    hexcode: 'U+1F1F8 U+1F1EA',
    html: '&#127480 &#127466'
  },
  {
    emoji: '🇸🇬',
    name: 'Singapore', 
    hexcode: 'U+1F1F8 U+1F1EC',
    html: '&#127480 &#127468'
  },
  {
    emoji: '🇸🇭',
    name: 'St. Helena', 
    hexcode: 'U+1F1F8 U+1F1ED',
    html: '&#127480 &#127469'
  },
  {
    emoji: '🇸🇮',
    name: 'Slovenia', 
    hexcode: 'U+1F1F8 U+1F1EE',
    html: '&#127480 &#127470'
  },
  {
    emoji: '🇸🇯',
    name: 'Svalbard & Jan Mayen', 
    hexcode: 'U+1F1F8 U+1F1EF',
    html: '&#127480 &#127471'
  },
  {
    emoji: '🇸🇰',
    name: 'Slovakia', 
    hexcode: 'U+1F1F8 U+1F1F0',
    html: '&#127480 &#127472'
  },
  {
    emoji: '🇸🇱',
    name: 'Sierra Leone', 
    hexcode: 'U+1F1F8 U+1F1F1',
    html: '&#127480 &#127473'
  },
  {
    emoji: '🇸🇲',
    name: 'San Marino', 
    hexcode: 'U+1F1F8 U+1F1F2',
    html: '&#127480 &#127474'
  },
  {
    emoji: '🇸🇳',
    name: 'Senegal', 
    hexcode: 'U+1F1F8 U+1F1F3',
    html: '&#127480 &#127475'
  },
  {
    emoji: '🇸🇴',
    name: 'Somalia', 
    hexcode: 'U+1F1F8 U+1F1F4',
    html: '&#127480 &#127476'
  },
  {
    emoji: '🇸🇷',
    name: 'Suriname', 
    hexcode: 'U+1F1F8 U+1F1F7',
    html: '&#127480 &#127479'
  },
  {
    emoji: '🇸🇸',
    name: 'South Sudan', 
    hexcode: 'U+1F1F8 U+1F1F8',
    html: '&#127480 &#127480'
  },
  {
    emoji: '🇸🇹',
    name: 'São Tomé & Príncipe', 
    hexcode: 'U+1F1F8 U+1F1F9',
    html: '&#127480 &#127481'
  },
  {
    emoji: '🇸🇻',
    name: 'El Salvador', 
    hexcode: 'U+1F1F8 U+1F1FB',
    html: '&#127480 &#127483'
  },
  {
    emoji: '🇸🇽',
    name: 'Sint Maarten', 
    hexcode: 'U+1F1F8 U+1F1FD',
    html: '&#127480 &#127485'
  },
  {
    emoji: '🇸🇾',
    name: 'Syria', 
    hexcode: 'U+1F1F8 U+1F1FE',
    html: '&#127480 &#127486'
  },
  {
    emoji: '🇸🇿',
    name: 'Eswatini', 
    hexcode: 'U+1F1F8 U+1F1FF',
    html: '&#127480 &#127487'
  },
  {
    emoji: '🇹🇦',
    name: 'Tristan da Cunha', 
    hexcode: 'U+1F1F9 U+1F1E6',
    html: '&#127481 &#127462'
  },
  {
    emoji: '🇹🇨',
    name: 'Turks & Caicos Islands', 
    hexcode: 'U+1F1F9 U+1F1E8',
    html: '&#127481 &#127464'
  },
  {
    emoji: '🇹🇩',
    name: 'Chad', 
    hexcode: 'U+1F1F9 U+1F1E9',
    html: '&#127481 &#127465'
  },
  {
    emoji: '🇹🇫',
    name: 'French Southern Territories', 
    hexcode: 'U+1F1F9 U+1F1EB',
    html: '&#127481 &#127467'
  },
  {
    emoji: '🇹🇬',
    name: 'Togo', 
    hexcode: 'U+1F1F9 U+1F1EC',
    html: '&#127481 &#127468'
  },
  {
    emoji: '🇹🇭',
    name: 'Thailand', 
    hexcode: 'U+1F1F9 U+1F1ED',
    html: '&#127481 &#127469'
  },
  {
    emoji: '🇹🇯',
    name: 'Tajikistan', 
    hexcode: 'U+1F1F9 U+1F1EF',
    html: '&#127481 &#127471'
  },
  {
    emoji: '🇹🇰',
    name: 'Tokelau', 
    hexcode: 'U+1F1F9 U+1F1F0',
    html: '&#127481 &#127472'
  },
  {
    emoji: '🇹🇱',
    name: 'Timor-Leste', 
    hexcode: 'U+1F1F9 U+1F1F1',
    html: '&#127481 &#127473'
  },
  {
    emoji: '🇹🇲',
    name: 'Turkmenistan', 
    hexcode: 'U+1F1F9 U+1F1F2',
    html: '&#127481 &#127474'
  },
  {
    emoji: '🇹🇳',
    name: 'Tunisia', 
    hexcode: 'U+1F1F9 U+1F1F3',
    html: '&#127481 &#127475'
  },
  {
    emoji: '🇹🇴',
    name: 'Tonga', 
    hexcode: 'U+1F1F9 U+1F1F4',
    html: '&#127481 &#127476'
  },
  {
    emoji: '🇹🇷',
    name: 'Turkey', 
    hexcode: 'U+1F1F9 U+1F1F7',
    html: '&#127481 &#127479'
  },
  {
    emoji: '🇹🇹',
    name: 'Trinidad & Tobago', 
    hexcode: 'U+1F1F9 U+1F1F9',
    html: '&#127481 &#127481'
  },
  {
    emoji: '🇹🇻',
    name: 'Tuvalu', 
    hexcode: 'U+1F1F9 U+1F1FB',
    html: '&#127481 &#127483'
  },
  {
    emoji: '🇹🇼',
    name: 'Taiwan', 
    hexcode: 'U+1F1F9 U+1F1FC',
    html: '&#127481 &#127484'
  },
  {
    emoji: '🇹🇿',
    name: 'Tanzania', 
    hexcode: 'U+1F1F9 U+1F1FF',
    html: '&#127481 &#127487'
  },
  {
    emoji: '🇺🇦',
    name: 'Ukraine', 
    hexcode: 'U+1F1FA U+1F1E6',
    html: '&#127482 &#127462'
  },
  {
    emoji: '🇺🇬',
    name: 'Uganda', 
    hexcode: 'U+1F1FA U+1F1EC',
    html: '&#127482 &#127468'
  },
  {
    emoji: '🇺🇲',
    name: 'U.S. Outlying Islands', 
    hexcode: 'U+1F1FA U+1F1F2',
    html: '&#127482 &#127474'
  },
  {
    emoji: '🇺🇳',
    name: 'United Nations', 
    hexcode: 'U+1F1FA U+1F1F3',
    html: '&#127482 &#127475'
  },
  {
    emoji: '🇺🇸',
    name: 'United States', 
    hexcode: 'U+1F1FA U+1F1F8',
    html: '&#127482 &#127480'
  },
  {
    emoji: '🇺🇾',
    name: 'Uruguay', 
    hexcode: 'U+1F1FA U+1F1FE',
    html: '&#127482 &#127486'
  },
  {
    emoji: '🇺🇿',
    name: 'Uzbekistan', 
    hexcode: 'U+1F1FA U+1F1FF',
    html: '&#127482 &#127487'
  },
  {
    emoji: '🇻🇦',
    name: 'Vatican City', 
    hexcode: 'U+1F1FB U+1F1E6',
    html: '&#127483 &#127462'
  },
  {
    emoji: '🇻🇨',
    name: 'St. Vincent & Grenadines', 
    hexcode: 'U+1F1FB U+1F1E8',
    html: '&#127483 &#127464'
  },
  {
    emoji: '🇻🇪',
    name: 'Venezuela', 
    hexcode: 'U+1F1FB U+1F1EA',
    html: '&#127483 &#127466'
  },
  {
    emoji: '🇻🇬',
    name: 'British Virgin Islands', 
    hexcode: 'U+1F1FB U+1F1EC',
    html: '&#127483 &#127468'
  },
  {
    emoji: '🇻🇮',
    name: 'U.S. Virgin Islands', 
    hexcode: 'U+1F1FB U+1F1EE',
    html: '&#127483 &#127470'
  },
  {
    emoji: '🇻🇳',
    name: 'Vietnam', 
    hexcode: 'U+1F1FB U+1F1F3',
    html: '&#127483 &#127475'
  },
  {
    emoji: '🇻🇺',
    name: 'Vanuatu', 
    hexcode: 'U+1F1FB U+1F1FA',
    html: '&#127483 &#127482'
  },
  {
    emoji: '🇼🇫',
    name: 'Wallis & Futuna', 
    hexcode: 'U+1F1FC U+1F1EB',
    html: '&#127484 &#127467'
  },
  {
    emoji: '🇼🇸',
    name: 'Samoa', 
    hexcode: 'U+1F1FC U+1F1F8',
    html: '&#127484 &#127480'
  },
  {
    emoji: '🇽🇰',
    name: 'Kosovo', 
    hexcode: 'U+1F1FD U+1F1F0',
    html: '&#127485 &#127472'
  },
  {
    emoji: '🇾🇪',
    name: 'Yemen', 
    hexcode: 'U+1F1FE U+1F1EA',
    html: '&#127486 &#127466'
  },
  {
    emoji: '🇾🇹',
    name: 'Mayotte', 
    hexcode: 'U+1F1FE U+1F1F9',
    html: '&#127486 &#127481'
  },
  {
    emoji: '🇿🇦',
    name: 'South Africa', 
    hexcode: 'U+1F1FF U+1F1E6',
    html: '&#127487 &#127462'
  },
  {
    emoji: '🇿🇲',
    name: 'Zambia', 
    hexcode: 'U+1F1FF U+1F1F2',
    html: '&#127487 &#127474'
  },
  {
    emoji: '🇿🇼',
    name: 'Zimbabwe', 
    hexcode: 'U+1F1FF U+1F1FC',
    html: '&#127487 &#127484'
  },
  {
    emoji: '🏴󠁧󠁢󠁥󠁮󠁧󠁿',
    name: 'England', 
    hexcode: 'U+1F3F4 U+E0067 U+E0062 U+E0065 U+E006E U+E0067 U+E007F',
    html: '&#127988 &#917607 &#917602 &#917605 &#917614 &#917607 &#917631'
  },
  {
    emoji: '🏴󠁧󠁢󠁳󠁣󠁴󠁿',
    name: 'Scotland', 
    hexcode: 'U+1F3F4 U+E0067 U+E0062 U+E0073 U+E0063 U+E0074 U+E007F',
    html: '&#127988 &#917607 &#917602 &#917619 &#917603 &#917620 &#917631'
  },
  {
    emoji: '🏴󠁧󠁢󠁷󠁬󠁳󠁿',
    name: 'Wales', 
    hexcode: 'U+1F3F4 U+E0067 U+E0062 U+E0077 U+E006C U+E0073 U+E007F',
    html: '&#127988 &#917607 &#917602 &#917623 &#917612 &#917619 &#917631'
  },
  {
		emoji: '🍇',
		name: 'Grapes Emoji',
		hexcode: 'U+1F347',
		html: '&#127815'
  },
  {
		emoji: '🍈',
		name: 'Melon Emoji',
		hexcode: 'U+1F348',
		html: '&#127816'
  },
  {
		emoji: '🍉',
		name: 'Watermelon Emoji',
		hexcode: 'U+1F349',
		html: '&#127817'
  },
  {
		emoji: '🍊',
		name: 'Tangerine Emoji',
		hexcode: 'U+1F34A',
		html: '&#127818'
  },
  {
		emoji: '🍋',
		name: 'Lemon Emoji',
		hexcode: 'U+1F34B',
		html: '&#127819'
  },
  {
		emoji: '🍌',
		name: 'Banana Emoji',
		hexcode: 'U+1F34C',
		html: '&#127820'
  },
  {
		emoji: '🍍',
		name: 'Pineapple Emoji',
		hexcode: 'U+1F34D',
		html: '&#127821'
  },
  {
		emoji: '🥭',
		name: 'Mango Emoji',
		hexcode: 'U+1F96D',
		html: '&#129389'
  },
  {
		emoji: '🍎',
		name: 'Red Apple Emoji',
		hexcode: 'U+1F34E',
		html: '&#127822'
  },
  {
		emoji: '🍏',
		name: 'Green Apple Emoji',
		hexcode: 'U+1F34F',
		html: '&#127823'
  },
  {
		emoji: '🍐',
		name: 'Pear Emoji',
		hexcode: 'U+1F350',
		html: '&#127824'
  },
  {
		emoji: '🍑',
		name: 'Peach Emoji',
		hexcode: 'U+1F351',
		html: '&#127825'
  },
  {
		emoji: '🍒',
		name: 'Cherries Emoji',
		hexcode: 'U+1F352',
		html: '&#127826'
  },
  {
		emoji: '🍓',
		name: 'Strawberry Emoji',
		hexcode: 'U+1F353',
		html: '&#127827'
  },
  {
		emoji: '🥝',
		name: 'Kiwi Fruit Emoji',
		hexcode: 'U+1F95D',
		html: '&#129373'
  },
  {
		emoji: '🍅',
		name: 'Tomato Emoji',
		hexcode: 'U+1F345',
		html: '&#127813'
  },
  {
		emoji: '🥥',
		name: 'Coconut Emoji',
		hexcode: 'U+1F965',
		html: '&#129381'
  },
  {
		emoji: '🥑',
		name: 'Avocado Emoji',
		hexcode: 'U+1F951',
		html: '&#129361'
  },
  {
		emoji: '🍆',
		name: 'Eggplant Emoji',
		hexcode: 'U+1F346',
		html: '&#127814'
  },
  {
		emoji: '🥔',
		name: 'Potato Emoji',
		hexcode: 'U+1F954',
		html: '&#129364'
  },
  {
		emoji: '🥕',
		name: 'Carrot Emoji',
		hexcode: 'U+1F955',
		html: '&#129365'
  },
  {
		emoji: '🌽',
		name: 'Ear Of Corn Emoji',
		hexcode: 'U+1F33D',
		html: '&#127805'
  },
  {
		emoji: '🌶',
		name: 'Hot Pepper Emoji',
		hexcode: 'U+1F336',
		html: '&#127798'
  },
  {
		emoji: '🥒',
		name: 'Cucumber Emoji',
		hexcode: 'U+1F952',
		html: '&#129362'
  },
  {
		emoji: '🥬',
		name: 'Leafy Green Emoji',
		hexcode: 'U+1F96C',
		html: '&#129388'
  },
  {
		emoji: '🥦',
		name: 'Broccoli Emoji',
		hexcode: 'U+1F966',
		html: '&#129382'
  },
  {
		emoji: '🍄',
		name: 'Mushroom Emoji',
		hexcode: 'U+1F344',
		html: '&#127812'
  },
  {
		emoji: '🥜',
		name: 'Peanuts Emoji',
		hexcode: 'U+1F95C',
		html: '&#129372'
  },
  {
		emoji: '🌰',
		name: 'Chestnut Emoji',
		hexcode: 'U+1F330',
		html: '&#127792'
  },
  {
		emoji: '🍞',
		name: 'Bread Emoji',
		hexcode: 'U+1F35E',
		html: '&#127838'
  },
  {
		emoji: '🥐',
		name: 'Croissant Emoji',
		hexcode: 'U+1F950',
		html: '&#129360'
  },
  {
		emoji: '🥖',
		name: 'Baguette Bread Emoji',
		hexcode: 'U+1F956',
		html: '&#129366'
  },
  {
		emoji: '🥨',
		name: 'Pretzel Emoji',
		hexcode: 'U+1F968',
		html: '&#129384'
  },
  {
		emoji: '🥯',
		name: 'Bagel Emoji',
		hexcode: 'U+1F96F',
		html: '&#129391'
  },
  {
		emoji: '🥞',
		name: 'Pancakes Emoji',
		hexcode: 'U+1F95E',
		html: '&#129374'
  },
  {
		emoji: '🧀',
		name: 'Cheese Wedge Emoji',
		hexcode: 'U+1F9C0',
		html: '&#129472'
  },
  {
		emoji: '🍖',
		name: 'Meat On Bone Emoji',
		hexcode: 'U+1F356',
		html: '&#127830'
  },
  {
		emoji: '🍗',
		name: 'Poultry Leg Emoji',
		hexcode: 'U+1F357',
		html: '&#127831'
  },
  {
		emoji: '🥩',
		name: 'Cut Of Meat Emoji',
		hexcode: 'U+1F969',
		html: '&#129385'
  },
  {
		emoji: '🥓',
		name: 'Bacon Emoji',
		hexcode: 'U+1F953',
		html: '&#129363'
  },
  {
		emoji: '🍔',
		name: 'Hamburger Emoji',
		hexcode: 'U+1F354',
		html: '&#127828'
  },
  {
		emoji: '🍟',
		name: 'French Fries Emoji',
		hexcode: 'U+1F35F',
		html: '&#127839'
  },
  {
		emoji: '🍕',
		name: 'Pizza Emoji',
		hexcode: 'U+1F355',
		html: '&#127829'
  },
  {
		emoji: '🌭',
		name: 'Hot Dog Emoji',
		hexcode: 'U+1F32D',
		html: '&#127789'
  },
  {
		emoji: '🥪',
		name: 'Sandwich Emoji',
		hexcode: 'U+1F96A',
		html: '&#129386'
  },
  {
		emoji: '🌮',
		name: 'Taco Emoji',
		hexcode: 'U+1F32E',
		html: '&#127790'
  },
  {
		emoji: '🌯',
		name: 'Burrito Emoji',
		hexcode: 'U+1F32F',
		html: '&#127791'
  },
  {
		emoji: '🥙',
		name: 'Stuffed Flatbread Emoji',
		hexcode: 'U+1F959',
		html: '&#129369'
  },
  {
		emoji: '🥚',
		name: 'Egg Emoji',
		hexcode: 'U+1F95A',
		html: '&#129370'
  },
  {
		emoji: '🍳',
		name: 'Cooking Emoji',
		hexcode: 'U+1F373',
		html: '&#127859'
  },
  {
		emoji: '🥘',
		name: 'Shallow Pan Of Food Emoji',
		hexcode: 'U+1F958',
		html: '&#129368'
  },
  {
		emoji: '🍲',
		name: 'Pot Of Food Emoji',
		hexcode: 'U+1F372',
		html: '&#127858'
  },
  {
		emoji: '🥣',
		name: 'Bowl With Spoon Emoji',
		hexcode: 'U+1F963',
		html: '&#129379'
  },
  {
		emoji: '🥗',
		name: 'Green Salad Emoji',
		hexcode: 'U+1F957',
		html: '&#129367'
  },
  {
		emoji: '🍿',
		name: 'Popcorn Emoji',
		hexcode: 'U+1F37F',
		html: '&#127871'
  },
  {
		emoji: '🧂',
		name: 'Salt Emoji',
		hexcode: 'U+1F9C2',
		html: '&#129474'
  },
  {
		emoji: '🥫',
		name: 'Canned Food Emoji',
		hexcode: 'U+1F96B',
		html: '&#129387'
  },
  {
		emoji: '🍱',
		name: 'Bento Box Emoji',
		hexcode: 'U+1F371',
		html: '&#127857'
  },
  {
		emoji: '🍘',
		name: 'Rice Cracker Emoji',
		hexcode: 'U+1F358',
		html: '&#127832'
  },
  {
		emoji: '🍙',
		name: 'Rice Ball Emoji',
		hexcode: 'U+1F359',
		html: '&#127833'
  },
  {
		emoji: '🍚',
		name: 'Cooked Rice Emoji',
		hexcode: 'U+1F35A',
		html: '&#127834'
  },
  {
		emoji: '🍛',
		name: 'Curry Rice Emoji',
		hexcode: 'U+1F35B',
		html: '&#127835'
  },
  {
		emoji: '🍜',
		name: 'Steaming Bowl Emoji',
		hexcode: 'U+1F35C',
		html: '&#127836'
  },
  {
		emoji: '🍝',
		name: 'Spaghetti Emoji',
		hexcode: 'U+1F35D',
		html: '&#127837'
  },
  {
		emoji: '🍠',
		name: 'Roasted Sweet Potato Emoji',
		hexcode: 'U+1F360',
		html: '&#127840'
  },
  {
		emoji: '🍢',
		name: 'Oden Emoji',
		hexcode: 'U+1F362',
		html: '&#127842'
  },
  {
		emoji: '🍣',
		name: 'Sushi Emoji',
		hexcode: 'U+1F363',
		html: '&#127843'
  },
  {
		emoji: '🍤',
		name: 'Fried Shrimp Emoji',
		hexcode: 'U+1F364',
		html: '&#127844'
  },
  {
		emoji: '🍥',
		name: 'Fish Cake With Swirl Emoji',
		hexcode: 'U+1F365',
		html: '&#127845'
  },
  {
		emoji: '🥮',
		name: 'Moon Cake Emoji',
		hexcode: 'U+1F96E',
		html: '&#129390'
  },
  {
		emoji: '🍡',
		name: 'Dango Emoji',
		hexcode: 'U+1F361',
		html: '&#127841'
  },
  {
		emoji: '🥟',
		name: 'Dumpling Emoji',
		hexcode: 'U+1F95F',
		html: '&#129375'
  },
  {
		emoji: '🥠',
		name: 'Fortune Cookie Emoji',
		hexcode: 'U+1F960',
		html: '&#129376'
  },
  {
		emoji: '🥡',
		name: 'Takeout Box Emoji',
		hexcode: 'U+1F961',
		html: '&#129377'
  },
  {
		emoji: '🦀',
		name: 'Crab Emoji',
		hexcode: 'U+1F980',
		html: '&#129408'
  },
  {
		emoji: '🦞',
		name: 'Lobster Emoji',
		hexcode: 'U+1F99E',
		html: '&#129438'
  },
  {
		emoji: '🦐',
		name: 'Shrimp Emoji',
		hexcode: 'U+1F990',
		html: '&#129424'
  },
  {
		emoji: '🦑',
		name: 'Squid Emoji',
		hexcode: 'U+1F991',
		html: '&#129425'
  },
  {
		emoji: '🍦',
		name: 'Soft İce Cream Emoji',
		hexcode: 'U+1F366',
		html: '&#127846'
  },
  {
		emoji: '🍧',
		name: 'Shaved İce Emoji',
		hexcode: 'U+1F367',
		html: '&#127847'
  },
  {
		emoji: '🍨',
		name: 'İce Cream Emoji',
		hexcode: 'U+1F368',
		html: '&#127848'
  },
  {
		emoji: '🍩',
		name: 'Doughnut Emoji',
		hexcode: 'U+1F369',
		html: '&#127849'
  },
  {
		emoji: '🍪',
		name: 'Cookie Emoji',
		hexcode: 'U+1F36A',
		html: '&#127850'
  },
  {
		emoji: '🎂',
		name: 'Birthday Cake Emoji',
		hexcode: 'U+1F382',
		html: '&#127874'
  },
  {
		emoji: '🍰',
		name: 'Shortcake Emoji',
		hexcode: 'U+1F370',
		html: '&#127856'
  },
  {
		emoji: '🧁',
		name: 'Cupcake Emoji',
		hexcode: 'U+1F9C1',
		html: '&#129473'
  },
  {
		emoji: '🥧',
		name: 'Pie Emoji',
		hexcode: 'U+1F967',
		html: '&#129383'
  },
  {
		emoji: '🍫',
		name: 'Chocolate Bar Emoji',
		hexcode: 'U+1F36B',
		html: '&#127851'
  },
  {
		emoji: '🍬',
		name: 'Candy Emoji',
		hexcode: 'U+1F36C',
		html: '&#127852'
  },
  {
		emoji: '🍭',
		name: 'Lollipop Emoji',
		hexcode: 'U+1F36D',
		html: '&#127853'
  },
  {
		emoji: '🍮',
		name: 'Custard Emoji',
		hexcode: 'U+1F36E',
		html: '&#127854'
  },
  {
		emoji: '🍯',
		name: 'Honey Pot Emoji',
		hexcode: 'U+1F36F',
		html: '&#127855'
  },
  {
		emoji: '🍼',
		name: 'Baby Bottle Emoji',
		hexcode: 'U+1F37C',
		html: '&#127868'
  },
  {
		emoji: '🥛',
		name: 'Glass Of Milk Emoji',
		hexcode: 'U+1F95B',
		html: '&#129371'
  },
  {
		emoji: '☕',
		name: 'Hot Beverage Emoji',
		hexcode: 'U+2615',
		html: '&#9749'
  },
  {
		emoji: '🍵',
		name: 'Teacup Without Handle Emoji',
		hexcode: 'U+1F375',
		html: '&#127861'
  },
  {
		emoji: '🍶',
		name: 'Sake Emoji',
		hexcode: 'U+1F376',
		html: '&#127862'
  },
  {
		emoji: '🍾',
		name: 'Bottle With Popping Cork Emoji',
		hexcode: 'U+1F37E',
		html: '&#127870'
  },
  {
		emoji: '🍷',
		name: 'Wine Glass Emoji',
		hexcode: 'U+1F377',
		html: '&#127863'
  },
  {
		emoji: '🍸',
		name: 'Cocktail Glass Emoji',
		hexcode: 'U+1F378',
		html: '&#127864'
  },
  {
		emoji: '🍹',
		name: 'Tropical Drink Emoji',
		hexcode: 'U+1F379',
		html: '&#127865'
  },
  {
		emoji: '🍺',
		name: 'Beer Mug Emoji',
		hexcode: 'U+1F37A',
		html: '&#127866'
  },
  {
		emoji: '🍻',
		name: 'Clinking Beer Mugs Emoji',
		hexcode: 'U+1F37B',
		html: '&#127867'
  },
  {
		emoji: '🥂',
		name: 'Clinking Glasses Emoji',
		hexcode: 'U+1F942',
		html: '&#129346'
  },
  {
		emoji: '🥃',
		name: 'Tumbler Glass Emoji',
		hexcode: 'U+1F943',
		html: '&#129347'
  },
  {
		emoji: '🥤',
		name: 'Cup With Straw Emoji',
		hexcode: 'U+1F964',
		html: '&#129380'
  },
  {
		emoji: '🥢',
		name: 'Chopsticks Emoji',
		hexcode: 'U+1F962',
		html: '&#129378'
  },
  {
		emoji: '🍽',
		name: 'Fork And Knife With Plate Emoji',
		hexcode: 'U+1F37D',
		html: '&#127869'
  },
  {
		emoji: '🍴',
		name: 'Fork And Knife Emoji',
		hexcode: 'U+1F374',
		html: '&#127860'
  },
  {
		emoji: '🥄',
		name: 'Spoon Emoji',
		hexcode: 'U+1F944',
		html: '&#129348'
  },
  {
		emoji: '🔪',
		name: 'Kitchen Knife Emoji',
		hexcode: 'U+1F52A',
		html: '&#128298'
  },
  {
		emoji: '🏺',
		name: 'Amphora Emoji',
		hexcode: 'U+1F3FA',
		html: '&#127994'
  },
  {
    emoji: '♡',
    name: 'White Heart Suit',
    html: '&#x2661;'
  },
  {
    emoji: '♥',
    name: 'Black Heart Suit',
    html: '&#x2665;'
  },
  {
    emoji: '🖤',
    name: 'Black Heart',
    html: '&#x1F5A4;'
  },
  {
    emoji: '💙',
    name: 'Blue Heart',
    html: '&#x1F499;'
  },
  {
    emoji: '💚',
    name: 'Green Heart',
    html: '&#x1F49A;'
  },
  {
    emoji: '💛',
    name: 'Yellow Heart',
    html: '&#x1F49B;'
  },
  {
    emoji: '💜',
    name: 'Purple Heart',
    html: '&#x1F49C;'
  },
  {
    emoji: '🧡',
    name: 'Orange Heart',
    html: '&#x1F9E1;'
  },
  {
    emoji: '🤍',
    name: 'White Heart',
    html: '&#x1F90D;'
  },
  {
    emoji: '🤎',
    name: 'Brown Heart',
    html: '&#x1F90E;'
  },
  {
    emoji: '❣',
    name: 'Heavy Heart Exclamation Mark Ornament',
    html: '&#x2763;'
  },
  {
    emoji: '❤',
    name: 'Heavy Black Heart',
    html: '&#x2764;'
  },
  {
    emoji: '❥',
    name: 'Rotated Heavy Black Heart Bullet',
    html: '&#x2765;'
  },
  {
    emoji: '🎔',
    name: 'Heart With Tip On The Left',
    html: '&#x1F394;'
  },
  {
    emoji: '💓',
    name: 'Beating Heart',
    html: '&#x1F493;'
  },
  {
    emoji: '💔',
    name: 'Broken Heart',
    html: '&#x1F494;'
  },
  {
    emoji: '💖',
    name: 'Sparkling Heart',
    html: '&#x1F496;'
  },
  {
    emoji: '💗',
    name: 'Growing Heart',
    html: '&#x1F497;'
  },
  {
    emoji: '💕',
    name: 'Two Hearts',
    html: '&#x1F495;'
  },
  {
    emoji: '💞',
    name: 'Revolving Hearts',
    html: '&#x1F49E;'
  },
  {
    emoji: '💘',
    name: 'Heart With Arrow',
    html: '&#x1F498;'
  },
  {
    emoji: '💝',
    name: 'Heart With Ribbon',
    html: '&#x1F49D;'
  },
  {
    emoji: '💟',
    name: 'Heart Decoration',
    html: '&#x1F49F;'
  },
  {
    emoji: '💌',
    name: 'Love Letter',
    html: '&#x1F48C;'
  },
  {
    emoji: '🥰',
    name: 'Smiling Face With Hearts',
    html: '&#x1F970;'
  },
  {
    emoji: '😍',
    name: 'Smiling Face With Heart-shaped Eyes',
    html: '&#x1F60D;'
  },
  {
    emoji: '😘',
    name: 'Face Blowing a Kiss',
    html: '&#x1F618;'
  },
  {
    emoji: '😻',
    name: 'Smiling Cat Face With Heart-shaped Eyes',
    html: '&#x1F63B;'
  },
  {
    emoji: '💑',
    name: 'Couple With Heart',
    html: '&#x1F491;'
  },
  {
    emoji: '💏',
    name: 'Kiss',
    html: '&#x1F48F;'
  },
  {
    emoji: '❦',
    name: 'Floral Heart',
    html: '&#x2766;'
  },
  {
    emoji: '❧',
    name: 'Rotated Floral Heart Bullet',
    html: '&#x2767;'
  },
  {
    emoji: '☙',
    name: 'Reversed Rotated Floral Heart Bullet',
    html: '&#x2619;'
  },
  {
    emoji: '⺖',
    name: 'Cjk Radical Heart One',
    html: '&#x2E96;'
  },
  {
    emoji: '⺗',
    name: 'Cjk Radical Heart Two',
    html: '&#x2E97;'
  },
  {
    emoji: '⼼',
    name: 'Kangxi Radical Heart',
    html: '&#x2F3C;'
  },
  {
		emoji: '†',
    name: 'Cross',
    html: '0134'
  },
  {
		emoji: '✝',
    name: 'Latin Roman Cross',
    html: '&#10013;'
  },
  {
		emoji: '✟',
    name: 'Outlined Cross',
    html: '&#10015;'
  },
  {
		emoji: '✞',
    name: 'Latin Cross 3D',
    html: '&#10014;'
  },
  {
		emoji: '✟',
    name: 'Latin Cross outline',
    html: '&#10013;'
  },
  {
		emoji: '☩',
    name: 'Greek Cross',
    html: '&#9769;'
  },
  {
		emoji: '☦',
    name: 'Orthodox Cross',
    html: '&#9766;'
  },
  {
		emoji: '⁜',
    name: 'Dotted Cross',
    html: '&#8284;'
  },
  {
		emoji: '☨',
    name: 'Cross',
    html: '&#9768;'
  },
  {
		emoji: '☩',
    name: 'Jerusalem Cross',
    html: '&#9769;'
  },
  {
		emoji: '♰',
    name: 'West Syriac Cross',
    html: '&#9840;'
  },
  {
		emoji: '♱',
    name: 'East Syriac Cross',
    html: '&#9841;'
  },
  {
		emoji: '✚',
    name: 'Heavy Cross',
    html: '&#10010;'
  },
  {
		emoji: '✠',
    name: 'Maltese Cross',
    html: '&#10016;'
  },
  {
    emoji: '🎵',
    name: 'Musical Note',
    hexcode: 'U + 1F3B5;',
    html: '&#127925;'
  },
  {
    emoji: '🎶',
    name: 'Multiple Musical Notes',
    hexcode: 'U + 1F3B6;',
    html: '&#127926;'
  },
  {
    emoji: '🎼',
    name: 'Musical Score',
    hexcode: 'U + 1F3BC;',
    html: '&#127932;'
  },
  {
    emoji: '🎹',
    name: 'Musical Keyboard',
    hexcode: 'U + 1F3B9;',
    html: '&#127929;'
  },
  {
    emoji: '🎻',
    name: 'Violin',
    hexcode: 'U + 1F3BB;',
    html: '&#127931;'
  },
  {
    emoji: '🎷',
    name: 'Saxophone',
    hexcode: 'U + 1F3B7;',
    html: '&#127927;'
  },
  {
    emoji: '🎸',
    name: 'Guitar',
    hexcode: 'U + 1F3B8;',
    html: '&#127928;'
  },
  {
    emoji: '🎺',
    name: 'Trumpet',
    hexcode: 'U + 1F3BA;',
    html: '&#127930;'
  },
  {
    emoji: '🥁',
    name: 'Drum',
    hexcode: 'U + 1F941;',
    html: '&#129345;'
  },
  {
    emoji: '🎙',
    name: 'Studio Microphone',
    hexcode: 'U + 1F399;',
    html: '&#127897;'
  },
  {
    emoji: '🎚',
    name: 'Level Slider',
    hexcode: 'U + 1F39A;',
    html: '&#127898;'
  },
  {
    emoji: '🎛',
    name: 'Control Knobs',
    hexcode: 'U + 1F39B;',
    html: '&#127899;'
  },
  {
    emoji: '🎤',
    name: 'Microphone',
    hexcode: 'U + 1F3A4;',
    html: '&#127908;'
  },
  {
    emoji: '🎧',
    name: 'Headphone',
    hexcode: 'U + 1F3A7;',
    html: '&#127911;'
  },
  {
    emoji: '📻',
    name: 'Radio',
    hexcode: 'U + 1F4FB;',
    html: '&#128251;'
  },
 {
    emoji: '😀',
    name: 'Grinning Face',
    hexcode: 'U+1F600',
    html: '&#128512'
  },
  {
    emoji: '😃',
    name: 'Grinning Face With Big Eyes',
    hexcode: 'U+1F603',
    html: '&#128515'
  },
  {
    emoji: '😄',
    name: 'Grinning Face With Smiling Eyes',
    hexcode: 'U+1F604',
    html: '&#128516'
  },
  {
    emoji: '😁',
    name: 'Beaming Face With Smiling Eyes',
    hexcode: 'U+1F601',
    html: '&#128513'
  },
  {
    emoji: '😆',
    name: 'Grinning Squinting Face',
    hexcode: 'U+1F606',
    html: '&#128518'
  },
  {
    emoji: '😅',
    name: 'Grinning Face With Sweat',
    hexcode: 'U+1F605',
    html: '&#128517'
  },
  {
    emoji: '🤣',
    name: 'Rolling On The Floor Laughing',
    hexcode: 'U+1F923',
    html: '&#129315'
  },
  {
    emoji: '😂',
    name: 'Face With Tears Of Joy',
    hexcode: 'U+1F602',
    html: '&#128514'
  },
  {
    emoji: '🙂',
    name: 'Slightly Smiling Face',
    hexcode: 'U+1F642',
    html: '&#128578'
  },
  {
    emoji: '🙃',
    name: 'Upside-Down Face',
    hexcode: 'U+1F643',
    html: '&#128579'
  },
  {
    emoji: '😉',
    name: 'Winking Face',
    hexcode: 'U+1F609',
    html: '&#128521'
  },
  {
    emoji: '😊',
    name: 'Smiling Face With Smiling Eyes',
    hexcode: 'U+1F60A',
    html: '&#128522'
  },
  {
    emoji: '😇',
    name: 'Smiling Face With Halo',
    hexcode: 'U+1F607',
    html: '&#128519'
  },
  {
    emoji: '🥰',
    name: 'Smiling Face With 3 Hearts',
    hexcode: 'U+1F970',
    html: '&#129392'
  },
  {
    emoji: '😍',
    name: 'Smiling Face With Heart-Eyes',
    hexcode: 'U+1F60D',
    html: '&#128525'
  },
  {
    emoji: '🤩',
    name: 'Star-Struck',
    hexcode: 'U+1F929',
    html: '&#129321'
  },
  {
    emoji: '😘',
    name: 'Face Blowing A Kiss',
    hexcode: 'U+1F618',
    html: '&#128536'
  },
  {
    emoji: '😗',
    name: 'Kissing Face',
    hexcode: 'U+1F617',
    html: '&#128535'
  },
  {
    emoji: '☺',
    name: 'Smiling Face',
    hexcode: 'U+263A',
    html: '&#9786'
  },
  {
    emoji: '😚',
    name: 'Kissing Face With Closed Eyes',
    hexcode: 'U+1F61A',
    html: '&#128538'
  },
  {
    emoji: '😙',
    name: 'Kissing Face With Smiling Eyes',
    hexcode: 'U+1F619',
    html: '&#128537'
  },
  {
    emoji: '😋',
    name: 'Face Savoring Food',
    hexcode: 'U+1F60B',
    html: '&#128523'
  },
  {
    emoji: '😛',
    name: 'Face With Tongue',
    hexcode: 'U+1F61B',
    html: '&#128539'
  },
  {
    emoji: '😜',
    name: 'Winking Face With Tongue',
    hexcode: 'U+1F61C',
    html: '&#128540'
  },
  {
    emoji: '🤪',
    name: 'Zany Face',
    hexcode: 'U+1F92A',
    html: '&#129322'
  },
  {
    emoji: '😝',
    name: 'Squinting Face With Tongue',
    hexcode: 'U+1F61D',
    html: '&#128541'
  },
  {
    emoji: '🤑',
    name: 'Money-Mouth Face',
    hexcode: 'U+1F911',
    html: '&#129297'
  },
  {
    emoji: '🤗',
    name: 'Hugging Face',
    hexcode: 'U+1F917',
    html: '&#129303'
  },
  {
    emoji: '🤭',
    name: 'Face With Hand Over Mouth',
    hexcode: 'U+1F92D',
    html: '&#129325'
  },
  {
    emoji: '🤫',
    name: 'Shushing Face',
    hexcode: 'U+1F92B',
    html: '&#129323'
  },
  {
    emoji: '🤔',
    name: 'Thinking Face',
    hexcode: 'U+1F914',
    html: '&#129300'
  },
  {
    emoji: '🤐',
    name: 'Zipper-Mouth Face',
    hexcode: 'U+1F910',
    html: '&#129296'
  },
  {
    emoji: '🤨',
    name: 'Face With Raised Eyebrow',
    hexcode: 'U+1F928',
    html: '&#129320'
  },
  {
    emoji: '😐',
    name: 'Neutral Face',
    hexcode: 'U+1F610',
    html: '&#128528'
  },
  {
    emoji: '😑',
    name: 'Expressionless Face',
    hexcode: 'U+1F611',
    html: '&#128529'
  },
  {
    emoji: '😶',
    name: 'Face Without Mouth',
    hexcode: 'U+1F636',
    html: '&#128566'
  },
  {
    emoji: '😏',
    name: 'Smirking Face',
    hexcode: 'U+1F60F',
    html: '&#128527'
  },
  {
    emoji: '😒',
    name: 'Unamused Face',
    hexcode: 'U+1F612',
    html: '&#128530'
  },
  {
    emoji: '🙄',
    name: 'Face With Rolling Eyes',
    hexcode: 'U+1F644',
    html: '&#128580'
  },
  {
    emoji: '😬',
    name: 'Grimacing Face',
    hexcode: 'U+1F62C',
    html: '&#128556'
  },
  {
    emoji: '🤥',
    name: 'Lying Face',
    hexcode: 'U+1F925',
    html: '&#129317'
  },
  {
    emoji: '😌',
    name: 'Relieved Face',
    hexcode: 'U+1F60C',
    html: '&#128524'
  },
  {
    emoji: '😔',
    name: 'Pensive Face',
    hexcode: 'U+1F614',
    html: '&#128532'
  },
  {
    emoji: '😪',
    name: 'Sleepy Face',
    hexcode: 'U+1F62A',
    html: '&#128554'
  },
  {
    emoji: '🤤',
    name: 'Drooling Face',
    hexcode: 'U+1F924',
    html: '&#129316'
  },
  {
    emoji: '😴',
    name: 'Sleeping Face',
    hexcode: 'U+1F634',
    html: '&#128564'
  },
  {
    emoji: '😷',
    name: 'Face With Medical Mask',
    hexcode: 'U+1F637',
    html: '&#128567'
  },
  {
    emoji: '🤒',
    name: 'Face With Thermometer',
    hexcode: 'U+1F912',
    html: '&#129298'
  },
  {
    emoji: '🤕',
    name: 'Face With Head-Bandage',
    hexcode: 'U+1F915',
    html: '&#129301'
  },
  {
    emoji: '🤢',
    name: 'Nauseated Face',
    hexcode: 'U+1F922',
    html: '&#129314'
  },
  {
    emoji: '🤮',
    name: 'Face Vomiting',
    hexcode: 'U+1F92E',
    html: '&#129326'
  },
  {
    emoji: '🤧',
    name: 'Sneezing Face',
    hexcode: 'U+1F927',
    html: '&#129319'
  },
  {
    emoji: '🥵',
    name: 'Hot Face',
    hexcode: 'U+1F975',
    html: '&#129397'
  },
  {
    emoji: '🥶',
    name: 'Cold Face',
    hexcode: 'U+1F976',
    html: '&#129398'
  },
  {
    emoji: '🥴',
    name: 'Woozy Face',
    hexcode: 'U+1F974',
    html: '&#129396'
  },
  {
    emoji: '😵',
    name: 'Dizzy Face',
    hexcode: 'U+1F635',
    html: '&#128565'
  },
  {
    emoji: '🤯',
    name: 'Exploding Head',
    hexcode: 'U+1F92F',
    html: '&#129327'
  },
  {
    emoji: '🤠',
    name: 'Cowboy Hat Face',
    hexcode: 'U+1F920',
    html: '&#129312'
  },
  {
    emoji: '🥳',
    name: 'Partying Face',
    hexcode: 'U+1F973',
    html: '&#129395'
  },
  {
    emoji: '😎',
    name: 'Smiling Face With Sunglasses',
    hexcode: 'U+1F60E',
    html: '&#128526'
  },
  {
    emoji: '🤓',
    name: 'Nerd Face',
    hexcode: 'U+1F913',
    html: '&#129299'
  },
  {
    emoji: '🧐',
    name: 'Face With Monocle',
    hexcode: 'U+1F9D0',
    html: '&#129488'
  },
  {
    emoji: '😕',
    name: 'Confused Face',
    hexcode: 'U+1F615',
    html: '&#128533'
  },
  {
    emoji: '😟',
    name: 'Worried Face',
    hexcode: 'U+1F61F',
    html: '&#128543'
  },
  {
    emoji: '🙁',
    name: 'Slightly Frowning Face',
    hexcode: 'U+1F641',
    html: '&#128577'
  },
  {
    emoji: '☹',
    name: 'Frowning Face',
    hexcode: 'U+2639',
    html: '&#9785'
  },
  {
    emoji: '😮',
    name: 'Face With Open Mouth',
    hexcode: 'U+1F62E',
    html: '&#128558'
  },
  {
    emoji: '😯',
    name: 'Hushed Face',
    hexcode: 'U+1F62F',
    html: '&#128559'
  },
  {
    emoji: '😲',
    name: 'Astonished Face',
    hexcode: 'U+1F632',
    html: '&#128562'
  },
  {
    emoji: '😳',
    name: 'Flushed Face',
    hexcode: 'U+1F633',
    html: '&#128563'
  },
  {
    emoji: '🥺',
    name: 'Pleading Face',
    hexcode: 'U+1F97A',
    html: '&#129402'
  },
  {
    emoji: '😦',
    name: 'Frowning Face With Open Mouth',
    hexcode: 'U+1F626',
    html: '&#128550'
  },
  {
    emoji: '😧',
    name: 'Anguished Face',
    hexcode: 'U+1F627',
    html: '&#128551'
  },
  {
    emoji: '😨',
    name: 'Fearful Face',
    hexcode: 'U+1F628',
    html: '&#128552'
  },
  {
    emoji: '😰',
    name: 'Anxious Face With Sweat',
    hexcode: 'U+1F630',
    html: '&#128560'
  },
  {
    emoji: '😥',
    name: 'Sad But Relieved Face',
    hexcode: 'U+1F625',
    html: '&#128549'
  },
  {
    emoji: '😢',
    name: 'Crying Face',
    hexcode: 'U+1F622',
    html: '&#128546'
  },
  {
    emoji: '😭',
    name: 'Loudly Crying Face',
    hexcode: 'U+1F62D',
    html: '&#128557'
  },
  {
    emoji: '😱',
    name: 'Face Screaming In Fear',
    hexcode: 'U+1F631',
    html: '&#128561'
  },
  {
    emoji: '😖',
    name: 'Confounded Face',
    hexcode: 'U+1F616',
    html: '&#128534'
  },
  {
    emoji: '😣',
    name: 'Persevering Face',
    hexcode: 'U+1F623',
    html: '&#128547'
  },
  {
    emoji: '😞',
    name: 'Disappointed Face',
    hexcode: 'U+1F61E',
    html: '&#128542'
  },
  {
    emoji: '😓',
    name: 'Downcast Face With Sweat',
    hexcode: 'U+1F613',
    html: '&#128531'
  },
  {
    emoji: '😩',
    name: 'Weary Face',
    hexcode: 'U+1F629',
    html: '&#128553'
  },
  {
    emoji: '😫',
    name: 'Tired Face',
    hexcode: 'U+1F62B',
    html: '&#128555'
  },
  {
    emoji: '😤',
    name: 'Face With Steam From Nose',
    hexcode: 'U+1F624',
    html: '&#128548'
  },
  {
    emoji: '😡',
    name: 'Pouting Face',
    hexcode: 'U+1F621',
    html: '&#128545'
  },
  {
    emoji: '😠',
    name: 'Angry Face',
    hexcode: 'U+1F620',
    html: '&#128544'
  },
  {
    emoji: '🤬',
    name: 'Face With Symbols On Mouth',
    hexcode: 'U+1F92C',
    html: '&#129324'
  },
  {
    emoji: '😈',
    name: 'Smiling Face With Horns',
    hexcode: 'U+1F608',
    html: '&#128520'
  },
  {
    emoji: '👿',
    name: 'Angry Face With Horns',
    hexcode: 'U+1F47F',
    html: '&#128127'
  },
  {
    emoji: '💀',
    name: 'Skull',
    hexcode: 'U+1F480',
    html: '&#128128'
  },
  {
    emoji: '☠',
    name: 'Skull And Crossbones',
    hexcode: 'U+2620',
    html: '&#9760'
  },
  {
    emoji: '💩',
    name: 'Pile Of Poo',
    hexcode: 'U+1F4A9',
    html: '&#128169'
  },
  {
    emoji: '🤡',
    name: 'Clown Face',
    hexcode: 'U+1F921',
    html: '&#129313'
  },
  {
    emoji: '👹',
    name: 'Ogre',
    hexcode: 'U+1F479',
    html: '&#128121'
  },
  {
    emoji: '👺',
    name: 'Goblin',
    hexcode: 'U+1F47A',
    html: '&#128122'
  },
  {
    emoji: '👻',
    name: 'Ghost',
    hexcode: 'U+1F47B',
    html: '&#128123'
  },
  {
    emoji: '👽',
    name: 'Alien',
    hexcode: 'U+1F47D',
    html: '&#128125'
  },
  {
    emoji: '👾',
    name: 'Alien Monster',
    hexcode: 'U+1F47E',
    html: '&#128126'
  },
  {
    emoji: '🤖',
    name: 'Robot Face',
    hexcode: 'U+1F916',
    html: '&#129302'
  },
  {
    emoji: '😺',
    name: 'Grinning Cat Face',
    hexcode: 'U+1F63A',
    html: '&#128570'
  },
  {
    emoji: '😸',
    name: 'Grinning Cat Face With Smiling Eyes',
    hexcode: 'U+1F638',
    html: '&#128568'
  },
  {
    emoji: '😹',
    name: 'Cat Face With Tears Of Joy',
    hexcode: 'U+1F639',
    html: '&#128569'
  },
  {
    emoji: '😻',
    name: 'Smiling Cat Face With Heart-Eyes',
    hexcode: 'U+1F63B',
    html: '&#128571'
  },
  {
    emoji: '😼',
    name: 'Cat Face With Wry Smile',
    hexcode: 'U+1F63C',
    html: '&#128572'
  },
  {
    emoji: '😽',
    name: 'Kissing Cat Face',
    hexcode: 'U+1F63D',
    html: '&#128573'
  },
  {
    emoji: '🙀',
    name: 'Weary Cat Face',
    hexcode: 'U+1F640',
    html: '&#128576'
  },
  {
    emoji: '😿',
    name: 'Crying Cat Face',
    hexcode: 'U+1F63F',
    html: '&#128575'
  },
  {
    emoji: '😾',
    name: 'Pouting Cat Face',
    hexcode: 'U+1F63E',
    html: '&#128574'
  },
  {
    emoji: '🙈',
    name: 'See-No-Evil Monkey',
    hexcode: 'U+1F648',
    html: '&#128584'
  },
  {
    emoji: '🙉',
    name: 'Hear-No-Evil Monkey',
    hexcode: 'U+1F649',
    html: '&#128585'
  },
  {
    emoji: '🙊',
    name: 'Speak-No-Evil Monkey',
    hexcode: 'U+1F64A',
    html: '&#128586'
  },
  {
    emoji: '💯',
    name: 'Hundred Points',
    hexcode: 'U+1F4AF',
    html: '&#128175'
  },
  {
    emoji: '💢',
    name: 'Anger Symbol',
    hexcode: 'U+1F4A2',
    html: '&#128162'
  },
  {
    emoji: '💥',
    name: 'Collision',
    hexcode: 'U+1F4A5',
    html: '&#128165'
  },
  {
    emoji: '💫',
    name: 'Dizzy',
    hexcode: 'U+1F4AB',
    html: '&#128171'
  },
  {
    emoji: '💦',
    name: 'Sweat Droplets',
    hexcode: 'U+1F4A6',
    html: '&#128166'
  },
  {
    emoji: '💨',
    name: 'Dashing Away',
    hexcode: 'U+1F4A8',
    html: '&#128168'
  },
  {
    emoji: '🕳',
    name: 'Hole',
    hexcode: 'U+1F573',
    html: '&#128371'
  },
  {
    emoji: '💣',
    name: 'Bomb',
    hexcode: 'U+1F4A3',
    html: '&#128163'
  },
  {
    emoji: '💬',
    name: 'Speech Balloon',
    hexcode: 'U+1F4AC',
    html: '&#128172'
  },
  {
    emoji: '🗨',
    name: 'Left Speech Bubble',
    hexcode: 'U+1F5E8',
    html: '&#128488'
  },
  {
    emoji: '🗯',
    name: 'Right Anger Bubble',
    hexcode: 'U+1F5EF',
    html: '&#128495'
  },
  {
    emoji: '💭',
    name: 'Thought Balloon',
    hexcode: 'U+1F4AD',
    html: '&#128173'
  },
  {
    emoji: '💤',
    name: 'Zzz',
    hexcode: 'U+1F4A4',
    html: '&#128164'
  },
  {
    emoji: '👋',
    name: 'Waving Hand',
    hexcode: 'U+1F44B',
    html: '&#128075'
  },
  {
    emoji: '🤚',
    name: 'Raised Back Of Hand',
    hexcode: 'U+1F91A',
    html: '&#129306'
  },
  {
    emoji: '🖐',
    name: 'Hand With Fingers Splayed',
    hexcode: 'U+1F590',
    html: '&#128400'
  },
  {
    emoji: '✋',
    name: 'Raised Hand',
    hexcode: 'U+270B',
    html: '&#9995'
  },
  {
    emoji: '🖖',
    name: 'Vulcan Salute',
    hexcode: 'U+1F596',
    html: '&#128406'
  },
  {
    emoji: '👌',
    name: 'Ok Hand',
    hexcode: 'U+1F44C',
    html: '&#128076'
  },
  {
    emoji: '✌',
    name: 'Victory Hand',
    hexcode: 'U+270C',
    html: '&#9996'
  },
  {
    emoji: '🤞',
    name: 'Crossed Fingers',
    hexcode: 'U+1F91E',
    html: '&#129310'
  },
  {
    emoji: '🤟',
    name: 'Love-You Gesture',
    hexcode: 'U+1F91F',
    html: '&#129311'
  },
  {
    emoji: '🤘',
    name: 'Sign Of The Horns',
    hexcode: 'U+1F918',
    html: '&#129304'
  },
  {
    emoji: '🤙',
    name: 'Call Me Hand',
    hexcode: 'U+1F919',
    html: '&#129305'
  },
  {
    emoji: '👈',
    name: 'Backhand Index Pointing Left',
    hexcode: 'U+1F448',
    html: '&#128072'
  },
  {
    emoji: '👉',
    name: 'Backhand Index Pointing Right',
    hexcode: 'U+1F449',
    html: '&#128073'
  },
  {
    emoji: '👆',
    name: 'Backhand Index Pointing Up',
    hexcode: 'U+1F446',
    html: '&#128070'
  },
  {
    emoji: '🖕',
    name: 'Middle Finger',
    hexcode: 'U+1F595',
    html: '&#128405'
  },
  {
    emoji: '👇',
    name: 'Backhand Index Pointing Down',
    hexcode: 'U+1F447',
    html: '&#128071'
  },
  {
    emoji: '☝',
    name: 'Index Pointing Up',
    hexcode: 'U+261D',
    html: '&#9757'
  },
  {
    emoji: '👍',
    name: 'Thumbs Up',
    hexcode: 'U+1F44D',
    html: '&#128077'
  },
  {
    emoji: '👎',
    name: 'Thumbs Down',
    hexcode: 'U+1F44E',
    html: '&#128078'
  },
  {
    emoji: '✊',
    name: 'Raised Fist',
    hexcode: 'U+270A',
    html: '&#9994'
  },
  {
    emoji: '👊',
    name: 'Oncoming Fist',
    hexcode: 'U+1F44A',
    html: '&#128074'
  },
  {
    emoji: '🤛',
    name: 'Left-Facing Fist',
    hexcode: 'U+1F91B',
    html: '&#129307'
  },
  {
    emoji: '🤜',
    name: 'Right-Facing Fist',
    hexcode: 'U+1F91C',
    html: '&#129308'
  },
  {
    emoji: '👏',
    name: 'Clapping Hands',
    hexcode: 'U+1F44F',
    html: '&#128079'
  },
  {
    emoji: '🙌',
    name: 'Raising Hands',
    hexcode: 'U+1F64C',
    html: '&#128588'
  },
  {
    emoji: '👐',
    name: 'Open Hands',
    hexcode: 'U+1F450',
    html: '&#128080'
  },
  {
    emoji: '🤲',
    name: 'Palms Up Together',
    hexcode: 'U+1F932',
    html: '&#129330'
  },
  {
    emoji: '🤝',
    name: 'Handshake',
    hexcode: 'U+1F91D',
    html: '&#129309'
  },
  {
    emoji: '🙏',
    name: 'Folded Hands',
    hexcode: 'U+1F64F',
    html: '&#128591'
  },
  {
    emoji: '✍',
    name: 'Writing Hand',
    hexcode: 'U+270D',
    html: '&#9997'
  },
  {
    emoji: '💅',
    name: 'Nail Polish',
    hexcode: 'U+1F485',
    html: '&#128133'
  },
  {
    emoji: '🤳',
    name: 'Selfie',
    hexcode: 'U+1F933',
    html: '&#129331'
  },
  {
    emoji: '💪',
    name: 'Flexed Biceps',
    hexcode: 'U+1F4AA',
    html: '&#128170'
  },
  {
    emoji: '🦵',
    name: 'Leg',
    hexcode: 'U+1F9B5',
    html: '&#129461'
  },
  {
    emoji: '🦶',
    name: 'Foot',
    hexcode: 'U+1F9B6',
    html: '&#129462'
  },
  {
    emoji: '👂',
    name: 'Ear',
    hexcode: 'U+1F442',
    html: '&#128066'
  },
  {
    emoji: '👃',
    name: 'Nose',
    hexcode: 'U+1F443',
    html: '&#128067'
  },
  {
    emoji: '🧠',
    name: 'Brain',
    hexcode: 'U+1F9E0',
    html: '&#129504'
  },
  {
    emoji: '🦷',
    name: 'Tooth',
    hexcode: 'U+1F9B7',
    html: '&#129463'
  },
  {
    emoji: '🦴',
    name: 'Bone',
    hexcode: 'U+1F9B4',
    html: '&#129460'
  },
  {
    emoji: '👀',
    name: 'Eyes',
    hexcode: 'U+1F440',
    html: '&#128064'
  },
  {
    emoji: '👁',
    name: 'Eye',
    hexcode: 'U+1F441',
    html: '&#128065'
  },
  {
    emoji: '👅',
    name: 'Tongue',
    hexcode: 'U+1F445',
    html: '&#128069'
  },
  {
    emoji: '👄',
    name: 'Mouth',
    hexcode: 'U+1F444',
    html: '&#128068'
  },
  {
    emoji: '🗣',
    name: 'Speaking Head',
    hexcode: 'U+1F5E3',
    html: '&#128483'
  },
  {
    emoji: '👤',
    name: 'Bust In Silhouette',
    hexcode: 'U+1F464',
    html: '&#128100'
  },
  {
    emoji: '👥',
    name: 'Busts In Silhouette',
    hexcode: 'U+1F465',
    html: '&#128101'
 },
 {
    emoji: '♈',
    name: 'Aries',
    html: '&#9800;'
  },
  {
    emoji: '♉',
    name: 'Taurus',
    html: '&#9801;'
  },
  {
    emoji: '♊',
    name: 'Gemini',
    html: '&#9802;'
  },
  {
    emoji: '♋',
    name: 'Cancer',
    html: '&#9803;'
  },
  {
    emoji: '♌',
    name: 'Leo',
    html: '&#9804;'
  },
  {
    emoji: '♍',
    name: 'Virgo',
    html: '&#9805;'
  },
  {
    emoji: '♎',
    name: 'Libra',
    html: '&#9806;'
  },
  {
    emoji: '♏',
    name: 'Scorpio',
    html: '&#9807;'
  },
  {
    emoji: '♐',
    name: 'Sagittarius',
    html: '&#9808;'
  },
  {
    emoji: '♑',
    name: 'Capricorn',
    html: '&#9809;'
  },
  {
    emoji: '♒',
    name: 'Aquarius',
    html: '&#9810;'
  },
  {
    emoji: '♓',
    name: 'Pisces',
    html: '&#9811;'
  },
  {
    emoji: '♂',
    name: 'Male Symbol',
    html: '&#9794;'
  },
  {
    emoji: '♀',
    name: 'Female Symbol',
    html: '&#9792;'
  },
  {
    emoji: '⚥',
    name: 'Male and Female Symbol',
    html: '&#9893;'
  },
  {
    emoji: '⚢',
    name: 'Doubled Female Symbol',
    html: '&#9890;'
  },
  {
    emoji: '⚣',
    name: 'Doubled Male Symbol',
    html: '&#9891;'
  },
  {
    emoji: '⚤',
    name: 'Interlocked Female And Male Symbol',
    html: '&#9892;'
  },
  {
    emoji: '⚦',
    name: 'Male With Stroke Symbol',
    html: '&#9894;'
  },
  {
    emoji: '⚧',
    name: 'Male With Stroke And Male And Female',
    html: '&#9895;'
  },
  {
    emoji: '⚨',
    name: 'Vertical Male With Stroke Symbol',
    html: '&#9896;'
  },
  {
    emoji: '⚩',
    name: 'Horizontal Male With Stroke Symbol',
    html: '&#9897;'
  },
  {
    emoji: '♂',
    name: 'Male Symbol alt code',
    html: '11'
  },
  {
    emoji: '♀',
    name: 'Female Symbol alt code',
    html: '12'
  }
]