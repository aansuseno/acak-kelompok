var arr_siswa = []
var siswa_belum_dipilih = []
var lecana = []
var id_lecana_terpilih = -1
var jml_kelompok = -1
var max_siswa = -1
var anggota_kelompok = ''
var sebar_satukan = ''
var siswa_terpilih_sementara = []
var siswa_preview = []
var kelompok_dengan_siswa = []
var terakhir_kelompok_diisi = 0

const get_siswa = () => {
	$.getJSON('/siswa/'+id_kelas_master, (res) => {
		arr_siswa = res.data
		siswa_belum_dipilih = [...arr_siswa]
	})
}

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

const buat = {
	buat: () => {
		$('#buat-kelompok').show()
	},
	perbarui_lecana: () => {
		lecana.length = 0; // hapus array

		// ambil semua lecana yang masih avaiable
		siswa_belum_dipilih.forEach(i => {
			i.lecana.forEach(j => {
				ada = false
				for (var k = 0; k < lecana.length; k++) {
					if (lecana[k].nama == j.nama) {
						ada = true
						break
					}
				}
				if (!ada) {lecana.push(j)}
			})
		})

		// cari jika ada siswa tak punya lecana\
		var ada = false
		for (var i = 0; i < siswa_belum_dipilih.length; i++) {
			if (siswa_belum_dipilih[i].lecana.length == 0) {
				ada = true
				break
			}
		}
		if (ada) {
			lecana.push({ikon: "&#8709;", nama: "tanpa-lecana"})
		}
		lecana.push({ikon: '&#127757;', nama: "semua"})

		var lecana_tampil = ''
		lecana.forEach((i, idx) => {
			lecana_tampil += `<button class="btn btn-outline-info btn-lg m-1 lecana-tampil" title="${i.nama}" onclick="buat.lecana_ganti(${idx})" id="lecana-tampil-${idx}">${i.ikon}</button>`
		})

		$('.kumpulan-lecana').html(lecana_tampil)
	},
	get_jml: () => {
		jml = parseInt($('#amount').val())
		anggota_kelompok = $('#anggota_kelompok').val()
		var jml_siswa = arr_siswa.length
		$('#btn-buat-kelompok').remove()

		// validasi jml
		if (!(jml >= 2) || jml > Math.floor(jml_siswa/2)) {
			alert('min 2 anggota kelompok atau 2 kelompok dan max '+Math.floor(jml_siswa/2))
			return
		}

		if (anggota_kelompok == 'kelompok') {
			jml_kelompok = jml
			max_siswa = Math.floor(jml_siswa/jml)
		} else {
			jml_kelompok = Math.floor(jml_siswa/jml)
			max_siswa = jml
		}

		// buat wadah kelompok
		for (var i = 0; i < jml_kelompok; i++) {
			$('#wadah-kelompok').append(`
				<div class="list-group d-inline-block m-2" id="list-siswa-kelompok-${i}">
				  <div class="list-group-item list-group-item-action bg-primary">
				    <div class="d-flex w-100 justify-content-between">
				      <h5 class="mb-1"><b>Kelompok ${i+1}</b></h5>
				    </div>
				  </div>
				</div>`)
		}

		for (let i = 0; i < jml_kelompok; i++) {
		  kelompok_dengan_siswa.push([]);
		}
		buat.perbarui_lecana()
		$('#buat-kelompok').hide()
		$('#kelompokkan-lecana').show()
		buat.lecana_ganti(0)
	},
	lecana_ganti: (id) => {
		id_lecana_terpilih = id
		$('.lecana-tampil').removeClass('btn-secondary')
		$('#lecana-tampil-'+id).addClass('btn-secondary')
		$('#tombol-sebar').html(`
			<p>Siswa <b>${lecana[id].nama}</b>${lecana[id].ikon}:</p>
			<div class="d-flex justify-content-center">
			  <button class="btn btn-outline-primary rounded-pill mr-1" onclick="buat.disebar(${id})"><b>Disebar</b></button>
			  <button class="btn btn-outline-primary rounded-pill ml-1" onclick="buat.jadikan_satu(${id})"><b>Jadikan Satu</b></button>
			</div>`)
	},
	acak: (id_lecana) => {
		$('.lecana-tampil').prop('disabled', true)
		$('#lecana-tampil-'+id_lecana).addClass('btn-secondary')
		$('.siswa-preview').remove()
		siswa_terpilih_sementara.length = 0
		siswa_preview.length = 0
		var nama_ikon = lecana[id_lecana].nama
		var sementara = []

		// ambil semua lecana dari siswayang masih avaiable
		if (nama_ikon != 'tanpa-lecana' && nama_ikon != 'semua') {
			siswa_belum_dipilih.forEach(i => {
				i.lecana.forEach(j => {
					if (j.nama == nama_ikon) {
						sementara.push(i)
					}
				})
			})
		} else if (nama_ikon == 'tanpa-lecana') {
			siswa_belum_dipilih.forEach(i => {
				if(i.lecana.length == 0) {
					sementara.push(i)
				}
			})
		} else {
			sementara = [...siswa_belum_dipilih]
		}

		siswa_terpilih_sementara = [...shuffleArray(sementara)]
	},
	disebar: (id_lecana) => {
		buat.acak(id_lecana)
		sebar_satukan = 'sebar'

		// isi siswa
		var kelompok_ke = terakhir_kelompok_diisi
		var loop = 0
		for (var i = 0; i < siswa_terpilih_sementara.length; i++) {
			if (buat.penuh_semua()) break;
			if (kelompok_ke == jml_kelompok) {kelompok_ke = 0}
			while($('#list-siswa-kelompok-'+kelompok_ke).children().length >= max_siswa+1) {
				kelompok_ke++
				if (kelompok_ke == jml_kelompok) {kelompok_ke = 0}
			}

			$('#list-siswa-kelompok-'+kelompok_ke).append(`
			<div class="list-group-item list-group-item-action siswa-preview" id="siswa-preview-${i}">
			  <div class="d-flex w-100 justify-content-between">
			    <h5 class="mb-1"><b>${siswa_terpilih_sementara[i].nama}</b></h5>
			    <button class="btn btn-outline-danger btn-sm ml-2" onclick="buat.hapus_preview(${i})">
			    	<i class="fa fa-times"></i>
			    </button>
			  </div>
			</div>`)
			siswa_preview.push({
				siswa: siswa_terpilih_sementara[i].id,
				kelompok: kelompok_ke
			})
			kelompok_ke++
		}

		$('.dadu-oke').html(`
			<button class="btn btn-secondary btn-lg mr-1" title="acak lagi, tanpa mereset semua" onclick="buat.cancel()">cancel</button>
			<button class="btn btn-outline-primary btn-lg mr-1" title="acak lagi, tanpa mereset semua" onclick="buat.disebar(${id_lecana})"><i class="fa fa-dice"></i></button>
			<button class="btn btn-primary btn-lg mr-1" onclick="buat.oke()">Oke <i class="fa fa-check"></i></button>`)
	},
	jadikan_satu : (id_lecana) => {
		buat.acak(id_lecana)
		sebar_satukan = 'satukan'

		// isi siswa
		var kelompok_terakhir = terakhir_kelompok_diisi
		var jml_anggota_kelompok = kelompok_dengan_siswa[kelompok_terakhir].length
		for (var i = 0; i < siswa_terpilih_sementara.length; i++) {
			if (buat.penuh_semua()) break;
			while($('#list-siswa-kelompok-'+kelompok_terakhir).children().length >= max_siswa+1){
				kelompok_terakhir++
				if (kelompok_terakhir == jml_kelompok) {
					kelompok_terakhir = 0
				}
			}
			$('#list-siswa-kelompok-'+kelompok_terakhir).append(`
			<div class="list-group-item list-group-item-action siswa-preview" id="siswa-preview-${i}">
			  <div class="d-flex w-100 justify-content-between">
			    <h5 class="mb-1"><b>${siswa_terpilih_sementara[i].nama}</b></h5>
			    <button class="btn btn-outline-danger btn-sm ml-2" onclick="buat.hapus_preview(${i})">
			    	<i class="fa fa-times"></i>
			    </button>
			  </div>
			</div>`)
			siswa_preview.push({
				siswa: siswa_terpilih_sementara[i].id,
				kelompok: kelompok_terakhir
			})
		}

		$('.dadu-oke').html(`
			<button class="btn btn-secondary btn-lg mr-1" title="acak lagi, tanpa mereset semua" onclick="buat.cancel()">cancel</button>
			<button class="btn btn-outline-primary btn-lg mr-1" title="acak lagi, tanpa mereset semua" onclick="buat.jadikan_satu(${id_lecana})"><i class="fa fa-dice"></i></button>
			<button class="btn btn-primary btn-lg mr-1" onclick="buat.oke()">Oke <i class="fa fa-check"></i></button>`)
	},
	cancel: () => {
		$('.lecana-tampil').prop('disabled', false)
		$('.dadu-oke').html(``)
		$('.siswa-preview').remove()
		sebar_satukan = ''
		siswa_terpilih_sementara.length = 0
	},
	hapus_preview: (idx) => {
		$('#siswa-preview-'+idx).remove()
		siswa_preview.splice(idx, 1);
	},
	oke: () => {
		$('.siswa-preview button').remove()
		$('.siswa-preview').removeAttr('id');
		$('.siswa-preview').removeClass('siswa-preview')
		siswa_preview.forEach(i => {
			var id_dihapus = buat.cari_id_siswa(i.siswa);
			siswa_belum_dipilih.splice(id_dihapus, 1)
			kelompok_dengan_siswa[i.kelompok].push(i.siswa)
		})
		siswa_preview.length = 0
		$('.dadu-oke').html('')
		$('.lecana-tampil').prop('disabled', false)

		if (siswa_belum_dipilih.length == 0) {
			$('.kumpulan-lecana').remove()
			$('#tombol-sebar').remove()
			$('.dadu-oke').remove()
			simpan_db()
			return
		}

		if (
			siswa_belum_dipilih.length < max_siswa
			|| (siswa_belum_dipilih.length > 0 && buat.penuh_semua())
		) {
			$('.kumpulan-lecana').html(`
				<b>Anggota yang belum dipilih hanya tersissa ${siswa_belum_dipilih.length}, silakan pilih gabungkan dengan kelompok lain atau buat kelompok sendiri.</b>
			`)
			$('#tombol-sebar').html(`
				<div class="d-flex justify-content-center">
				  <button class="btn btn-outline-primary rounded-pill mr-1" onclick="buat.gabung()"><b>Gabungkan</b></button>
				  <button class="btn btn-outline-primary rounded-pill ml-1" onclick="buat.pisah()"><b>Pisah</b></button>
				</div>
			`)
			return
		}

		buat.perbarui_lecana()
		buat.lecana_ganti(0)
	},
	cari_id_siswa: (id_siswa)=> {
		var dihapus = -1
		for (var i = 0; i < siswa_belum_dipilih.length; i++) {
			if (id_siswa == siswa_belum_dipilih[i].id) {
				dihapus = i;
				break;
			}
		}

		return dihapus
	},
	penuh_semua: () => {
		var full = true
		for (var i = 0; i < jml_kelompok; i++) {
			var jml = $('#list-siswa-kelompok-'+i).children().length
			if (jml < max_siswa+1) {
				full = false
				break
			}
		}

		return full
	},
	gabung: () => {
		max_siswa+=1
		buat.perbarui_lecana()
		buat.lecana_ganti(0)
	},
	pisah: () =>{
		jml_kelompok+=1
		kelompok_dengan_siswa.push([])
		$('#wadah-kelompok').append(`
			<div class="list-group d-inline-block m-2" id="list-siswa-kelompok-${jml_kelompok-1}">
			  <div class="list-group-item list-group-item-action bg-primary">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Kelompok ${jml_kelompok}</b></h5>
			    </div>
			  </div>
			</div>`)
		buat.perbarui_lecana()
		buat.lecana_ganti(0)
	}
}

const simpan_db = () => {
	$.ajax({
	  url: "/kelompok-simpan",
	  method: "POST",
	  data: {
	    kelompok: JSON.stringify(kelompok_dengan_siswa),
	    id_tugas: id_tugas_master
	  },
	  success: function(response) {
	    res = JSON.parse(response)
	    if (res.msg == 'success') {
	    	get_kelompok()
	    	$('#wadah-kelompok').remove()
	    	return
	    }

	    alert(res.msg)
	  },
	  error: function(xhr, status, error) {
	    alert(error);
	  }
	});
}