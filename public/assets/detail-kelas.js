
var now = 'siswa'
var id_siswa = -1
var lecana_siswa = []
var index_lecana = -1

function capitalizeName(name) {
	var words = name.toLowerCase().split(" ");
	for (var i = 0; i < words.length; i++) {
		words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
	}
	return words.join(" ");
}

function removeLastString(text) {
  var trimmedText = text.trim(); // Menghapus spasi di awal dan akhir teks
  var lastCommaIndex = trimmedText.lastIndexOf(",");
  var lastSpaceIndex = trimmedText.lastIndexOf(" ");
  var lastIndex = Math.max(lastCommaIndex, lastSpaceIndex); // Menentukan indeks terakhir dari koma atau spasi

  if (lastIndex !== -1) {
    var removedText = trimmedText.substr(0, lastIndex + 1); // Mengambil teks hingga indeks terakhir, termasuk tanda koma atau spasi
    return removedText;
  } else if (trimmedText.split(" ").length === 1) {
    return "";
  }

  return trimmedText;
}

function ganti_nama_dgn_template(nama) {
	$(".nama_saran_item").fadeOut("slideUp", function() {
		$(this).remove();
	});

	var nama_semen = $('#nama_siswa').val()

	$('#nama_siswa').val(removeLastString(nama_semen) + nama)
	$('#nama_siswa').focus()
}

var arr_nama_sementara = []

const tampil_saran_nama = () => {
	var nama_semen = $('#nama_siswa').val().split(/[ ,]+/).pop()
	if (nama_semen == '') {return}
	$.getJSON('/nama-template/'+nama_semen, function(data) {
		$(".nama_saran_item").fadeOut("slideUp", function() {
			$(this).remove();
		});
		if (data.length == 0) {
			$('#nama-saran').append(`
				<button class="btn btn-xs btn-outline-secondary mx-2 my-1 nama_saran_item">tidak ada nama mirip</button>
			`)
			return
		}
		arr_nama_sementara = []
		data.forEach(i => {
			if(arr_nama_sementara.indexOf(i.nama) == -1) {
				arr_nama_sementara.push(i.nama)
				var nama = capitalizeName(i.nama)
				$('#nama-saran').append(`
					<button class="btn btn-xs btn-outline-primary mx-2 my-1 nama_saran_item" onclick="ganti_nama_dgn_template('${nama}')">${nama}</button>
				`)
			}
		})
	})
	.fail(function(xhr, status, error) {
		console.error(error);
	});
}

var timeoutId;

function debounce(func, delay) {
	clearTimeout(timeoutId);
	timeoutId = setTimeout(func, delay);
}


// untuk tambah siswa
const modal_tambah = (now = 'siswa') => {
	$('#btn-simpan').show()
	$('#btn-simpan').remove()
	if (now == 'siswa') {
		$('#modalTitle').text('Tambah Siswa')
		$('#mdl-body').html(`
		<small>Nama Siswa, pisahkan dengan tanda koma:</small>
		<textarea name="" oninput="debounce(tampil_saran_nama, 500)" id="nama_siswa"class="form-control"></textarea>
		<div id="nama-saran" class="d-flex flex-wrap"></div>
		`)
		$('#footer-button').append('<button type="button" class="btn btn-primary" id="btn-simpan" onclick="proses_siswa.simpan_siswa()">Simpan Siswa</button>')
	} else if(now == 'tugas') {
		$('#modalTitle').text('Buat Tugas')
		$('#mdl-body').html(`
		<small>Nama:</small>
		<input type="text" autocomplete="off" id="nama_tugas" class="form-control" />
		<small>Keterangan:</small>
		<textarea name="" id="deskripsi_tugas"class="form-control"></textarea>
		`)
		$('#footer-button').append('<button type="button" class="btn btn-primary" id="btn-simpan" onclick="proses_tugas.simpan()">Simpan Tugas</button>')
	} else {
		$('#modalTitle').text('Buat Lecana')
		$('#mdl-body').html(`
		<small>Nama:</small>
		<input type="text" autocomplete="off" id="nama_lecana" class="form-control" />
		<small>Emoji:</small>
		<input type="text" autocomplete="off" id="emoji_lecana" class="form-control"/>
		<small>Keterangan:</small>
		<textarea id="deskripsi_lecana"class="form-control"></textarea>
		`)
		$('#footer-button').append('<button type="button" class="btn btn-primary" id="btn-simpan" onclick="proses_lecana.simpan()">Simpan Lecana</button>')
	}
}

const proses_siswa = {
	simpan_siswa : () => {
		$.ajax({
			url: '/siswa/'+id_kelas_,
			type: 'POST',
			data: {nama: $('#nama_siswa').val()},
			success: function(res) {
				$('#exampleModalCenter').modal('hide');
				if(res == 'notoke') {
					$('#err').show()
					$('.text_err').text('Nama tidak valid');
				} else if (res == 'kelebihan') {
					$('#err').show()
					$('.text_err').text('Kelas yang Anda buat sudah melebihi batas.');
				} else if(res == 'oke') {
					proses_siswa.show_siswa()
				}
			}
		});
	},
	show_siswa : () => {
		$('.kumpulan').hide()
		$('.siswa').show()
		$.getJSON( "/siswa/"+id_kelas_, function( res ) {
			if(res.count == 0) {
				$('#btn-tugas').prop('disabled', true)
				$('#btn-lecana').prop('disabled', true)
				return
			}

			if(res.count < 4) {
				$('#btn-tugas').prop('disabled', true)
				$('#btn-lecana').prop('disabled', true)
			} else {
				$('#btn-tugas').prop('disabled', false)
				$('#btn-lecana').prop('disabled', false)
			}

			$('#data-siswa').html('')
			res.data.forEach((i, idx) => {

				// simpan emoji di variable
				lecana_siswa.push({id: i.id, lecana: i.lecana})

				var buat_lecana = ''
				i.lecana.forEach(j => {
					buat_lecana+=`<button class="btn btn-outline-info btn-xs m-1" title="${j.nama}" onclick="alert('${j.nama}')" id="on-table-lecana-${j.id_siswa_items}">${j.ikon}</button>`
				})

				$('#data-siswa').append(`
				<tr id="siswa-${i.id}">
					<td>${i.nama}</td>
					<td class="container" id="lecana-siswa-${i.id}">
						${buat_lecana}
					</td>
					<td class="d-flex flex-nowrap">
						<button onclick="proses_siswa.hapus_siswa(${i.id})" id="btn-hapus-siswa-${i.id}" class="btn btn-danger btn-xs" title="hapus siswa">
							<i class="fa fa-trash"></i>
						</button>
						<button onclick="proses_siswa.edit_siswa(${i.id})" id="btn-hapus-siswa-${i.id}" class="btn btn-warning btn-xs m-1 col-6" data-toggle="modal" data-target="#exampleModalCenter" title="edit siswa">
							<i class="fa fa-edit"></i>
						</button>
					</td>
				</tr>
				`)
			})
		});
	},
	hapus_siswa : (id) => {
		$('#btn-hapus-siswa-'+id).html('<i class="fa fa-spinner"></i>')
		$('#btn-hapus-siswa-'+id).prop('disabled', true)
		$.ajax({
			url: '/siswa-hapus/'+id_kelas_+'/'+id,
			success: function(res) {
				if (res=='success') {
					proses_siswa.show_siswa()
				} else {
					alert('something went wrong')
				}
			}
		});
	},
	edit_siswa : (id) => {
		id_siswa = id
		var nama = $('#siswa-'+id).find("td:first").text()
		var lecana_sekarang = ''
		index_lecana = lecana_siswa.findIndex(item => item.id == id);
		lecana_siswa[index_lecana].lecana.forEach(i => {
			lecana_sekarang += `
				<button
				 class="btn btn-outline-danger m-1"
				 title="hapus ${i.nama}"
				 onclick="proses_siswa.hapus_lecana_dari_siswa(${i.id_siswa_items})"
				 id="lecana-sekarang-hapus-${i.id_siswa_items}"
				 >${i.ikon}</button>
			`
		})

		$('#btn-simpan').hide()
		$('#modalTitle').text('Edit Data Siswa')
		$('#mdl-body').html(`
		<div class="input-group">
			<input type="text" id="edit-nama-siswa" value="${nama}" class="form-control" placeholder="Edit Nama">
			<div class="input-group-append">
				<button onclick="proses_siswa.edit_nama_siswa()" id="btn-edit-nama-siswa" class="btn btn-primary">Ubah</button>
			</div>
		</div>
		<br>
		<div class="container" id="lecana-dipakai">
				${lecana_sekarang}
		</div>
	
		<hr>
		<div class="container" id="lecana-baru">
			<center><i class="fa fa-spinner"></i></center>
		</div>
		`)
		
		$.getJSON('/items-belum-dipakai/'+id, (res) => {
			// cari yang sudah ada
			res.lecana.forEach((i, idx) => {
				res.lecana[idx]['disabled'] = false
				lecana_siswa[index_lecana].lecana.forEach(j => {
					if (i.ikon == j.ikon) {
						res.lecana[idx]['disabled'] = true
					}
				})
			})

			var lecana_belum_dipakai = ''
			res.lecana.forEach(i => {
				if (i.disabled) {
					return
				}
				lecana_belum_dipakai+=`
				<button
				 class="btn btn-outline-success m-1"
				 title="tambah ${i.nama}"
				 id="lecana-baru-${i.id}"
				 onclick="proses_siswa.tambah_lecana_ke_siswa(${i.id})"
				 >${i.ikon}</button>
				`
			})

			$('#lecana-baru').html(lecana_belum_dipakai)
		})
	},
	tambah_lecana_ke_siswa : (id_items) => {
		$('#lecana-baru-'+id_items).prop('disabled', true)
		$('#lecana-baru-'+id_items).html('<i class="fa fa-spinner"></i>')

		$.ajax({
			url: '/siswa-tambah-lecana',
			type: 'POST',
			data: {id_items: id_items, id_siswa: id_siswa},
			success: function(res) {
				var arr = JSON.parse(res);
				if (arr.msg == 'success') {
					// hapus dari view
					$('#lecana-baru-'+id_items).remove()
					
					// tambah
					$('#lecana-dipakai').append(`
						<button
					 class="btn btn-outline-danger m-1"
					 title="hapus ${arr.nama}"
					 onclick="proses_siswa.hapus_lecana_dari_siswa(${arr.id})"
					 id="lecana-sekarang-hapus-${arr.id}"
					 >${arr.ikon}</button>
					`)

					// tambah lecana di table
					$('#lecana-siswa-'+id_siswa).append(`
						<button
						 class="btn btn-outline-info btn-xs m-1"
						 title="${arr.nama}" onclick="alert('${arr.nama}')"
						 id="on-table-lecana-${arr.id}">${arr.ikon}</button>
					`)

					// tambah ke list lecana
					lecana_siswa[index_lecana].lecana.push({
						id_siswa_items: arr.id,
						nama: arr.nama,
						ikon: arr.ikon
					})

					return
				}

				$('#exampleModalCenter').modal('hide');
				alert(arr.msg)
			}
		});
	},
	hapus_lecana_dari_siswa : (id_siswa_items) => {
		$('#lecana-sekarang-hapus-'+id_siswa_items).prop('disabled', true)
		$('#lecana-sekarang-hapus-'+id_siswa_items).html('<i class="fa fa-spinner"></i>')

		$.ajax({
			url: '/siswa-hapus-lecana',
			type: 'POST',
			data: {id: id_siswa_items},
			success: function(res) {
				var arr = JSON.parse(res);
				if (arr.msg != 'success') {
					$('#exampleModalCenter').modal('hide');
					alert(arr.msg)
					return
				}
				
				// hapus array
				var index_lecana_dihapus = lecana_siswa[index_lecana].lecana.findIndex(item => item.id_siswa_items == id_siswa_items);
				lecana_siswa[index_lecana].lecana.splice(index_lecana_dihapus, 1);

				// hapus di view
				$('#lecana-sekarang-hapus-'+id_siswa_items).remove()
				$('#on-table-lecana-'+id_siswa_items).remove()

				// tambah lecana yang	dihapus
				$('#lecana-baru').append(`
				<button
				 class="btn btn-outline-success m-1"
				 title="tambah ${arr.nama}"
				 id="lecana-baru-${arr.id}"
				 onclick="proses_siswa.tambah_lecana_ke_siswa(${arr.id})"
				 >${arr.ikon}</button>
				`)
			}
		})
	},
	edit_nama_siswa : () => {
		$('#btn-edit-nama-siswa').prop('disabled', true)
		$('#btn-edit-nama-siswa').html('<i class="fa fa-spinner"></i>')
		var nama_baru = $('#edit-nama-siswa').val()
		$.ajax({
			url: '/siswa-edit',
			type: 'POST',
			data: {nama: nama_baru, id: id_siswa},
			success: function(res) {
				if(res == 'success') {
					$('#siswa-'+id_siswa).find("td:first").text(nama_baru)
					$('#btn-edit-nama-siswa').prop('disabled', false)
					$('#btn-edit-nama-siswa').html('Ubah')
					return ;
				}

				$('#exampleModalCenter').modal('hide');
				alert('Nama atau id tidak valid.');
				return ;
			}
		});
	}
}

const proses_tugas = {
	simpan : () => {
		$.ajax({
			url: '/tugas/'+id_kelas_,
			type: 'POST',
			data: {
				nama: $('#nama_tugas').val(),
				deskripsi: $('#deskripsi_tugas').val()
			},
			success: function(res) {
				res = JSON.parse(res)
				if (res.msg != 'success') {
					alert(res.msg)
					return
				}

				$('#exampleModalCenter').modal('hide');
				proses_tugas.show()
			}
		});
	},
	show : () => {
		$.getJSON( "/tugas/"+id_kelas_, function( res ) {
			$('#daftar-tugas').html('')
			res.forEach(i => {
				$('#daftar-tugas').append(`
				<div class="col-sm-6 col-md-4 col-lg-3 mb-3">
					<a href="/detail-tugas/${i.id}">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${i.nama}</h5>
							</div>
						</div>
					</a>
				</div>
				`)
			})
		});
	}
}

const proses_lecana = {
	tampil: () => {
		$('#kotak-lecana').show()
		$('#accordion').html('')
		$.getJSON('/items', function(data) {
			data.forEach((i, idx) => {
				var hapus = (i.is_default == 0) ? `<br>
					<div class="d-flex justify-content-end">
						<button class="btn btn-danger" onclick="proses_lecana.hapus(${i.id}, '${i.nama}')">
							<i class="fa fa-trash"></i>
						</button>
					</div>` : ''
				$('#accordion').append(`
					<div class="card">
						<div class="card-header" id="headingTwo-${idx}">
							<h5 class="mb-0">
								<button class="btn collapsed text-uppercase" data-toggle="collapse" data-target="#collapseTwo-${idx}" aria-expanded="false" aria-controls="collapseTwo-${idx}">
									${i.ikon} ${i.nama}
								</button>
							</h5>
						</div>
						<div id="collapseTwo-${idx}" class="collapse" aria-labelledby="headingTwo-${idx}" data-parent="#accordion">
							<div class="card-body">
								${i.deskripsi}
								${ hapus }
							</div>
						</div>
					</div>	
					`)
			})
		});
	},
	simpan: () => {
		var nama = $('#nama_lecana').val()
		var deskripsi = $('#deskripsi_lecana').val()
		var ikon = $('#emoji_lecana').val()

		$.ajax({
			url: '/buat-lecana',
			type: 'POST',
			data: {
				nama, 
				deskripsi,
				ikon
			},
			success: function(res) {
				if (res == 'success') {
					$('#exampleModalCenter').modal('hide');
					proses_lecana.tampil()
					return
				}

				alert(res)
			}
		});
	},
	hapus: (id, nama) => {
		if (confirm('Anda yakin ingin menghapus lecana '+nama)) {
			$.ajax({
				url: '/hapus-lecana/'+id,
				success: function(res) {
					if (res == 'success') {
						proses_lecana.tampil()
						return
					}
					alert(res)
				}
			});
		}
	}
}

function tampil_menu(menu) {
	$('.kumpulan').hide()
	$('.'+menu).show()

	$('.btn-atas').removeClass('btn-success text-light')
	$('#btn-'+menu).addClass('btn-success text-light')

	if (menu == 'tugas') {
		proses_tugas.show()
	} else if(menu == 'lecana') {
		proses_lecana.tampil()
	}
}

function edit_kelas() {
	$('#nama_kelas').prop('contenteditable', true)
	$('#nama_kelas').focus()

	$('#button-edit-nama-kelas').removeClass('btn-outline-warning')
	$('#button-edit-nama-kelas').addClass('btn-primary')
	$('#button-edit-nama-kelas').html('<i class="fa fa-save"></i>')
	$('#button-edit-nama-kelas').attr('onclick', 'proses_edit_kelas()')
}

function proses_edit_kelas() {
	$('#nama_kelas').prop('contenteditable', false)
	$('#button-edit-nama-kelas').attr(`disabled`, true)
	$('#button-edit-nama-kelas').html('<i class="fa fa-spinner"></i>')
	var nama_baru = $('#nama_kelas').html()

	if (nama_baru == nama_kelas_sekarang) return kembalikan_button_seperti_awal();

	$.ajax({
		url: '/edit-nama-kelas/'+id_kelas_,
		type: 'POST',
		data: {nama: nama_baru},
		success: function(res) {
			kembalikan_button_seperti_awal()
			if (res == 'success') {
				nama_kelas_sekarang = nama_baru
				$('#nama_kelas').html(nama_baru)
				return
			}
			kembalikan_button_seperti_awal()
			alert(res)
		}
	})
}

function kembalikan_button_seperti_awal() {
	$('#nama_kelas').html(nama_kelas_sekarang)
	$('#nama_kelas').prop('contenteditable', false)
	$('#button-edit-nama-kelas').attr(`disabled`, false)
	$('#button-edit-nama-kelas').addClass('btn-outline-warning')
	$('#button-edit-nama-kelas').removeClass('btn-primary')
	$('#button-edit-nama-kelas').html('<i class="fa fa-cog"></i>')
	$('#button-edit-nama-kelas').attr('onclick', 'edit_kelas()')
}

function hapus_kelas() {
	var hapus = confirm('Apakah anda yakin ingin menghapus kelas '+nama_kelas_sekarang)
	if (hapus) {
		window.location.href = "/hapus-kelas/"+id_kelas_;
	}
}

$(document).ready(() => {
	proses_siswa.show_siswa()

	$('#btn-siswa').click(proses_siswa.show_siswa)
	$('#addsiswa').click(() => {modal_tambah('siswa')})
	$('#addtugas').click(() => {modal_tambah('tugas')})
	$('#addlecana').click(() => {modal_tambah('lecana')})
})