
var kelompok_sudah_jadi = []
const get_kelompok = () =>{
	$.getJSON('/kelompok/'+id_tugas_, function(data) {
		$('#spinner-tunggu-data').remove()
	  if (data.count == 0) {
	  	$('#btn-buat-kelompok').show()
	  	return
	  }

	  kelompok_sudah_jadi = data.kelompok
	  $('#buat-kelompok').remove()

	  data.kelompok.forEach((i, idx) => {

	  	// buat kotak kelompok
	  	$('#wadah-kelompok-tampil').append(`
	  		<div class="list-group d-inline-block m-2" id="list-siswa-kelompok-tampil${idx}">
	  		  <div class="list-group-item list-group-item-action bg-success">
	  		    <div class="d-flex w-100 justify-content-between nama-kelompok" id="nama-kelompok-${idx}">
	  		      <h5 class="mb-1"><b contentediable="false" id="text-dari-kelompok-${idx}">${i.nama}</b></h5>
	  		      <button class="btn btn-sm btn-outline-warning ml-3" onclick="ganti_nama_kelompok(${idx})">
	  		      	<i class="fa fa-edit"></i>
	  		      </button>
	  		    </div>
	  		  </div>
	  		</div>`)

	  	// tambah siswa
	  	i.siswa.forEach((j, jdx) => {
	  		var siswa = j[0]
	  		var lecana = ''

	  		j.lecana.forEach((k, kdx) => {
	  			lecana+=k.ikon+" "
	  		})

	  		$('#list-siswa-kelompok-tampil'+idx).append(`
	  		<div class="list-group-item list-group-item-action siswa-preview" id="siswa-tampil-${idx}-${jdx}">
	  		  <div class="d-flex w-100 justify-content-between">
	  		    <h5 class="mb-1"><b>${siswa.nama}</b></h5>
	  		    <div id="lecana-${idx}">${lecana}</div>
	  		  </div>
	  		</div>`)
	  	})

	  })

	  $('#buat-kelompok').remove()
	  $('#btn-buat-kelompok').remove()
	})
	.fail(function(jqxhr, textStatus, error) {
	  alert("Error: " + textStatus + ", " + error);
	});
}

function ganti_nama_kelompok(id) {
	$(`.nama-kelompok button`).removeClass('btn-secondary')
	$(`.nama-kelompok button`).removeClass('btn-primary')
	$(`.nama-kelompok button`).addClass('btn-outline-warning')
	$(`.nama-kelompok button`).html(`<i class="fa fa-edit"></i>`)
	$(`.nama-kelompok button`).attr(`disabled`, false)
	$('.nama-kelompok b').prop('contenteditable', false)

	$('#nama-kelompok-'+id+' b').prop('contenteditable', true)
	$('#nama-kelompok-'+id+' b').focus()
	$(`#nama-kelompok-${id} button`).removeClass('btn-outline-warning')
	$(`#nama-kelompok-${id} button`).addClass('btn-primary')
	$(`#nama-kelompok-${id} button`).attr('onclick', 'simpan_ganti_nama_kelompok('+id+')')
	$(`#nama-kelompok-${id} button`).html(`<i class="fa fa-save"></i>`)
}

function simpan_ganti_nama_kelompok(id) {
	$('.nama-kelompok b').prop('contenteditable', false)
	$(`#nama-kelompok-${id} button`).removeClass('btn-primary')
	$(`#nama-kelompok-${id} button`).addClass('btn-secondary')
	$(`#nama-kelompok-${id} button`).html(`<i class="fa fa-spinner"></i>`)
	$(`#nama-kelompok-${id} button`).attr(`disabled`, true)

	var nama_baru = $('#nama-kelompok-'+id+' b').text()
	var id_kelompok = kelompok_sudah_jadi[id].id_kelompok
	$.ajax({
    url: '/ganti-nama-kelompok/',
    method: 'POST',
    data: {
    	nama: nama_baru,
    	id_kelompok: id_kelompok
    },
    success: function(response) {
    	if (response != 'success') {
    		reset_nama_kelompok(id)
    		alert(response)
    		return
    	}

    	kelompok_sudah_jadi[id].nama = nama_baru
    	reset_nama_kelompok(id)
    },
    error: function(xhr, status, error) {
    	reset_nama_kelompok(id)
      alert('Request failed:'+ error);
    }
  });
}

function reset_nama_kelompok(id) {
	$('#nama-kelompok-'+id+' b').text(kelompok_sudah_jadi[id].nama)
	$(`#nama-kelompok-${id} button`).removeClass('btn-secondary')
	$(`#nama-kelompok-${id} button`).addClass('btn-outline-warning')
	$(`#nama-kelompok-${id} button`).html(`<i class="fa fa-edit"></i>`)
	$(`#nama-kelompok-${id} button`).attr(`disabled`, false)
	$(`#nama-kelompok-${id} button`).attr('onclick', 'ganti_nama_kelompok('+id+')')
}

function hapus_tugas() {
	const confirmation = window.confirm('Yakin ingin menhapusnya?');
	  if (confirmation) {
	    window.location.href = '/hapus-tugas/'+id_tugas_
	  }
}

function update_tugas() {
	var nama = $('#nama_tugas').val()
	var deskripsi = $('#deskripsi_tugas').val()
	$.ajax({
	  url: '/update-tugas/'+id_tugas_,
	  type: 'POST',
	  data: {nama,deskripsi},
	  success: function(response) {
	    if (response == 'success') {
	    	$('#exampleModalCenter').modal('hide');
	    	$('#nama_tugas-default').text(nama)
	    	$('#deskripsi_tugas-default').text(deskripsi)
	    	return
	    }

	    alert(response)
	  },
	  error: function(xhr, status, error) {
	    alert(error);
	  }
	});
}

$(document).ready(() => {
	get_kelompok()
	get_siswa()
}) 