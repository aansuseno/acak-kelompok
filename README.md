# Aplikasi Pemilihan Kelompok Tugas Berbasis CodeIgniter 4


## Deskripsi Singkat

Aplikasi ini adalah sebuah platform yang berguna untuk membagi anggota kelas ke dalam kelompok-kelompok tugas secara acak atau berdasarkan kriteria tertentu. Dengan menggunakan teknologi CodeIgniter 4, aplikasi ini menawarkan kemudahan dalam manajemen anggota kelas dan pembagian tugas untuk meningkatkan kolaborasi dan produktivitas dalam lingkungan pembelajaran. Aplikasi ini mudah sederhana dan mudah digunakan.

## Alasan Membuat
Pemilihan Kelompok yang Adil: Dalam situasi di mana pembagian kelompok menjadi perhatian, aplikasi ini dapat membantu mengatasi masalah preferensi subjektif atau ketidakseimbangan keanggotaan kelompok. Anda dapat menggunakan opsi pembagian kelompok secara acak atau berdasarkan kriteria tertentu untuk memastikan bahwa pembagian kelompok adil dan objektif.

## Demo
Silakan cek di [sini](acakkelompok.jawaireng.com).