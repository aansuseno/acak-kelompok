<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Items extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama' => 'pintar',
                'ikon' => '&#x1F9E0;',
                'deskripsi' => 'Lecana untuk siswa yang pintar atau rajin.'
            ],[
                'nama' => 'laptop',
                'ikon' => '&#x1f4bb;',
                'deskripsi' => 'Lecana untuk yang punya laptop.'
            ],[
                'nama' => 'happy',
                'ikon' => '&#x1f601;',
                'deskripsi' => 'Lecana untuk anggota yang selalu happy.'
            ],[
                'nama' => 'presenter',
                'ikon' => '&#x1F3A4;',
                'deskripsi' => 'Lecana untuk yang pintar presentasi.'
            ],[
                'nama' => 'laki-laki',
                'ikon' => '&#x2642;',
                'deskripsi' => 'Lecana untuk laki-laki.'
            ],[
                'nama' => 'perempuan',
                'ikon' => '&#x2640;',
                'deskripsi' => 'Lecana untuk perempuan.'
            ],
        ];

        // Using Query Builder
        $this->db->table('items')->insertBatch($data);
    }
}
