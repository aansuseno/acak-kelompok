<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Items extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'ikon'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100'
			],
			'is_default'       => [
				'type'           => 'INT',
				'constraint'     => '5',
				'default'     => 1,
			],
			'deskripsi'       => [
				'type'           => 'TEXT',
				'null'     => true,
			],
			'id_users'       => [
				'type'           => 'INT',
				'constraint'     => '5'
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('items');
    }

    public function down()
    {
        $this->forge->dropTable('items');
    }
}
