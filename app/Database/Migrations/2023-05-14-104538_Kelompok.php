<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Kelompok extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100'
			],
			'deskripsi'       => [
				'type'           => 'TEXT',
                'null' => true
			],
			'id_tugas'       => [
				'type'           => 'INT',
				'constraint'     => '5'
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('kelompok');
    }

    public function down()
    {
        $this->forge->dropTable('kelompok');
    }
}
