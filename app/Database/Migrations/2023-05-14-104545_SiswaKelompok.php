<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SiswaKelompok extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'id_siswa'       => [
				'type'           => 'INT',
				'constraint'     => '5'
			],
			'id_kelompok'       => [
				'type'           => 'INT',
				'constraint'     => '5'
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('siswa_kelompok');
    }

    public function down()
    {
        $this->forge->dropTable('siswa_kelompok');
    }
}
