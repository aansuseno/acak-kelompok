<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsersKelas extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'id_users'       => [
				'type'           => 'INT',
				'constraint'     => '5',
			],
			'id_kelas'       => [
				'type'           => 'INT',
				'constraint'     => '5',
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('users_kelas');
    }

    public function down()
    {
        $this->forge->createTable('users_kelas');
    }
}
