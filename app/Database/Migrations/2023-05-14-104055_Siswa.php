<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Siswa extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'jk'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
                'default'        => 'none'
			],
			'id_pembuat'       => [
				'type'           => 'INT',
				'constraint'     => '5',
			],
			'id_kelas'       => [
				'type'           => 'INT',
				'constraint'     => '5',
			],
			'is_aktif'       => [
				'type'           => 'INT',
				'constraint'     => '5',
                'default'        => 1
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('siswa');
    }

    public function down()
    {
        $this->forge->dropTable('siswa');
    }
}
