<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Kelompok::index');
$routes->get('/acak', 'Kelompok::acak');
$routes->get('/register', 'Register::index');
$routes->get('/login', 'Login::index');
$routes->get('/logout', 'Login::logout');
$routes->post('/register', 'Register::proses');
$routes->post('/login', 'Login::proses');

$routes->get('/home', 'Kelas::index', ['filter' => 'usersAuth']);

$routes->get('/kelas', 'Kelas::get_data', ['filter' => 'usersAuth']);
$routes->get('/detail-kelas/(:num)', 'Kelas::detail/$1', ['filter' => 'usersAuth']);
$routes->get('/hapus-kelas/(:num)', 'Kelas::hapus/$1', ['filter' => 'usersAuth']);
$routes->post('/kelas', 'Kelas::tambah_kelas', ['filter' => 'usersAuth']);
$routes->post('/edit-nama-kelas/(:num)', 'Kelas::update_kelas/$1', ['filter' => 'usersAuth']);

$routes->get('/siswa/(:num)', 'Siswa::get_data/$1', ['filter' => 'usersAuth']);
$routes->post('/siswa/(:num)', 'Siswa::tambah/$1', ['filter' => 'usersAuth']);
$routes->post('/siswa-tambah-lecana', 'Siswa::tambah_lecana', ['filter' => 'usersAuth']);
$routes->post('/siswa-hapus-lecana', 'Siswa::hapus_lecana', ['filter' => 'usersAuth']);
$routes->get('/siswa-hapus/(:num)/(:num)', 'Siswa::hapus/$1/$2', ['filter' => 'usersAuth']);
$routes->post('/siswa-edit', 'Siswa::edit', ['filter' => 'usersAuth']);

$routes->get('/items-belum-dipakai/(:num)', 'Siswa::not_used_item/$1', ['filter' => 'usersAuth']);
$routes->get('/items', 'Items::index', ['filter' => 'usersAuth']);


$routes->post('/tugas/(:num)', 'Tugas::tambah/$1', ['filter' => 'usersAuth']);
$routes->get('/tugasku/(:num)', 'Tugas::publik/$1', ['filter' => 'usersAuth']);
$routes->get('/tugas/(:num)', 'Tugas::index/$1', ['filter' => 'usersAuth']);
$routes->get('/detail-tugas/(:num)', 'Tugas::detail/$1', ['filter' => 'usersAuth']);
$routes->get('/hapus-tugas/(:num)', 'Tugas::hapus/$1', ['filter' => 'usersAuth']);
$routes->post('/update-tugas/(:num)', 'Tugas::update/$1', ['filter' => 'usersAuth']);

$routes->post('/kelompok-simpan', 'Tugas::simpan_kelompok', ['filter' => 'usersAuth']);
$routes->get('/kelompok/(:num)', 'Tugas::get_kelompok/$1', ['filter' => 'usersAuth']);
$routes->post('/ganti-nama-kelompok', 'Tugas::ganti_nama_kelompok', ['filter' => 'usersAuth']);

$routes->get('/publik/tugas/(:alphanum)', 'Tugas::tampil/$1');

$routes->post('/buat-lecana', 'Items::tambah', ['filter' => 'usersAuth']);
$routes->get('/hapus-lecana/(:num)', 'Items::hapus/$1', ['filter' => 'usersAuth']);

$routes->get('/nama-template/(:segment)', 'NamaTemplate::index/$1', ['filter' => 'usersAuth']);
$routes->get('/input-nama', 'NamaTemplate::tampil');
$routes->post('/input-nama', 'NamaTemplate::tampil');

$routes->get('/edit-user', 'Users::index', [ 'filter' => 'usersAuth']);
$routes->post('/edit-password', 'Users::edit_password', [ 'filter' => 'usersAuth']);

$routes->get('/logout', function ()
{
	session_destroy();
	return redirect()->to('/');
});

$routes->get('/tamu', 'Tamu::index');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
