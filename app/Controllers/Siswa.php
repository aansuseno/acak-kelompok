<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Siswa extends BaseController
{
    public function get_data($id_kelas)
    {
        $msiswa = model('SiswaModel');

        $count = $msiswa->get_data(session()->id, $id_kelas)->countAllResults();
        $data = [];

        if ($count > 0) {
            $data = $msiswa->get_data(session()->id, $id_kelas)->find();
            $mitems = model('ItemsModel');
            foreach ($data as $key => $value) {
                $data[$key]['lecana'] = $mitems->get_by_siswa($value['id'])->find();
            }
        }

        echo json_encode([
            'count' => $count,
            'data' => $data,
        ]);
    }

    public function tambah($id_kelas)
    {
        if(!$this->cek_kelas_valid($id_kelas)) {
            echo "oops";
            return;
        }

        $nama = $_POST['nama'];
        $nama = str_replace("\n", "", $nama);
        if (!preg_match('/^[a-zA-Z0-9_\-\|\,\s\.]{1,10000}$/i', $nama)) {
            echo "notoke";
            return;
        }

        $msiswa =model('SiswaModel');
        $kumpulan_nama = explode(",", $nama);
        $arr_nama = [];
        foreach ($kumpulan_nama as $k) {
            $k = trim($k);
            if ($k == '') continue;
            $arr_nama[] = [
                'nama' => $k,
                'id_pembuat' => session()->id,
                'id_kelas' => $id_kelas,
            ];
        }

        $msiswa->insertBatch($arr_nama);

        echo "oke";
    }

    public function hapus($id_kelas, $id_siswa)
    {
        if(!$this->cek_kelas_valid($id_kelas)) {
            echo "oops";
            return;
        }

        $msiswa = model('SiswaModel');
        if ($msiswa->where('id', $id_siswa)->where('id_kelas', $id_kelas)->set('is_aktif', 0)->update()) {
            echo 'success';
            return;
        }
        echo 'err';
    }

    public function edit()
    {
        // validasi user input
        if (!preg_match('/^(?!\s*$)[a-zA-Z0-9_.-]+$/', $_POST['nama']) || !preg_match('/\d+/', $_POST['id'])) {
            echo "err";
            return;
        }

        $msiswa = model('SiswaModel');
        // jaga user usil
        if(!$msiswa->is_valid(session()->id, $_POST['id'])) {
            echo "err";
            return;
        }

        // update nama siswa
        $msiswa->where('id', $_POST['id'])->set('nama', $_POST['nama']);
        if ($msiswa->update()) {
            echo "success";
            return;
        }

        echo "err";
        return;
    }

    public function not_used_item($id_siswa)
    {
        $msiswa = model('SiswaModel');
        // jaga user usil
        if(!$msiswa->is_valid(session()->id, $id_siswa)) {
            echo json_encode(['lecana' => []]);
            return;
        }

        $mitems = model('ItemsModel');
        $user_item = $mitems->all_user_have($id_siswa)->find();

        echo json_encode(['lecana' => $user_item]);
    }

    public function tambah_lecana()
    {
        $id_siswa = $_POST['id_siswa'];
        $id_items = $_POST['id_items'];

        // validasi user input
        if (!preg_match('/\d+/', $id_siswa) || !preg_match('/\d+/', $id_items)) {
            echo json_encode(['msg' => 'err']);
            return;
        }

        $msiswa = model('SiswaModel');
        // jaga user usil
        if(!$msiswa->is_valid(session()->id, $id_siswa)) {
            echo json_encode(['msg' => 'err']);
            return;
        }

        $msiswa_items = model('SiswaItemsModel');
        // cek untuk antisipasi data dobel
        if ($msiswa_items->sudah_ada_kah($id_siswa, $id_items)) {
            echo json_encode(['msg' => 'sudah ada']);
            return ;
        }

        $item = model('ItemsModel')->select('nama, ikon')->where('id', $id_items)->first();

        // tambah data
        if ($msiswa_items->tambah(['id_siswa' => $id_siswa, 'id_items' => $id_items])) {
            echo json_encode([
                'msg' => "success",
                'id' => $msiswa_items->insertId(),
                'nama' => $item['nama'],
                'ikon' => $item['ikon']
            ]);
            return;
        } 
        
        echo json_encode(['msg' => 'err']);
        return;
    }

    public function hapus_lecana()
    {
        $siswa_item = model('SiswaItemsModel');
        // apakah data valid
        if ($siswa_item->get_by_id($_POST['id'])->countAllResults() == 0) {
            echo json_encode(['msg' => 'data tidak ditemukan']);
            return;
        }

        $msiswa = model('SiswaModel');
        $si = $siswa_item->get_by_id($_POST['id'])->first();
        if (!$msiswa->is_valid(session()->id, $si['id_siswa'])) {
            echo json_encode(['msg' => 'jgn usil']);
            return;
        }

        // ambil lecana yang dihapus
        $mitems = model('ItemsModel')->where('id', $si['id_items'])->first();
        
        echo json_encode([
            'msg' => 'success',
            'id' => $mitems['id'],
            'ikon' => $mitems['ikon'],
            'nama' => $mitems['nama'],
        ]);

        // hapus di db
        $siswa_item->where('id', $_POST['id'])->delete();
        return;
    }

    // jika kelas tidak sesuai dengan id user maka kode akan berhenti
    public function cek_kelas_valid($id_kelas)
    {
        $mkelas = model('KelasModel');
        $kelas = $mkelas
            ->join('users_kelas uk', 'uk.id_kelas = kelas.id')
            ->where('uk.id_users', session()->id)
            ->where('kelas.id', $id_kelas)
            ->countAllResults();

        return ($kelas > 0) ? true : false;
    }
}
