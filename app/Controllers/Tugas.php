<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Tugas extends BaseController
{
    public function tambah($id_kelas)
    {
        $mkelas = model('KelasModel');
        if (!$mkelas->is_valid($id_kelas)) {
            echo json_encode([
                'msg' => 'jgn usil'
            ]);
            return ;
        }

        $mtugas = model('TugasModel');
        $nama = $_POST['nama'];
        $deskripsi = $_POST['deskripsi'];

        if (preg_match("/[;'\"<#]/i", $nama) || preg_match("/[;'\"<#]/i", $deskripsi)) {
            echo json_encode(['msg' => 'inputan tidak valid']);
            return 0;
        }

        $mtugas->tambah([
            'nama' => $nama,
            'deskripsi' => $deskripsi,
            'id_users' => session()->id,
            'id_kelas' => $id_kelas
        ]);

        echo json_encode([
            'msg' => 'success'
        ]);
        return;
    }

    public function index($id_kelas)
    {
        $mkelas = model('KelasModel');
        if (!$mkelas->is_valid($id_kelas)) {
            echo json_encode(['msg' => 'kelas tidak ditemukan']);
            return;
        }

        $mtugas = model('TugasModel');
        echo json_encode($mtugas->get_data($id_kelas)->find());
    }

    public function detail($id_tugas)
    {
        $mtugas = model('TugasModel');

        // jika data tidak ada
        if ($mtugas->detail($id_tugas)->countAllResults() == 0) {
            return redirect()->to('/home');
        }

        $tugas = $mtugas->detail($id_tugas)->first();

        return view('detail-tugas', ['title' => 'Tugas '.$tugas['nama'], 'tugas' => $tugas]);
    }

    public function simpan_kelompok()
    {
        $mtugas = model('TugasModel');

        // cek tugas valid
        if (!$mtugas->is_valid($_POST['id_tugas'])) {
            echo json_encode(['msg' => 'tugas tidak valid']);
            return;
        }

        // cek apakah sudah pernah input
        if (model('KelompokModel')->where('id_tugas', $_POST['id_tugas'])->countAllResults() > 0) {
            echo json_encode(['msg' => 'sudah pernah input, data ditolak']);
            return;
        }

        $kelompok = json_decode($_POST['kelompok']);
        $msiswa = model('SiswaModel');

        //cek apa ada siswa yang tidak valid
        $siswa_valid = true;
        foreach ($kelompok as $key => $v) {
            foreach ($v as $s) {
                if (!$msiswa->is_valid($s)) {
                    $siswa_valid = false;
                    break;
                }
            }
            if (!$siswa_valid) {
                break;
            }
        } 
        if (!$siswa_valid) {
            echo json_encode(['msg' => 'siswa tidak valid']);
        }

        // simpan ke db
        model('SiswaKelompokModel')->simpan($kelompok, $_POST['id_tugas']);
        echo json_encode(['msg' => 'success']);
    }

    public function get_kelompok($id_tugas)
    {
        $mtugas = model('TugasModel');
        if (!$mtugas->is_valid($id_tugas)) {
            echo json_encode(['count' => 0]);
        }

        $msiswa =model('SiswaModel');
        $mitems =model('ItemsModel');
        $mkelompok = model('KelompokModel');
        $kelompok = $mkelompok
            ->select('kelompok.nama, kelompok.id as id_kelompok, GROUP_CONCAT(sk.id_siswa) as kumpulan_siswa')
            ->join('siswa_kelompok sk', 'sk.id_kelompok = kelompok.id')
            ->where('id_tugas', $id_tugas)
            ->groupBy('id_kelompok')
            ->find();

        foreach ($kelompok as $key => $value) {
            $siswa = explode(',', $value['kumpulan_siswa']);
            $siswa_arr = [];
            foreach ($siswa as $key_siswa => $v_siswa) {
                $siswa_arr[] = $msiswa
                    ->select('nama, id, is_aktif')
                    ->where('id', $v_siswa)
                    ->find();
                $siswa_arr[$key_siswa]['lecana'] = $mitems
                    ->get_by_siswa($v_siswa)
                    ->find();
            }
            $kelompok[$key]['siswa'] = $siswa_arr;
        }

        return json_encode([
            'kelompok' => $kelompok,
            'count' => count($kelompok)
        ]);
    }

    public function ganti_nama_kelompok()
    {
        // cek keamanan input
        $nama = str_replace("\n", "", $_POST['nama']);
        if(!preg_match('/^\d+$/', $_POST['id_kelompok']) || !preg_match('/^[\dA-Za-z\s,!-]{1,25}$/', $nama)) {
            echo "input tidak valid";
            return;
        }

        $kel = model('KelompokModel')->select('id_tugas')->where('id', $_POST['id_kelompok'])->find();
        if (count($kel) == 0) {
            echo "input tidak valid";
            return;
        }
        $mtugas = model('TugasModel');
        $tugas = $mtugas->where('id', $kel[0]['id_tugas'])->first()['id'];
        if (!$mtugas->is_valid($tugas)) {
            echo "oopsss"; return;
        }

        // proses input
        $mkelompok = model('KelompokModel');
        $mkelompok
            ->where('id', $_POST['id_kelompok'])
            ->set('nama', $nama)
            ->update();

        echo "success";
    }

    public function hapus($id_tugas)
    {
        $mtugas = model('TugasModel');
        if (!$mtugas->is_valid($id_tugas)) {
            echo "oopsss"; return;
        }
        $id_kelas = $mtugas->where(['id' => $id_tugas])->first()['id_kelas'];

        // ambil semua kelompok\
        $mkelompok = model('KelompokModel');
        $kelompok = $mkelompok->where('id_tugas', $id_tugas)->find();
        $msk = model('SiswaKelompokModel');
        foreach ($kelompok as $key => $value) {
            $msk->delete(['id_kelompok' => $value['id']]);
        }
        $mkelompok->delete(['id_tugas' => $id_tugas]);

        $mtugas->delete(['id' => $id_tugas]);
        return redirect()->to('/detail-kelas/'.$id_kelas.'?page=tugas');
    }

    public function update($id_tugas)
    {
        $mtugas = model('TugasModel');
        if (!$mtugas->is_valid($id_tugas)) {
            echo "oopsss"; return;
        }

        $nama = $_POST['nama'];
        $deskripsi = $_POST['deskripsi'];
        if (preg_match("/[;'\"<#]/i", $nama) || preg_match("/[;'\"<#]/i", $deskripsi) || !preg_match("/^.{2,144}$/", $nama) || !preg_match("/^.{0,1000}$/", $deskripsi)) {
            return "input tidak valid(beberapa simbol dilarang)";
        }

        $mtugas->set('nama', $nama)->set('deskripsi', $deskripsi)->where('id', $id_tugas)->update();
        return "success";
    }

    public function publik($id_tugas)
    {
        echo "Loading...";
        $mtugas = model('TugasModel');
        if (!$mtugas->is_valid($id_tugas)) {
            return "tugas tidak ditemukan";
        }

        $muuid_tugas = model('UuidTugasModel');
        $uuid = $muuid_tugas->ambil_uuid($id_tugas);
        return redirect()->to('/publik/tugas/'.$uuid);
    }

    public function tampil($uuid)
    {
        $muuid_tugas = model('UuidTugasModel');
        if ($muuid_tugas->get_by_uuid($uuid)->countAllResults() == 0) {
            return $this->response->setStatusCode(404)->setBody('Page Not Found');
        }

        $id_tugas = $muuid_tugas->get_by_uuid($uuid)->first()['id_tugas'];

        $tugas = model('TugasModel')->detail($id_tugas)->first();
        $kelas = model('KelasModel')->detail(session()->id, $tugas['id_kelas'])->first();

        $data = [
            'title' => "Tugas <b>{$tugas['nama']}</b> dari kelas <b>{$kelas['nama']}</b>",
            'tugas' => $tugas,
            'kelompok' => get_object_vars(json_decode($this->get_kelompok($id_tugas)))
        ];

        // echo "<pre>";
        // print_r($data);

        return view('tampil-tugas', $data);
    }
}
