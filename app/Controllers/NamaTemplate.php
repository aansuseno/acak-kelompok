<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class NamaTemplate extends BaseController
{
    public function index($cari = '')
    {
    	$nama = model('NamaTemplate')->ambil_mirip($cari)->find();
        $nama_pendek = model('NamaTemplate')->select('nama')
            ->like('nama', $cari, 'after')
            ->orderBy('CHAR_LENGTH(nama)', 'asc')
            ->limit(5)->find();
    	return json_encode(array_merge($nama_pendek, $nama));
    }

    public function tampil($value='')
    {
    	if (isset($_POST['kode'])) {
    		if ($_POST['kode'] == 'kode') { // ganti ketika production

    			if (isset($_POST['nama_konfirmasi'])) {
    				$nama = $_POST['nama_konfirmasi'];
    				$data_input = array_map(function ($i) {
    					return ['nama' => $i, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
    				}, $nama);
    				model('NamaTemplate')->insertBatch($data_input);
    				return redirect()->to(base_url('/input-nama?berhasil'));
    			}

    			$nama = array_unique(explode(",", strtolower($_POST['nama'])));
    			$nama_baru = [];
    			foreach ($nama as $key => $value) {
    				if (strlen($value) < 3) continue;
    				if (model('NamaTemplate')->where('nama', $value)->countAllResults() == 0) {
    					$nama_baru[] = $value;
    				}
    			}
    			return view('konfirmasi-nama-template', ['nama' => $nama_baru, 'kode' => $_POST['kode']]);
    		}
    	}
    	
    	return view('input-nama-template');
    }
}
