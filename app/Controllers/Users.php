<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Users extends BaseController
{
    public function index()
    {
    	$user = model('UserModel')
    		->select('username')
    		->where('id', session()->id)
    		->find();

    	if (count($user) == 0) {
    		return "404";
    	}

      return view('edit-user', [
      	'title' => 'Edit User',
      	'username' => $user[0]['username']
      ]);
    }

    public function edit_password()
    {
    	$password = $this->request->getVar('password');
    	$new_password = $this->request->getVar('new-password');
    	$konf_password = $this->request->getVar('konf-password');

    	if (
    		!preg_match('/^.{4,}$/', $password) ||
    		!preg_match('/^.{4,}$/', $new_password) ||
    		!preg_match('/^.{4,}$/', $konf_password)) {
    		session()->setFlashdata('pesan', 'Password minimal 4 karakter');
    		session()->setFlashdata('error', 'true');
    		return redirect()->to('/edit-user');
    	}

    	if ($konf_password != $new_password) {
    		session()->setFlashdata('pesan', 'Password konfirmasi salah');
    		session()->setFlashdata('error', 'true');
    		return redirect()->to('/edit-user');
    	}

    	$muser = model('UserModel');
    	$user = $muser
    		->where('id', session()->id)
    		->first();

    	if (!password_verify($password, $user['password'])) {
    		session()->setFlashdata('pesan', 'Password Lama salah');
    		session()->setFlashdata('error', 'true');
    		return redirect()->to('/edit-user');
    	}

    	$muser->set('password', password_hash($new_password, PASSWORD_BCRYPT))->where('id', session()->id)->update();

    	session()->setFlashdata('success', 'success');
    	return redirect()->to('/edit-user');
    }
}
