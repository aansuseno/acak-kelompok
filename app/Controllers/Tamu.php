<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Tamu extends BaseController
{
	function generateRandomString($length) {
	    $bytes = random_bytes($length);
	    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $string = '';
	    
	    for ($i = 0; $i < $length; $i++) {
	        $index = ord($bytes[$i]) % strlen($characters);
	        $string .= $characters[$index];
	    }
	    
	    return $string;
	}

  public function index()
  {
  	if (session()->logged_in) {
  		return redirect()->to('/home');
  	}

  	$username = time()."-".$this->generateRandomString(5);

  	$users = model('UserModel');
  	$users->insert([
  	    'username' => $username,
  	    'password' => password_hash($this->generateRandomString(10), PASSWORD_BCRYPT),
  	    'is_tamu' => 1
  	]);

  	session()->set([
  	    'username' => $username,
  	    'id' => $users->insertId(),
  	    'logged_in' => TRUE,
  	    'is_tamu' => TRUE
  	]);
  	return redirect()->to(base_url('home'));
  }
}
