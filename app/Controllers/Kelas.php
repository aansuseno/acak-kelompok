<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Kelas extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home'
        ];
        return view('kelas', $data);
    }

    public function get_data()
    {
        $mkelas = model('KelasModel');
        return json_encode([
            'count' => $mkelas->get_by_user(session()->id)->countAllResults(),
            'data' => $mkelas
                ->get_by_user(session()->id)
                ->orderBy('kelas.updated_at', 'desc')
                ->find()
        ]);
    }

    public function tambah_kelas()
    {
        $muserskelas = model('UsersKelasModel');

        // maksismal 12 per user
        if(model('KelasModel')->get_by_user(session()->id)->countAllResults() > 50){
            echo "maksimum jumlah kelas 50";
            return;
        }

        if (session()->is_tamu
         && model('KelasModel')->get_by_user(session()->id)->countAllResults() > 2) {
            return "tamu maksimal membuat 3 kelas";
        }

        $nama = $_POST['nama'];
        if (!preg_match('/^[a-zA-Z0-9_\-\|\,\s]{1,144}$/i', $nama)) {
            echo "notoke";
            return;
        }

        $mkelas = model('KelasModel');
        $mkelas->insert(['nama' => $nama]);
        $muserskelas->insert([
            'id_users' => session()->id,
            'id_kelas' => $mkelas->insertID(),
        ]);

        echo "oke";
    }

    public function detail($id_kelas)
    {
        $mkelas = model('KelasModel');

        // jika data tidak ada
        if ($mkelas->detail(session()->id, $id_kelas)->countAllResults() == 0) {
            return redirect()->to('/home');
        }

        $kelas = $mkelas->detail(session()->id, $id_kelas)->first();

        return view('detail-kelas', ['title' => 'Kelas '.$kelas['nama'], 'kelas' => $kelas]);
    }

    public function update_kelas($id_kelas)
    {
        $mkelas = model('KelasModel');
        if (!$mkelas->is_valid($id_kelas)) {
            return "kelas tidak ditemukan";
        }
        
        $nama = $_POST['nama'];
        if (!preg_match('/^[a-zA-Z0-9_\-\|\,\s]{1,144}$/i', $nama)) {
            return "nama kelas tidak valid";
        }

        $mkelas
            ->set('nama', $nama)
            ->where('id', $id_kelas)
            ->update();

        return 'success';
    }

    public function hapus($id_kelas)
    {
        $mkelas = model('KelasModel');
        if (!$mkelas->is_valid($id_kelas)) {
            return "kelas tidak ditemukan";
        }

        $mkelas->where('id', $id_kelas)->delete();
        return redirect()->to('/home');
    }
}
