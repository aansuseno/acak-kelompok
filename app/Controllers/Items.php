<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Items extends BaseController
{
    public function index()
    {
        echo json_encode(model('ItemsModel')->all_user_have()->find());
    }

    public function tambah()
    {
    	$nama = strtolower(str_replace(' ', '-', $_POST['nama']));
    	$deskripsi = $_POST['deskripsi'];
    	$ikon = $_POST['ikon'];

    	// cek nama
    	if (!preg_match('/^[a-z0-9\-]{1,100}$/', $nama)) {
    	    return "nama hanya boleh huruf kecil, angka dan tanda minus (-), maksimal 100 karakter";
    	}

    	$mitems = model('ItemsModel');
    	$wajib_items = [
    		[
    			'nama' => 'tanpa-lecana',
    			'ikon' => '&#8709;'
    		],
    		[
    			'nama' => 'semua',
    			'ikon' => '&#127757;'
    		]
    	];
    	$items_sudah_ada = array_merge($mitems->all_user_have()->find(), $wajib_items);

        // tentukan maksimal
        if (count($items_sudah_ada) > 50) {
            return "maximal peruser membuat 50 kostum lecana";
        }

    	// cek apakah items sudah ada
    	foreach ($items_sudah_ada as $key => $value) {
    		if ($nama == $value['nama']) {
    			return "nama sudah ada yang sama";
    		}
    	}

    	// cek emoji valid
    	$emojis = json_decode(file_get_contents('emoji.json'), true);
    	$ikon_html = '';
    	foreach ($emojis as $key => $value) {
    		if ($ikon == $value['emoji']) {
    			$ikon_html = $value['html'];
    			break;
    		}
    	}
    	if ($ikon_html == '') {
    		return "emoji tidak valid";
    	}

    	// input
    	$mitems->insert([
    		'nama' => $nama,
    		'ikon' => $ikon_html,
    		'deskripsi' => $deskripsi,
    		'is_default' => 0,
    		'id_users' => session()->id
    	]);
    	return 'success';
    }

    public function hapus($id)
    {
        $mitems = model('ItemsModel');
        if ($mitems
            ->where('id', $id)
            ->where('id_users', session()->id)
            ->where('is_default', 0)
            ->countAllResults() == 1) {
            
            $mitems->where('id', $id)->delete();
            return "success";
        }

        return "items tidak ditemukan";
    }
}
