<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Kelompok extends BaseController
{
    public function index()
    {
        return view('home', ['title' => 'home']);
    }

    public function acak()
    {
    	return view('acak', ['title' => 'acak']);
    }
}
