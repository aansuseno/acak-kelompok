<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<h2 id="nama_tugas-default">Tugas <?= $tugas['nama'] ?></h2>
<hr>
<div class="d-flex justify-content-between">
	<div>
		<a href="/detail-kelas/<?= $tugas['id_kelas'] ?>?page=tugas" class="btn btn-outline-primary"><i class="fa fa-arrow-left"></i></a>
	</div>
	<div>
	  <button onclick="hapus_tugas()" type="button" class="btn btn-danger mr-1" title="hapus">
	  	<i class="fa fa-trash"></i>
	  </button>
	  <button type="button" class="btn btn-warning mr-1" title="edit" data-toggle="modal" data-target="#exampleModalCenter">
	  	<i class="fa fa-edit"></i>
	  </button>
	  <a href="/tugasku/<?= $tugas['id'] ?>" target="_blank" class="btn btn-info mr-1" title="share">
	  	<i class="fa fa-globe"></i>
	  </a>
	  <button type="button" class="btn btn-primary" id="btn-buat-kelompok" onclick="buat.buat()" style="display: none">
	  	<i class="fa fa-plus"></i> Buat Kelompok
	  </button>
	</div>
</div>

<div id="buat-kelompok" style="display:none">
	<h3>Acak Anggota Kelompok</h3>

	<!-- jumlah -->
	Input jumlah kelompok atau jumlah anggota perkelompok:
	<div class="input-group mb-3">
	  <input type="number" class="form-control" id="amount" placeholder="Enter amount" style="width: 100px;">
	  <div class="input-group-append">
	    <select class="custom-select" id="anggota_kelompok">
	      <option value="anggota">Anggota Setiap Kelompok</option>
	      <option selected value="kelompok">Kelompok</option>
	    </select>
	  </div>
	</div>
	<button class="btn btn-primary col-12" onclick="buat.get_jml()">Next <i class="fa fa-arrow-right"></i></button>
</div>

<div id="kelompokkan-lecana" style="display:none">
	<h3>Grup berdasarkan lecana</h3>
	<div class="kumpulan-lecana"></div>
	<div id="tombol-sebar"></div>
	<hr>
	<div class="d-flex justify-content-center dadu-oke"></div>
	<div class="d-flex flex-wrap justify-content-center" id="wadah-kelompok"></div>
	<div class="d-flex justify-content-center dadu-oke"></div>
</div>

<div class="d-flex flex-wrap justify-content-center" id="wadah-kelompok-tampil" style="display: none"></div>

<div class="content mt-5" id="spinner-tunggu-data">
  <div class="d-flex justify-content-center align-items-center">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>
</div>

<!-- modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<small>Nama Tugas</small>
				<input type="text" name="" id="nama_tugas" autocomplete="off" placeholder="A" class="form-control" value="<?= $tugas['nama'] ?>">
				<small>Deskripsi</small>
				<textarea class="form-control" id="deskripsi_tugas"><?= $tugas['deskripsi'] ?></textarea>
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="update_tugas()">Update</button>
      </div>
    </div>
  </div>
</div>
<blockquote class="blockquote mb-3">
  <p class="mb-0" id="deskripsi_tugas-default"><?= $tugas['deskripsi'] ?></p>
</blockquote>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script type="text/javascript">
	const id_tugas_master = <?= $tugas['id'] ?>;
	const id_kelas_master = <?= $tugas['id_kelas'] ?>;
</script>
<script src="/buat-kelompok.js"></script>
<script type="text/javascript">
	const id_tugas_ = <?= $tugas['id'] ?>
</script>
<script src="/assets/detail-tugas.min.js"></script>
<?= $this->endSection() ?>