<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<div class="d-flex justify-content-between">
	<h2>Daftar Kelas</h2>
	<div>
		<a href="/edit-user" class="btn btn-outline-secondary btn-sm rounded-pill ml-2" title="edit-user"><i class="fa fa-user"></i></a>
	</div>
</div>
<hr>
<?php if (session()->is_tamu): ?>
	<div class="alert alert-warning">
		Masuk sebagai tamu akan terkena limit dan tidak bisa login kembali! Tetapi bisa mencoba semua fitur yang ada.
	</div>
<?php endif ?>

<div class="d-flex justify-content-end mb-3">
	<button class="btn btn-success" id="addkelas" data-toggle="modal" data-target="#exampleModalCenter"></button>
</div>

<!-- for alert -->
<div id="err" style="display: none">
		<div class="alert alert-warning alert-dismissible fade show mt-2" role="alert">
	  <span class="text_err"></span>
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
</div>

<div class="row" id="daftar-kelas"></div>

<!-- modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<small>Nama Kelas</small>
				<input type="text" name="" id="nama_kelas" autocomplete="off" placeholder="A" class="form-control">
			</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="simpan_kelas()">Simpan Kelas</button>
      </div>
    </div>
  </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
	const modal_tambah = () => {
		$('#modalTitle').text('Buat Kelas Baru')
		$('#nama_kelas').val('')
	}

	const simpan_kelas = () => {
		$.ajax({
		  url: '/kelas',
		  type: 'POST',
		  data: {nama: $('#nama_kelas').val()},
		  success: function(res) {
				$('#exampleModalCenter').modal('hide');
		    if(res == 'notoke') {
					$('#err').show()
					$('.text_err').text('Nama tidak valid');
				} else if (res == 'kelebihan') {
					$('#err').show()
					$('.text_err').text('Kelas yang Anda buat sudah melebihi batas.');
				} else if(res == 'oke') {
					//
				} else alert(res);

				show_kelas()
		  }
		});
	}

	function show_kelas() {
		$('#daftar-kelas').html('')
		$.getJSON( "/kelas", function( res ) {
			if(res.count == 0) {
				$('#addkelas').html(`<i class="fa fa-plus"></i> Buat Kelas Baru`)
				$('#addkelas').addClass(`btn-lg m-auto mt-5`)
				return
			}

			res.data.forEach(i => {
				$('#daftar-kelas').append(`
				<div class="col-sm-6 col-md-4 col-lg-3 mb-2">
					<a href="/detail-kelas/${i.id}?page=siswa">
					  <div class="card">
					    <div class="card-body">
					      <h5 class="card-title">${i.nama}</h5>
					    </div>
					  </div>
					</a>
				</div>
				`)
			})

			$('#addkelas').html(`<i class="fa fa-plus"></i>`)
			$('#addkelas').addClass(`btn-sm ml-auto`)
		});
	}

	$(document).ready(() => {
		show_kelas()

		$('#addkelas').click(modal_tambah)
	})
</script>
<?= $this->endSection() ?>