<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<div class="d-flex justify-content-between">
	<h3>User: <b><?= $username ?></b></h3>
	<div>
		<a href="/logout" class="btn btn-outline-danger btn-sm rounded-pill ml-2"><i class="fa fa-sign-out-alt"></i></a>
		<a href="/home" class="btn btn-outline-secondary btn-sm rounded-pill ml-2"><i class="fa fa-home"></i></a>
	</div>
</div>
<hr>
<?php if (session()->getFlashdata('error') == 'true'): ?>
	<div class="alert alert-dismissible alert-danger">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
	    <?= session()->getFlashdata('pesan') ?>
	</div>
<?php endif ?>
<?php if (session()->getFlashdata('success') == 'success'): ?>
	<div class="alert alert-dismissible alert-success">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
	    Password berhasil diganti
	</div>
<?php endif ?>
<div class="my-3">
	<form action="/edit-password" method="post">
	  <label for="password">Password Lama:</label>
	  <div class="input-group mb-3">
	      <input type="password" class="form-control" id="pw1" name="password" placeholder="Password Lama" required minlength="4">
	      <div class="input-group-append">
	          <button class="btn btn-outline-secondary" type="button" onclick="show_hide('pw1')" id="btn-pw1">
	              <i class="fas fa-eye"></i>
	          </button>
	      </div>
	  </div>

	  <label for="password">Password Baru:</label>
	  <div class="input-group">
	      <input type="password" class="form-control" id="pw2" name="new-password" placeholder="Password Baru">
	      <div class="input-group-append">
	          <button class="btn btn-outline-secondary" type="button" onclick="show_hide('pw2')" id="btn-pw2" required minlength="4">
	              <i class="fas fa-eye"></i>
	          </button>
	      </div>
	  </div>

	  <label for="password">Konfirmasi Password:</label>
	  <div class="input-group">
	      <input type="password" class="form-control" id="pw3" name="konf-password" placeholder="Konfirmasi" required minlength="4">
	      <div class="input-group-append">
	          <button class="btn btn-outline-secondary" type="button" onclick="show_hide('pw3')" id="btn-pw3">
	              <i class="fas fa-eye"></i>
	          </button>
	      </div>
	  </div>
	  <button class="btn btn-success col-12 mt-3">Update</button>
  </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
    const show_hide = (id) => {
    	$('#'+id).attr('type', ($('#'+id).attr('type') == 'password') ? 'text': 'password')
    	$('#btn-'+id+' i').toggleClass('fa-eye-slash')
    }
</script>
<?= $this->endSection() ?>
