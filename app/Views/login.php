<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <link rel="icon" href="/img/ikon-acak.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi Acak Kelompok adalah sebuah alat yang dirancang untuk membantu mengacak dan membagi sekelompok orang menjadi kelompok-kelompok yang lebih kecil. Dengan aplikasi ini, Anda dapat dengan mudah mengatur dan mengelompokkan anggota tim, peserta pelatihan, atau siapa pun yang perlu dibagi menjadi beberapa kelompok secara acak.">
    <meta property="og:image" content="/img/preview.png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
  <title>Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Acak Kelompok</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Login</p>
			<?php if (!empty(session()->getFlashdata('error'))) : ?>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
              <?php echo session()->getFlashdata('error'); ?>
          </div>
      <?php endif; ?>
      <form action="/login" method="post">
				</= csrf_field() ?>
        <div class="input-group mb-3">
          <input type="text" name="username" autocomplete="off" class="form-control" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
					<div class="col-8"></div>
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
      <p class="mb-0">
        <a href="/register" class="text-center">Daftar</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/adminlte.min.js"></script>
</body>
</html>
