<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="icon" href="/img/ikon-acak.ico" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Aplikasi Acak Kelompok adalah sebuah alat yang dirancang untuk membantu mengacak dan membagi sekelompok orang menjadi kelompok-kelompok yang lebih kecil. Dengan aplikasi ini, Anda dapat dengan mudah mengatur dan mengelompokkan anggota tim, peserta pelatihan, atau siapa pun yang perlu dibagi menjadi beberapa kelompok secara acak.">
  <meta property="og:image" content="/img/preview.png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="630">
  <title><?= (isset($title)) ? $title : '' ?> | Acak Kelompok</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
</head>
<body class="pt-3 wrapper">
	<div class="container">
		<?= $this->renderSection('body') ?>
	</div>
	

<!-- jQuery -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/adminlte.min.js"></script>
<?= $this->renderSection('js')?>
</body>
</html>
