<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<h2 class="pd-2">Acak nama</h2>
<hr>
<div id="masukkan-nama">
	<h3>Masukkan semua nama</h3>

	<!-- jumlah -->
	pisahkan dengan tanda koma:
	<textarea class="form-control" rows="8" id="daftar-nama-kelompok"></textarea>
	<button class="btn btn-primary col-12 mt-2" onclick="buat.ambil_siswa()">Next <i class="fa fa-arrow-right"></i></button>
</div>

<div id="buat-kelompok" style="display:none">
	<h3>Acak Anggota Kelompok</h3>

	<!-- jumlah -->
	Input jumlah kelompok atau jumlah anggota perkelompok:
	<div class="input-group mb-3">
	  <input type="number" class="form-control" id="amount" placeholder="Enter amount" style="width: 100px;">
	  <div class="input-group-append">
	    <select class="custom-select" id="anggota_kelompok">
	      <option value="anggota">Anggota Setiap Kelompok</option>
	      <option selected value="kelompok">Kelompok</option>
	    </select>
	  </div>
	</div>
	<div class="d-flex justify-content-between">
		<button class="btn btn-secondary col-2 mr-1" onclick="buat.kembali_ke_ambil()"><i class="fa fa-arrow-left"></i></button>
		<button class="btn btn-primary col-9" onclick="buat.get_jml()">Next <i class="fa fa-arrow-right"></i></button>
	</div>
</div>

<div class="d-flex flex-wrap justify-content-center" id="wadah-kelompok" style="display: none"></div>
<hr>
<div class="d-flex justify-content-center mb-3" id="tombol-acak" style="display: none !important">
  <button class="btn btn-outline-primary rounded-pil" onclick="buat.acak()"><i class="fa fa-dice"></i></button>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script type="text/javascript">
	var siswa = []
	var jml_kelompok = 0
	var max_siswa = 0
	var anggota_kelompok = ''
	var nama_sementara = []
	var kelompok_ke = 0
	var siswa_ke = 1

	function shuffleArray(array) {
	  for (let i = array.length - 1; i > 0; i--) {
	    const j = Math.floor(Math.random() * (i + 1));
	    [array[i], array[j]] = [array[j], array[i]];
	  }
	  return array;
	}

	function addNumbersToDuplicates(array) {
	  var duplicates = {};

	  for (var i = 0; i < array.length; i++) {
	    var currentItem = array[i];

	    if (duplicates[currentItem]) {
	      duplicates[currentItem]++;
	      array[i] = currentItem + duplicates[currentItem];
	    } else {
	      duplicates[currentItem] = 1;
	    }
	  }

	  return array;
	}

	const buat = {
		ambil_siswa: () => {
			var nama = $('#daftar-nama-kelompok').val().split(',')
			if (nama.length < 4) {
				alert('paling sedikit 4')
				return
			}

			var nama_valid = []
			nama.forEach((i, idx) => {
				var nama_temp = i.trim()
				if (nama_temp.length > 0) {nama_valid.push(nama_temp)}
			})

			// cek jika ada yang dobel
			nama_valid_no_dobel = addNumbersToDuplicates(nama_valid)

			window.localStorage.setItem('siswa', JSON.	stringify(nama_valid_no_dobel));
			siswa = nama_valid_no_dobel

			$('#masukkan-nama').hide()
			$('#buat-kelompok').show()
		},
		kembali_ke_ambil : () => {
			$('#daftar-nama-kelompok').val(siswa.join(", "))
			$('#buat-kelompok').hide()
			$('#masukkan-nama').show()
		},
		get_jml: () => {
			$('#wadah-kelompok').html('')
			jml = parseInt($('#amount').val())
			anggota_kelompok = $('#anggota_kelompok').val()
			var jml_siswa = siswa.length

			// validasi jml
			if (!(jml >= 2) || jml > Math.floor(jml_siswa/2)) {
				alert('min 2 anggota kelompok atau 2 kelompok dan max '+Math.floor(jml_siswa/2))
				return
			}

			if (anggota_kelompok == 'kelompok') {
				jml_kelompok = jml
				max_siswa = Math.floor(jml_siswa/jml)
			} else {
				jml_kelompok = Math.floor(jml_siswa/jml)
				max_siswa = jml
				if ((max_siswa*jml_kelompok) < jml_siswa) {
					jml_kelompok+=1
				}
			}

			for (var i = 0; i < jml_kelompok; i++) {
				$('#wadah-kelompok').append(`
					<div class="list-group d-inline-block m-2" id="list-siswa-kelompok-${i}">
					  <div class="list-group-item list-group-item-action bg-primary">
					    <div class="d-flex w-100 justify-content-between">
					      <h5 class="mb-1"><b>Kelompok ${i+1}</b></h5>
					    </div>
					  </div>
					</div>`)
			}

			$('#buat-kelompok').hide()
			$('#wadah-kelompok').show()
			$('#tombol-acak').show()
		},
		acak: () => {
			$('.list-nama').remove()
			nama_sementara.length = 0
			nama_sementara = [...shuffleArray(siswa)]
			kelompok_ke = 0
			siswa_ke = 1
			if (anggota_kelompok == 'kelompok') {
				letakkan()
			} else {
				letakkan2()
			}
		}
	}

	function letakkan () {
		if (kelompok_ke == jml_kelompok) {
			kelompok_ke = 0
		}

		$('#list-siswa-kelompok-'+kelompok_ke).append(`
		<div class="list-group-item list-group-item-action list-nama">
		  <div class="d-flex w-100 justify-content-between">
		    <h5 class="mb-1 text-capitalize"><b>${nama_sementara[0]}</b></h5>
		  </div>
		</div>`)
		if (nama_sementara.length > 1) {
			nama_sementara.shift()
			kelompok_ke += 1
			setTimeout(letakkan, 1000)
		}
	}

	function letakkan2 () {
		if (kelompok_ke == jml_kelompok) {
			kelompok_ke = 0
		}

		$('#list-siswa-kelompok-'+kelompok_ke).append(`
		<div class="list-group-item list-group-item-action list-nama">
		  <div class="d-flex w-100 justify-content-between">
		    <h5 class="mb-1 text-capitalize"><b>${nama_sementara[0]}</b></h5>
		  </div>
		</div>`)
		siswa_ke += 1
		if (siswa_ke > max_siswa) {
			kelompok_ke += 1
			siswa_ke = 1
		}
		if (nama_sementara.length > 1) {
			nama_sementara.shift()
			setTimeout(letakkan2, 1000)
		}
	}

	function cek_daftar_nama() {
		if (typeof window.localStorage !== "undefined" && window.localStorage.getItem("siswa") !== null) {
			siswa = JSON.parse(localStorage.getItem('siswa'))

			$('#daftar-nama-kelompok').val(siswa.join(", "))
		}
	}

	$(document).ready(() => {
		cek_daftar_nama()
	})
</script>
<?= $this->endSection() ?>