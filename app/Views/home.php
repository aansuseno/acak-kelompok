<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="icon" href="/img/ikon-acak.ico" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Aplikasi Acak Kelompok adalah sebuah alat yang dirancang untuk membantu mengacak dan membagi sekelompok orang menjadi kelompok-kelompok yang lebih kecil. Dengan aplikasi ini, Anda dapat dengan mudah mengatur dan mengelompokkan anggota tim, peserta pelatihan, atau siapa pun yang perlu dibagi menjadi beberapa kelompok secara acak.">
  <meta property="og:image" content="/img/preview.png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="630">
  <title><?= (isset($title)) ? $title : '' ?> | Acak Kelompok</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
</head>
<body>
<style>
  .landing-page {
    background: url('https://picsum.photos/1000/1000') center/cover no-repeat;
    position: relative;
    height: 100vh;
  }

  .overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.7);
  }
</style>
<div class="landing-page">
    <div class="overlay"></div>
    <div class="container">
      <div class="row justify-content-center align-items-center vh-100">
        <div class="col-md-6 text-center">
          <h1 class="text-light"><b>Buat grup tugas</b></h1>
          <p class="text-light">Silakan masuk atau daftar untuk mendapatkan fitur lengkapnya.</p>
          <a href="/acak" class="btn btn-success btn-lg btn-block mb-3">Buat Acak</a>
          <div class="row">
            <div class="col-sm-6 mt-2">
              <a href="/register" class="btn btn-outline-primary btn-block">Daftar</a>
            </div>
            <div class="col-sm-6 mt-2">
              <a href="/login" class="btn btn-primary btn-block">Masuk</a>
            </div>
          </div>
          <a class="btn btn-outline-primary btn-sm col-12 mt-3" href="/tamu">Masuk Tanpa Daftar</a>
        </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/adminlte.min.js"></script>
<?= $this->renderSection('js')?>
</body>
</html>