<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<div class="alert">
	<h2 class="mt-3 text-light"><?= $title ?></h2>
	<hr>
	<p class="text-light"><?= $tugas['deskripsi'] ?></p>
</div>

<div class="d-flex flex-wrap justify-content-center pb-5">
	<?php  foreach ($kelompok['kelompok'] as $key => $value): ?>
		<div class="list-group d-inline-block m-2 kotak-kotak">
		  <div class="list-group-item list-group-item-action bg-dark">
		    <div class="d-flex w-100 justify-content-between nama-kelompok" id="nama-kelompok-${idx}">
		      <h5 class="mb-1"><b contentediable="false" ></b><?= $value->nama ?></h5>
		    </div>
		  </div>
		  <?php foreach ($value->siswa as $k => $v): ?>
		  	<div class="list-group-item list-group-item-action siswa-preview" id="siswa-tampil-${idx}-${jdx}">
		  	  <div class="d-flex w-100 justify-content-between">
		  	    <h5 class="mb-1"><b><?= $v->{'0'}->nama ?></b></h5>
		  	    <div>
		  	    	<?php foreach ($v->lecana as $l): ?>
		  	    		<span><?= $l->ikon ?></span>
		  	    	<?php endforeach ?>
		  	    </div>
		  	  </div>
		  	</div>
		  <?php endforeach ?>
		</div>
	<?php endforeach ?>
</div>

<style type="text/css">
	body {
		background: url('https://picsum.photos/1000/1000') center/cover no-repeat, rgba(0,0,0,.4);
		position: relative;
	}

	body::before {
	  content: "";
	  position: absolute;
	  top: 0;
	  left: 0;
	  width: 100%;
	  height: 100%;
	  min-height: 100vh;
	  background-color: rgba(0, 0, 0, 0.8);
	}
</style>
<?= $this->endSection() ?>