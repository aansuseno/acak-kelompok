<?= $this->extend('template') ?>

<?= $this->section('body') ?>
<div class="d-flex justify-content-between">
	<h2>
		Kelas <span id="nama_kelas"><?= $kelas['nama'] ?></span>
	</h2>
	<div>
		<button class="btn btn-outline-warning rounded-pill ml-2 mt-2 btn-sm" id="button-edit-nama-kelas" onclick="edit_kelas()"><i class="fa fa-cog"></i></button>
		<button class="btn btn-outline-danger rounded-pill ml-2 mt-2 btn-sm" onclick="hapus_kelas()"><i class="fa fa-trash"></i></button>
		<a href="/home" class="btn btn-outline-secondary rounded-pill ml-2 mt-2 btn-sm" ><i class="fa fa-home"></i></a>
	</div>
</div>
<hr>

<div class="d-flex mb-3 justify-content-between">
	<button class="col-3 btn btn-outline-success mx-1 btn-atas rounded-pill btn-sm" id="btn-siswa" onclick="tampil_menu('siswa')" title="siswa">
		<i class="fa fa-user"></i>
	</button>
	<button class="col-3 btn btn-outline-success mx-1 btn-atas rounded-pill btn-sm" id="btn-tugas" onclick="tampil_menu('tugas')" title="tugas">
		<i class="fa fa-tasks"></i>
	</button>
	<button class="col-3 btn btn-outline-success mx-1 btn-atas rounded-pill btn-sm" id="btn-lecana" onclick="tampil_menu('lecana')" title="lecana">
		<i class="fa fa-id-badge"></i>
	</button>
</div>

<div class="kumpulan siswa">
	<h4><b>List Siswa</b></h4>
	<div class="d-flex justify-content-end">
		<button class="btn btn-success" id="addsiswa" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i> Tambah Siswa</button>
	</div>

	<br><br>
	<table class="table table-hover table-responsive">
		<thead>
			<tr>
				<th>Nama</th>
				<th>Lecana</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody id="data-siswa"></tbody>
	</table>
</div>

<div class="kumpulan tugas" style="display: none">
	<h4><b>List Tugas</b></h4>
	<div class="d-flex justify-content-end">
		<button class="btn btn-success" id="addtugas" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i> Tambah Tugas</button>
	</div>

	<br><br>
	<div class="row" id="daftar-tugas"></div>
</div>

<!-- for alert -->
<div id="err" style="display: none">
		<div class="alert alert-warning alert-dismissible fade show mt-2" role="alert">
		<span class="text_err"></span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</div>

<div class="row" id="daftar-kelas"></div>

<!-- badge -->
<div id="kotak-lecana" class="kumpulan" style="display: none">
	<h4><b>List Lecana</b></h4>
	<div class="d-flex justify-content-end">
			<button class="btn btn-success" id="addlecana" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i> Buat Lecana Baru</button>
		</div>
	<hr>
	<div id="accordion">
	</div>
</div>

<!-- modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="mdl-body"></div>
			<div class="modal-footer" id="footer-button">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script type="text/javascript">
	var nama_kelas_sekarang = '<?= $kelas['nama'] ?>'
	const id_kelas_ = <?= $kelas['id'] ?>
</script>
<script src="/assets/detail-kelas.js"></script>
<?php if (isset($_GET['page'])): ?>
	<script type="text/javascript">
		$(document).ready(() => {
			tampil_menu('<?= $_GET['page'] ?>')
		})
	</script>
<?php endif ?>
<?= $this->endSection() ?>