<?php

namespace App\Models;

use CodeIgniter\Model;

class KelasModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'kelas';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['nama'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function get_by_user($user_id)
    {
        return $this->select('kelas.nama, kelas.id, kelas.updated_at')
            ->join('users_kelas ukelas', 'ukelas.id_kelas = kelas.id')
            ->where('ukelas.id_users', $user_id);
    }

    public function detail($id_user, $id_kelas)
    {
        return $this->select('kelas.nama, kelas.id')
            ->join('users_kelas ukelas', 'ukelas.id_kelas = kelas.id')
            ->where('ukelas.id_users', $id_user)
            ->where('kelas.id', $id_kelas);
    }

    public function is_valid($id_kelas)
    {
        $kelas = $this
            ->join('users_kelas uk', 'uk.id_kelas = kelas.id')
            ->where('uk.id_users', session()->id)
            ->where('kelas.id', $id_kelas)
            ->countAllResults();

        return ($kelas > 0) ? true : false;
    }
}
