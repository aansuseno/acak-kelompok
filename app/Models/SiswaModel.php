<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'siswa';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function get_data($id_users, $id_kelas)
    {
        return $this->select('siswa.nama, siswa.id')
            ->join('kelas k', 'k.id = siswa.id_kelas')
            ->join('users_kelas uk', 'uk.id_kelas = siswa.id_kelas')
            ->join('users u', 'uk.id_users = u.id')
            ->where('uk.id_users', $id_users)
            ->where('siswa.id_kelas', $id_kelas)
            ->where('siswa.is_aktif', 1);
    }

    // cek id siswa di autorize
    public function is_valid($id_users, $id_siswa = -1)
    {
        if ($id_siswa == -1) {
            $id_siswa = $id_users;
            $id_users = session()->id;
        }

        $this->select('*')
            ->join('kelas k', 'k.id = siswa.id_kelas')
            ->join('users_kelas uk', 'uk.id_kelas = siswa.id_kelas')
            ->join('users u', 'uk.id_users = u.id')
            ->where('uk.id_users', $id_users)
            ->where('siswa.id', $id_siswa)
            ->where('siswa.is_aktif', 1);
           
        if ($this->countAllResults() > 0) {
            return true;
        }

        return false;
    }
}
