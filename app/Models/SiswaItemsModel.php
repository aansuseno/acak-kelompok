<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaItemsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'siswa_items';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function tambah($data)
    {
        $data['id_users'] = session()->id;
        return $this->insert($data);
    }

    public function sudah_ada_kah($id_siswa, $id_items)
    {
        $this->where('id_siswa', $id_siswa)
            ->where('id_items', $id_items);

        if ($this->countAllResults() > 0) {
            return true;
        }

        return false;
    }

    public function get_by_id($id)
    {
        return $this->where('id', $id);
    }
}
