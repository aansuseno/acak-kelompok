<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaKelompokModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'siswa_kelompok';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function simpan($data, $id_tugas)
    {
        $data_baru = [];
        $mkelompok =  model('KelompokModel');
        foreach ($data as $k => $d) {

            // buat kelompok
            $mkelompok->insert([
                'nama' => 'Kelompok '.$k+1,
                'id_tugas' => $id_tugas
            ]);

            foreach ($d as $s) {
                array_push($data_baru, [
                    'id_kelompok' => $mkelompok->insertId(),
                    'id_siswa' => $s
                ]);
            }
        }
        $this->insertBatch($data_baru);
    }
}
