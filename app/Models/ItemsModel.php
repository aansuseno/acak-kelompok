<?php

namespace App\Models;

use CodeIgniter\Model;

class ItemsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'items';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function get_by_siswa($id_siswa)
    {
        return $this->select('items.nama, items.ikon, si.id as id_siswa_items')
            ->join('siswa_items si', 'si.id_items = items.id')
            ->join('siswa s', 's.id = si.id_siswa')
            ->where('s.id', $id_siswa);
    }

    public function all_user_have($id_siswa = 0)
    // semua ikon yang bisa ditampilkan di user tertentu
    {
        return $this->select('items.nama, items.ikon, items.id, items.is_default, items.deskripsi')
            ->groupStart()
                ->where('items.id_users', session()->id)
                ->orWhere('items.is_default', 1)
            ->groupEnd();
    }
}
