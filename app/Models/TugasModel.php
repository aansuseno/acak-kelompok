<?php

namespace App\Models;

use CodeIgniter\Model;

class TugasModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'tugas';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function tambah($data)
    {
        return $this->insert($data);
    }

    public function get_data($id_kelas)
    {
        return $this
            ->select('id, nama, deskripsi, updated_at as tanggal')
            ->where('id_kelas', $id_kelas);
    }

    public function detail($id_tugas)
    {
        return $this
            ->where('id', $id_tugas);
    }

    public function is_valid($id_tugas)
    {
        $this->select('*')
            ->where('id', $id_tugas);

        $tugas = $this->first();

        if ($tugas == null) {
            return false;
        }
           
        if (model('KelasModel')->is_valid($tugas['id_kelas'])) {
            return true;
        }

        return false;
    }
}
