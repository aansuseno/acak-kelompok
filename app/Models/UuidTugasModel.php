<?php

namespace App\Models;

use CodeIgniter\Model;

class UuidTugasModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'uuid_tugas';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    function generate_random_string($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters_length = strlen($characters);
        $random_string = '';

        try {
            $bytes = random_bytes($length);
        } catch (Exception $e) {
            return false; // Error generating random bytes
        }

        for ($i = 0; $i < $length; $i++) {
            $index = ord($bytes[$i]) % $characters_length;
            $random_string .= $characters[$index];
        }

        return $random_string;
    }

    public function ambil_uuid($id_tugas)
    {
        $this->where('id_tugas', $id_tugas);

        if ($this->countAllResults() == 0) {
            $uuid = $this->generate_random_string(6);
            
            while ($this->where('uuid', $uuid)->countAllResults() > 0) {
                $uuid = $this->generate_random_string(6);
            }
            
            $this->insert([
                'uuid' => $uuid,
                'id_tugas' => $id_tugas
            ]);

            return $uuid;
        }

        return $this->where('id_tugas', $id_tugas)->first()['uuid'];
    }

    public function get_by_uuid($uuid)
    {
        return $this->where('uuid', $uuid);
    }
}
