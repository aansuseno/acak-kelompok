<?php

namespace App\Models;

use CodeIgniter\Model;

class NamaTemplate extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'nama_template';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = false;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function ambil_mirip($mirip)
    {
        $this
            ->select('nama')
            ->like('nama', $mirip, 'after')
            ->orderBy('nama', 'random');

        if (session()->is_tamu) {
            $this->limit(5);
        } else {
            $this->limit(15);
        }

        return $this;
    }
}
